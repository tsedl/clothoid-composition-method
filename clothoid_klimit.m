%% clothoid investigation with plotting

% clothoid_klimit, will plot a single clothoid segment with a constant
% sharpness given by KDot. If the curvature exceeds kmax (or -kmax) this 
% function will plot an arc of kmax until the specified length S 

function [x,y,psi,kappa,t] = clothoid_klimit(Kdot, S, kappa_0, kappa_max)

dt = 0.01;
n_samples = ceil(S/dt);


if(S==0)
    fprintf('travel distance S must be greater than the step size %d m \n',dt);
    t=0;
    flex_dt=0;
else
    flex_dt = S/n_samples;
    t = flex_dt:flex_dt:S;
end

kappa = Kdot*t + kappa_0;

kappa = kappa.*(kappa<=kappa_max) + kappa_max*(kappa>kappa_max);
kappa = kappa.*(kappa>=-kappa_max) - kappa_max*(kappa<-kappa_max);

if any(kappa>kappa_max) || any(kappa<-kappa_max)
    fprintf('curvature limited to %d radians/metre, this will affect the path! \n',kappa_max);
end


psi = cumsum(kappa*dt);%Kdot*t.*t*0.5 + kappa_0*t;%
psi = wrapToPi(psi); % wrap angle to range (-pi, +pi) to prevent weirdness

   
x = cumsum(flex_dt*cos(psi)) ;
y = cumsum(flex_dt*sin(psi));
if(any(isnan(x)))
    assert(false)
end


end



