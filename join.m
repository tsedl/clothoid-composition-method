function [xx, yy, psipsi, kappakappa, ss] = join(s_list1, s_list2, produce_graph)
% given two s_lists (stacked rows of x, y, heading, curvature, s) rotate and
% translate all poses in s_list2 to make it continuous with the end of
% s_list1
if nargin < 3
    produce_graph = false;
end
m = length(s_list1);
n = m/5;
x =s_list1(:,1:n);
y = s_list1(:,n+1:2*n);
psi = s_list1(:,2*n+1:3*n);
kappa = s_list1(:,3*n+1:4*n);
s = s_list1(:,4*n+1:5*n);

n = length(x);
s_m = [x(n), y(n), psi(n), kappa(n), s(n)];
psi_m = s_m(3);

p = length(s_list2);
n = p/5;
x_ =s_list2(:,1:n);
y_= s_list2(:,n+1:2*n);
psi_ = s_list2(:,2*n+1:3*n);
kappa_ = s_list2(:,3*n+1:4*n);
s_ = s_list2(:,4*n+1:5*n);


n_ = length(x_);
if n_ == 0
    display(n, 'aah')
end
s_2 = [x_(n_);y_(n_);psi_(n_);kappa_(n_);s_(n_)];

% figure();
% plot(x,y)
% hold on
% plot(x_,y_)
% hold on


rot  = [cos(psi_m), -sin(psi_m);
    sin(psi_m), cos(psi_m)];
s_f(1:2) = s_m(1:2) + s_2(1:2)'*rot;
s_f(3) = wrapToPi(s_m(3) + s_2(3));
s_f(4) = s_2(4);

xyshift = rot*[x_;y_];
xx = [x, s_m(1) + xyshift(1,:)];
yy = [y, s_m(2) + xyshift(2,:)];
if produce_graph
    plot(s_m(1) + xyshift(1,:), s_m(2) + xyshift(2,:))
    hold on
end

psipsi = [psi, s_m(3) + psi_];
psipsi = wrapToPi(psipsi);%mod(psipsi + 2*pi, 2*pi) - 2*pi;
kappakappa = [kappa, kappa_];
ss = [s, s_m(5) + s_];

end
