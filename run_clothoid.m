% This is all original work by E.D.Lambert but this paper seems to develop 
% similar ideas judging by the abstract
% Path planning for autonomous vehicles using clothoid based smoothing of A * generated paths and optimal control
% Lundberg, Marcus
% KTH, School of Engineering Sciences (SCI), Mathematics (Inst.), Numerical Analysis, NA.
% 2017 (English)

function run_clothoid()
close all; clear all;
[x, y, psi, kappa] = clothoid(5, 10, 0);
fig1 = plot(x, y);

%title('single clothoid')
xlabel('x(m)')
ylabel('y(m)')
legend('\alpha=5')

% n=length(x);
% xf = x(n);
% yf = y(n);
% psif = psi(n);
% kappaf = kappa(n);
% [x, y, psi, kappa] = clothoid_pair(-2, 1, 4, 0.5);
% plot(x, y)
% title('clothoid pair')

clothoid_arc_clothoid(2, 1, -4, 0.5, 1.2);
title('Clothoid-Arc-Clothoid')
grid on
axis equal

end



function [xx, yy, psipsi, kappakappa] = clothoid_arc_clothoid(Kdot1, S1, Kdot2, S2, arcLength)
% first clothoid
[x, y, psi, kappa] = clothoid(Kdot1, S1, 0);
n = length(x);
s_m = [x(n);y(n);psi(n);kappa(n)];
kap_m = s_m(4);

figure();
plot(x,y)
hold on;

produce_graph = 0
% arc of radius 1/kappa
[xr, yr, psir, kappar] = clothoid(0, arcLength, kap_m);
[xx, yy, psipsi, kappakappa] = join([x, y, psi, kappa],[xr, yr, psir, kappar], produce_graph)

% arc of radius 1/kappa
[x_, y_, psi_, kappa_] = clothoid(Kdot2, S2, s_m(4));
[xx, yy, psipsi, kappakappa] = join([xx, yy, psipsi, kappakappa],[x_, y_, psi_, kappa_], produce_graph);

end

