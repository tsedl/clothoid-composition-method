% Attempted to find some analytical derivatives in x and y 
% test them by comparing the output to central difference

%dx/dalpha

% needs to be slightly larger than the deflection of the first curve 
% otherwise the deflection of the second curve goes to zero and the
% sharpness becomes infinite to meet the boundary conditions

%dy/dalpha

a=0;
b=1;
c=1;
d=1;
step=0.01;
f_arr=[];
dfdx_arr=[];
x_arr = 0.1:step:2;
for xi = x_arr
    fi = sample(xi, a,b,c,d);
    f_arr = [f_arr, fi];
    dfdx = fbyalpha(xi, a,b,c,d);
    dfdx_arr = [dfdx_arr, dfdx];
end
figure
plot(x_arr, f_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(f_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(x_arr(2:length(x_arr)-1), grad)
%analytic diff
plot(x_arr, dfdx_arr);

title(['\partial f/\partial\alpha, a=b=c=d=',int2str(a)])
legend('f', '( f(\alpha + \Delta\alpha) - f(\alpha - \Delta\alpha) ) / 2\Delta\alpha', 'analytic gradient')
xlabel('\alpha [radians/m^2]')
hold off


function f = sample(alpha ,a, b, c, d)
y = a + (b/sqrt(alpha));
fun = @(u) sin( c*sqrt(alpha).*u + d*alpha*u.*u );
f = integral(fun, 0, y);
end

function grad = fbyalpha(alpha ,a, b, c, d)
% df/dalpha
y = a + (b/sqrt(alpha));
fun1 = @(u) ((1/2)*c*u/sqrt(alpha) + d*u.*u).*cos( c*sqrt(alpha).*u + d*alpha*u.*u );
dHbydalpha = integral(fun1, 0 , y);

dHbydy = sin(c*sqrt(alpha)*y + d*alpha*y*y);
dybydalpha = (-1/2)*b*alpha^(-3/2);

grad = dHbydalpha + dHbydy*dybydalpha;

end


