%just as an experiemnt before I break the original
function [alpha, chainage] = matched_pairs_to_simple_clothoids_new(alpha_first, deflection, theta)
    chainage = zeros(1, 2*length(deflection));
    alpha = zeros(1, 2*length(alpha_first));
    
    for i = 1:length(deflection)
        deflection2 = theta(i) - deflection(i);
        if(abs(deflection2)>eps)
            alpha2 = -alpha_first(i)*deflection(i)/deflection2;
        else
            % as the deflection2 is zero, the length will be zero and
            % the sharpness magnitude is irrelevant
            % the sign must match the deflection for the square below?? I
            % set it to 1...
            alpha2 = 1;
        end
        s1 = sqrt(2*abs(deflection(i)/alpha_first(i)));
        s2 = sqrt(abs(2*deflection2/alpha2));
        
        alpha(2*i-1) = alpha_first(i);
        alpha(2*i) = alpha2;
        chainage(2*i - 1) = s1;
        chainage(2*i) = s2;

    end
end