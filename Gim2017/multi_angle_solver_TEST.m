
%%
Thetaf = linspace(-pi, pi, 6); % check positions around a circle
n = length(Thetaf);
Psif = zeros(1, n*n);
Xf = zeros(1, n*n);
Yf = zeros(1, n*n);
for i = 1:n
    Psif((i-1)*n+1 : i*n) = Thetaf;
    Xf((i-1)*n+1 : i*n) = cos(Thetaf);
    Yf((i-1)*n+1 : i*n) = sin(Thetaf);
end

Pi = [0,0,0,0];
for j = 1:n*n
    Pf = [Xf(j), Yf(j), Psif(j), 0];
    [x, y, heading, curvature, s] = function_under_test(Pi, Pf);
    assert(x(length(x))==Pf(1));
    assert(y(length(y))==Pf(2));
    assert(heading(length(heading))==Pf(3));
    assert(curvature(length(curvature))==Pf(4));
end
%%
function [x, y, heading, curvature, s] = function_under_test(Pi, Pf)
[x, y, heading, curvature, s] = solve_fmincon_deriv(Pi, Pf);
end