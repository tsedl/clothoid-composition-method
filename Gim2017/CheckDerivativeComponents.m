clear;

check_integral = 0
if check_integral
    %dx/dalpha
    delta = 1;
    step=0.001;
    x_arr=[];
    x_arr_alt=[];
    dxda_arr=[];
    alpha_arr = 0.1:step:2;
    for alphai = alpha_arr
        [xi,~] = sample(alphai, delta);
        [xi_alt,~] = sample_alt(alphai, delta);
        x_arr = [x_arr, xi];
        x_arr_alt = [x_arr_alt, xi_alt];
        dxda = xbyalpha(alphai, delta);
        dxda_arr = [dxda_arr, dxda];
    end
    plot(alpha_arr, x_arr)
    hold on;
    %numeric diff
    filt = [1, 0, -1];
    grad = conv(x_arr, filt/step, 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(alpha_arr(2:length(alpha_arr)-1), grad)
    %analytic diff
    plot(alpha_arr, dxda_arr);


    plot(alpha_arr, x_arr_alt)
    grad_alt = conv(x_arr_alt, filt/step, 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(alpha_arr(2:length(alpha_arr)-1), grad_alt)

    title(['\partial x/\partial\alpha, \delta=',int2str(delta)])
    legend('x', 'x(\alpha - \Delta\alpha) + x(\alpha + \Delta\alpha)  / 2\Delta\alpha', 'analytic gradient', 'x_{alt}', 'x_{alt}(\alpha - \Delta\alpha) + x_{alt}(\alpha + \Delta\alpha)  / 2\Delta\alpha')
    xlabel('\alpha [radians/m^2]')
    hold off
    clear
end

% components
%
% dx/dL
step=0.001;
alpha_arr = 0.1:step:2;
delta = 1;
x_arr_alt=[];
dxdL_arr=[];
alpha_arr = 0.1:step:2;
L_arr = sqrt(2*delta./alpha_arr);
for alphai = alpha_arr
    [xi_alt,~] = sample_alt(alphai, delta);
    x_arr_alt = [x_arr_alt, xi_alt];
    dxdL = xbyL(alphai, delta);
    dxdL_arr = [dxdL_arr, dxdL];
end

plot(L_arr, x_arr_alt)
hold on

%numeric diff
filt = [1, 0, -1];
grad = conv(x_arr_alt, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(L_arr(2:length(L_arr)-1), grad)

%analytic diff
plot(L_arr, dxdL_arr);

%sanity check
check = cumsum([x_arr_alt(1), grad*step]);
x_arr_alt(1:length(check)) - check
plot(L_arr(1:length(check)), check);

xlabel('L [m]')
legend('x [m]','x(L - \DeltaL) + x(L + \DeltaL)  / 2\DeltaL', '\partialx/\partialL', 'check');






function [xf, yf] = sample_alt(alpha, delta)
L = sqrt(2*delta/alpha);
funx = @(u)cos(alpha*u.*u/2);
xf = integral(funx, 0, L);
funy = @(u)sin(alpha*u.*u/2);
yf = integral(funy, 0, L);
end

function [xf, yf] = sample(alpha, delta)
L = sqrt(2*delta/alpha);
[x,y,~] = clothoid(alpha, L, 0);
xf = x(length(x));
yf = y(length(y));
end

function grad = xbyalpha(alpha, delta)
L = sqrt(2*delta/alpha);
fun = @(u)u.*u.*sin(alpha.*u.*u/2);
grad = -sqrt(delta/(2*alpha^3))*cos(delta) - 0.5*integral(fun, 0, L);
end

function grad = xbyL(alpha, delta)
L = sqrt(2*delta/alpha);
fun = @(u)(-0.5)*u.*u.*sin(alpha.*u.*u/2);
grad = cos(alpha*L*L/2);% + integral(fun, 0, L);
end
