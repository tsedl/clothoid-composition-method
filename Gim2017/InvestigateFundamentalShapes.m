% try find fundamental clothoid shape, which can only be scaled by changing
% alpha. It is not possible to reach poses where y < yfun(x, theta) with 
% two clothoids. Bisection will never converge etc
angle = 60*pi/180;
[C, S] = fundamentalClothoid(angle);
xgoal = 1; 
scale = xgoal/C;
yfun = scale*S

% Try a range of final angles
psi_degrees = 1:180;
yfun_arr = zeros(size(psi_degrees));
phi_max_arr =  zeros(size(psi_degrees));
phi_min_arr = zeros(size(psi_degrees));
Carr = zeros(size(psi_degrees));
Sarr = zeros(size(psi_degrees));
for i = 1:length(psi_degrees)
    [Carr(i), Sarr(i)] = fundamentalClothoid(pi*psi_degrees(i)/180);
    xgoal = 1; 
    scale = xgoal/Carr(i);
    yfun_arr(i) = scale*Sarr(i);
    phi_min_arr(i)  = 180*atan2(yfun_arr(i),xgoal);
    phi_max_arr(i)  = 180*atan2(yfun_arr(i),xgoal);
end

figure
plot(Carr, Sarr)

% figure
% plot(angle_degrees, yfun_arr, '.');
% hold on 
% p1 = polyfit(angle_degrees, yfun_arr, 1);
% fit_y1 = polyval(p1, angle_degrees);
% plot(angle_degrees, fit_y1, '-');
% xlabel('angle [degrees]')
% ylabel('y/x ratio')
% legend('y/x', ['linear fit m=',num2str(p1(1))])

figure
plot(psi_degrees, psi_degrees,'-');
hold on
plot(psi_degrees, (180/pi)*atan2(yfun_arr, 1),'.');
hold on
p2 = polyfit(psi_degrees, (180/pi)*atan2(yfun_arr, 1), 1);
fit_y2 = polyval(p2, psi_degrees);
plot(psi_degrees, fit_y2, '-');
xlabel('final heading [degrees]')
ylabel('clothoid shape atan(y/x) [degrees]')
legend( 'final heading', 'clothoid shape atan(y/x)', ['linear fit m=',num2str(p2(1)), ' c=', num2str(p2(2))] )
title('Linear fit between final heading and clothoid shape atan(y/x)')

%Now try to work out the other limit
psi_degrees = 60;
angle = psi_degrees*pi/180;
[C, S] = fundamentalClothoid(angle);
xgoal = 10; 
scale = xgoal/C;
yfun = scale*S
% this is a rotation by angle in combination with reflection in y axis
xr = xgoal*cos(angle) + yfun*sin(angle);
yr = xgoal*sin(angle) - yfun*cos(angle);
% this works but it takes a long time to solve
 delta = 0.5;
 f_solve_bisection([0, 0, 0], [xgoal, yfun, angle])
 f_solve_bisection([0, 0, 0], [xr, yr, angle])
% % should be the same as
 f_solve_bisection([xgoal, yfun, angle + pi], [0,0,pi])


% invert the linear fit to find the maximum psi reachable at a given y/x
figure
x = 10;
y = 4;
psi_max = ((180/pi)*atan2(y, x) - p2(2))/p2(1)
psi_min = (180/pi)*atan2(y, x)

n = 5
psi = linspace(psi_min + 15, psi_max - 5, n);
for i = 1:n
    f_solve_bisection([0, 0, 0], [x, y, (pi/180)*psi(i)])
end

subplot(2, 1, 1)
title(['Paths to [',num2str([x, y]),'], \psi=',num2str(psi_min),' to \psi=', num2str(psi_max)])


function [x,y] = fundamentalClothoid(theta)
fun1 = @(v)cos(v)./sqrt(v);
x = (1/sqrt(2*pi))*integral(fun1, 0, theta);
fun2 = @(v)sin(v)./sqrt(v);
y = (1/sqrt(2*pi))*integral(fun2, 0, theta);
end