test = uint8( 3);

if bitand(test,uint8(1))>0
    
    Lstep = 10^-3;
    delta = 1;

    L_arr = 0.6:Lstep:2;
    alpha_arr = 2*delta./(L_arr.*L_arr);
    
    x_arr = zeros(1, length(alpha_arr));
    dxdL_arr = zeros(1, length(alpha_arr));
    
    for i = 1:length(alpha_arr)
        x_arr(i) = sample(alpha_arr(i), delta);
        dxdL_arr(i) = xbyL(alpha_arr(i), delta);
    end

    plot(L_arr, x_arr)
    hold on

    %numeric diff
    filt = [1, 0, -1];
    grad = conv(x_arr, filt/(2*Lstep), 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(L_arr(2:length(L_arr)-1), grad)

    %analytic diff
    plot(L_arr, dxdL_arr);
    
%    plot(L_arr, alpha_arr);

    title(['\partial x/\partialL, \delta=',int2str(delta)])
    xlabel('L [m]')
    legend('x','( x(L + \DeltaL) - x(L - \DeltaL) )/ 2\DeltaL', '\partialx/\partialL', '\alpha');%, 'check');
    
%     %sanity check
%     check = cumsum([x_arr(1), grad*Lstep]);
%     %x_arr_alt(1:length(check)) - check
%     plot(L_arr(1:length(check)), check);
end

if bitand(test,uint8(2))>0
    alpha = 2;
    x_arr=[];
    dxdL_arr=[];
    step = 10^-3;
    L_arr = 0.1:step:2;
    delta_arr = alpha*L_arr.*L_arr/2;
    for deltai = delta_arr
        xi = sample(alpha, deltai);
        x_arr = [x_arr, xi];
        dxdL = xbyL(alpha, deltai);
        dxdL_arr = [dxdL_arr, dxdL];
    end
    figure
    plot(L_arr, x_arr)
    hold on;
    %numeric diff
    filt = [1, 0, -1];
    grad = conv(x_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(L_arr(2:length(L_arr)-1), grad)
    %analytic diff
    plot(L_arr, dxdL_arr);
    
    %plot(L_arr, delta_arr)

    title(['\partial x/\partialL, \alpha=',int2str(alpha)])
    legend('x', '( x(L + \DeltaL) - x(L - \DeltaL) ) / 2\DeltaL', '\partialx/\partialL', '\delta')
    xlabel('L [m]')
    
    

%     %sanity check
%      check = cumsum([x_arr(1), grad*step]);
%      %x_arr_alt(1:length(check)) - check
%      plot(L_arr(1:length(check)), check);
    hold off  
end

function x = sample(alpha, delta)
    L = sqrt(2*delta/alpha);
    fun = @(u) cos(alpha*u.*u/2);
    x = integral(fun, 0, L);
end

function grad = xbyL(alpha, delta)
    L = sqrt(2*delta/alpha);
    %grad = cos(alpha*L.*L/2);
    fun = @(u) (-u.*u/2).*sin(alpha*u.*u/2);
    grad = cos(delta) + (-4*delta/L^3)*integral(fun, 0, L);
end