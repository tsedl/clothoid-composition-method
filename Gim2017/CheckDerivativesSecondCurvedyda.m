% Attempted to find some analytical derivatives in x and y 
% test them by comparing the output to central difference

%dx/dalpha
delta = 0.5;
psi_g = 1.1; % needs to be slightly larger than the deflection of the first curve 
% otherwise the deflection of the second curve goes to zero and the
% sharpness becomes infinite to meet the boundary conditions

%dy/dalpha

delta = 1;
step=0.01;
y_arr=[];
dyda_arr=[];
alpha_arr = 0.1:step:2;
for alphai = alpha_arr
    a = 0;
    b = (psi_g - delta)*sqrt(2/delta);
    c = sqrt(2*delta);
    d = 1/2;
    
    [xi, yi] = sample(alphai, delta, psi_g);
    %y_arr = [y_arr, yi];
    %dyda = ybyalpha3(alphai, delta, psi_g);
    dyda = fbyalpha(alphai ,a, b, c, d);
    %dyda_arr = [dyda_arr, dyda];
    

    f = fsample(alphai, a, b, c, d);% This does not match dyda - check this!
    y_arr = [y_arr, f];
    dfda = fbyalpha(alphai ,a, b, c, d);
    dyda_arr = [dyda_arr, dfda];
end
figure
plot(alpha_arr, y_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(y_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(alpha_arr(2:length(alpha_arr)-1), grad)
%analytic diff
plot(alpha_arr, dyda_arr);

title(['\partial y/\partial\alpha, \delta=',int2str(delta)])
legend('y', '( y(\alpha + \Delta\alpha) - y(\alpha - \Delta\alpha) ) / 2\Delta\alpha', 'analytic gradient')
xlabel('\alpha [radians/m^2]')
hold off


function [xf, yf] = sample(alpha, delta, psi_g)
delta_2 = (psi_g - delta);
alpha_2 = -alpha*delta/delta_2;
L_2 = delta_2*sqrt(2/(alpha*delta));
k_m = sqrt(2*alpha*delta);

% [x,y,~] = clothoid(alpha_2, L_2, k_m);
% xf = x(length(x));
% yf = y(length(y));

funx = @(u)cos(k_m.*u + alpha_2*u.*u/2);
xf = integral(funx, 0, L_2); 
funy = @(u)sin(k_m.*u + alpha_2*u.*u/2);
yf = integral(funy, 0, L_2);
end

function grad = ybyalpha(alpha, delta, psi_g)
% dy/dalpha
delta_2 = psi_g - delta;
alpha_2 = -alpha*delta/delta_2;
L_2 = delta_2*sqrt(2/(alpha*delta));
kappa_m = sqrt(2*alpha*delta);

fun1 = @(u)(u.*u/2).*cos(kappa_m*u + alpha_2*u.*u/2);
dybyda_2 = integral(fun1, 0, L_2);
da_2byda_1 = -delta/delta_2;

dybydL_2 = sin(3*delta_2);
dL_2byda_1 = -delta_2/sqrt(8*delta*alpha^3);

fun2 = @(u)u.*cos(kappa_m*u + alpha_2*u.*u/2);
dybydk = -integral(fun2, 0, L_2);
dkbyda = sqrt(delta/(2*alpha));

grad = dybyda_2.*da_2byda_1 + dybydL_2.*dL_2byda_1 + dybydk.*dkbyda;

end

function grad = ybyalpha2(alpha, delta, psi_g)
% dy/dalpha 
% based on DWs analysis in dydaProblem.docx
delta_2 = psi_g - delta;
alpha_2 = -alpha*delta/delta_2;
L_2 = delta_2*sqrt(2/(alpha*delta));
kappa_m = sqrt(2*alpha*delta);

fun = @(u)((1/2)*sqrt(2*delta/alpha)*u + (alpha_2/2)*u.*u).*cos(kappa_m*u + (alpha_2/2).*u.*u);

grad = integral(fun, 0, L_2) + sin(kappa_m*L_2 + (alpha_2/2)*L_2*L_2);
end

function grad = ybyalpha3(alpha, delta, psi_g)
% dy/dalpha 
% based on third page of dydaProblem.docx
k = -1;

y = (psi_g - delta)*sqrt(2/(alpha*delta));

q = @(u) sqrt(2*alpha*delta)*u + k*alpha*delta*u.*u/(2*(psi_g - delta));

fun = @(u)(sqrt(delta/(2*alpha))*u + (1/2)*(k*delta/(psi_g - delta))*u.*u).*cos(q(u));

grad = integral(fun, 0, y) + sin(q(y))*(psi_g - delta)/sqrt(2*delta*alpha*alpha*alpha);
end

function f = fsample(alpha ,a, b, c, d)
y = a + (b/sqrt(alpha));
fun = @(u) sin( c*sqrt(alpha).*u + d*alpha*u.*u );
f = integral(fun, 0, y);
end

function grad = fbyalpha(alpha ,a, b, c, d)
% df/dalpha
y = a + (b/sqrt(alpha));
fun1 = @(u) ((1/2)*c*u/sqrt(alpha) + d*u.*u).*cos( c*sqrt(alpha).*u + d*alpha*u.*u );
dHbydalpha = integral(fun1, 0 , y);

dHbydy = sin(c*sqrt(alpha)*y + d*alpha*y*y);
dybydalpha = (-1/2)*b*alpha^(-3/2);

grad = dHbydalpha + dHbydy*dybydalpha;

end


