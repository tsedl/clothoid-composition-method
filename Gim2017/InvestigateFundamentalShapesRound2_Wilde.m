% try find fundamental clothoid shape, which can only be scaled by changing
% alpha. It is not possible to reach poses where y < ymax(x, theta) with 
% two clothoids. Bisection will never converge etc


xgoal = 10; 
ygoal = 4;
psigoal = 90*pi/180;
[C, S] = fundamentalClothoid(psigoal);

% solve simulataneous equations for p1, p2 distance from intercept
p1 = xgoal - ygoal/tan(psigoal);
p2 = ygoal/sin(psigoal);

%Wilde2005 give a condition for Unsymmetric Blending
%g/h must be less than klim -> gmax or hmin
klim = (C/S)*sin(psigoal) - cos(psigoal) 
assert(klim>1)

if(p1 >p2)
    g = p1;
    hmin = g/klim
    if p2 > hmin
        fprintf('OKAY %f exceeded hmin %f \n', p2, hmin);
    else
        fprintf('FALSE %f did not exceed hmin %f \n', p2, hmin);
    end
else
    g = p2
    hmax = g/klim
    if p2 > hmin
        fprintf('OKAY %f exceeded hmin %f \n', p2, hmin);
    else
        fprintf('FALSE %f did not exceed hmin %f \n', p2, hmin);
    end
end


%It should be possible to work out the upper limit using the condition given
%by Wilde2005, AND the lower limit


% try a range of angles
% n = 90;
% kmin_arr = zeros(1,n);
% phi_max_arr = zeros(1,n);
% phi_min_arr = zeros(1,n);
% for i = 1:n
%     angle = i*pi/180;
%     [C, S] = fundamentalClothoid(angle);
%     kmin_arr(i) = (C/S)*sin(angle) - cos(angle);
%     phi_min_arr(i) = atan(kmin_arr(i));
%     phi_min_arr(i) = atan(kmin_arr(i));
% end
% subplot(2,1,1)
% plot(1:n, kmin_arr)
% xlabel('\alpha degrees')
% ylabel('kmin')
% axis([0, n, 0, 2]);
% subplot(2,1,2)
% plot(1:n, 180*phi_min_arr/pi);
% hold on 
% plot(1:n, 180*phi_max_arr/pi);
% xlabel('\phi degrees')
% ylabel('\phi')

function [x,y] = fundamentalClothoid(theta)
fun1 = @(v)cos(v)./sqrt(v);
x = (1/sqrt(2*pi))*integral(fun1, 0, theta);
fun2 = @(v)sin(v)./sqrt(v);
y = (1/sqrt(2*pi))*integral(fun2, 0, theta);
end