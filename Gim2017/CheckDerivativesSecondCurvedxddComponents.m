% Attempted to find some analytical derivatives in x and y 
% test them by comparing the output to central difference

test = uint8(all)

delta = 0.5;
psi_g = 1.1; % needs to be slightly larger than the deflection of the first curve 
% otherwise the deflection of the second curve goes to zero and the
% sharpness becomes infinite to meet the boundary conditions

alpha = 1;
step=0.01;
y_arr=[];
w_arr=[];
dydd_arr=[];
x_arr = 0.1:step:1;


% check components as well
Hxy_arr = [];
Hx_arr = [];
Hy_arr = [];   

dHbydx_arr = [];
dHbydy_arr = [];
dybydx_arr = [];

totaldfbydx_arr = [];

xconst  = x_arr(1);
a = psi_g*sqrt(2/alpha);
b = -sqrt(2/alpha);
c = sqrt(2*alpha);
d = -alpha/2;
e = psi_g;
yconst = a/sqrt(xconst) + b*sqrt(xconst);
y_arr = a./sqrt(x_arr) + b*sqrt(x_arr);

for i = 1:length(x_arr)
    x = x_arr(i);
    y = y_arr(i);
    %y = a/sqrt(x) + b*sqrt(x); 
    
    Hxy_arr = [Hxy_arr, H(a, b, c, d, e, x, y)];
    Hx_arr = [Hx_arr, H(a, b, c, d, e, x, yconst)];
    Hy_arr = [Hy_arr, H(a, b, c, d, e, xconst, y)];
   
    dHbydx_arr = [dHbydx_arr, eval_dHbydx(a, b, c, d, e, x, yconst)];
    dHbydy_arr = [dHbydy_arr, eval_dHbydy(a, b, c, d, e, xconst, y)];
    dybydx_arr = [dybydx_arr, eval_dybydx(a, b, c, d, e, x, y)]; 
    
    totaldfbydx_arr = [totaldfbydx_arr, eval_dHbydx(a, b, c, d, e, x, y) + eval_dybydx(a, b, c, d, e, x, y)*eval_dHbydy(a, b, c, d, e, x, y)];
end

if bitand(test,uint8(1)) > 0
    figure
    plot(x_arr, Hxy_arr, 'cx')
    hold on;
    plot(x_arr, Hx_arr)
    %numeric diff
    filt = [1, 0, -1];
    grad = conv(Hx_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(x_arr(2:length(x_arr)-1), grad)
    %analytic diff
    plot(x_arr, dHbydx_arr);

    title(['dH/dx components of \partial X_2/\partial\delta, \alpha=',int2str(alpha)])
    legend('H(x,y)', 'H(x, y_0)', '( H(x + \Delta x, y_0) - H(x - \Delta x, y_0) ) / 2\Delta x', 'dHbydx')
    xlabel('x [radians]')
    hold off
end

if bitand(test ,uint8(2)) > 0
figure
    plot(y_arr, Hxy_arr, 'cx')
    hold on;
    plot(y_arr, Hy_arr)

    %numeric diff
    % - STEP SIZE NO LONGER CONSTANT !!
    filt = [1, 0, -1];
    step_arr = conv(y_arr, filt, 'valid');  % valid removes the results at the ends calculated with zero padding
    grad = conv(Hy_arr, filt, 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(y_arr(2:length(y_arr)-1), grad./step_arr)

    %analytic diff
    plot(y_arr, dHbydy_arr);

    title(['dH/dy components of \partial X_2/\partial\delta, \alpha=',int2str(alpha)])
    legend('H(x,y)', 'H(x_0, y)', '( H(x_0, y + \Delta y) - H(x_0, y - \Delta y) ) / 2\Delta y', 'dHbydy')
    xlabel('y = a/sqrt(x) + b*sqrt(x) ')
    hold off
end

if bitand(test, uint8(4)) > 0
    figure
    plot(x_arr, y_arr, 'cx')
    hold on;
    plot(x_arr, y_arr)
    %numeric diff
    filt = [1, 0, -1];
    grad = conv(y_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(x_arr(2:length(y_arr)-1), grad)
    %analytic diff
    plot(x_arr, dybydx_arr);

    title(['dy/dx component of \partial X_2/\partial\delta, \alpha=',int2str(alpha)])
    legend('y(x)', 'y(x)', '( y(x + \Delta x) - y(x - \Delta x) ) / 2\Delta x', 'dybydx')
    xlabel('x [radians/m^2]')
    hold off
end

if bitand(test, uint8(8)) > 0
    figure
    plot(x_arr, Hxy_arr, 'cx')
    hold on;
    plot(x_arr, Hxy_arr)
    %numeric diff
    filt = [1, 0, -1];
    grad = conv(Hxy_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(x_arr(2:length(y_arr)-1), grad)
    
    %analytic diff
    plot(x_arr, totaldfbydx_arr);

    title(['total df/dx of \partial Y_2/\partial\delta, \alpha=',int2str(alpha)])
    legend('f(x)', 'f(x)', '( f(x + \Delta x) - f(x - \Delta x) ) / 2\Delta x', 'dfbydx(x,y)')
    xlabel('x [radians]')
    hold off
end



function f = H(a, b, c, d, e, x, y)
fun = @(u) cos( c*sqrt(x).*u + d*x*u.*u/(e-x) );
f = integral(fun, 0, y);
end

function grad = eval_dHbydx(a, b, c, d, e, x, y)
fun = @(u) (  (1/2)*c*u*x^(-1/2) + ( d*e/(e-x)^2 )*u.*u  )*(-1).*sin( c*sqrt(x).*u + d*x.*u.*u/(e-x) );
grad = integral(fun, 0, y);
end

function grad = eval_dHbydy(a, b, c, d, e, x, y)
grad = cos(c*sqrt(x)*y + d*x*y*y/(e-x));
end

function grad = eval_dybydx(a, b, c, d, e, x, y)
grad = (-1/2)*a*x^(-3/2) + (1/2)*b*x^(-1/2);
end
