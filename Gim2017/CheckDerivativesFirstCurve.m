% Attempted to find some analytical derivatives in x and y 
% test them by comparing the output to central difference
clear;


%dx/dalpha
delta = 1;

options = optimoptions(@fmincon,'Algorithm','sqp');
options = optimoptions(options,'SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true);
lb = [ ]; ub = [ ];   % No upper or lower bounds
% [x,fval] = fmincon(@objfungrad,x0,[],[],[],[],lb,ub,... 
%    @confungrad,options);

step=0.01;
x_arr=[];
dxda_arr=[];
alpha_arr = 0.1:step:2;
for alphai = alpha_arr
    [xi,~] = sample(alphai, delta);
    x_arr = [x_arr, xi];
    dxda = xbyalpha(alphai, delta);
    dxda_arr = [dxda_arr, dxda];
end
plot(alpha_arr, x_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(x_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(alpha_arr(2:length(alpha_arr)-1), grad)
%analytic diff
plot(alpha_arr, dxda_arr);

title(['\partial x/\partial\alpha, \delta=',int2str(delta)])
legend('x', '( x(\alpha + \Delta\alpha) - x(\alpha + \Delta\alpha) ) / 2\Delta\alpha', 'analytic gradient')
xlabel('\alpha [radians/m^2]')
hold off

%dx/ddelta
alpha = 1;


% analytical way
dxdd_an = xbydelta(alpha, delta);

x_arr=[];
dxdd_arr=[];
delta_arr = 0.1:step:1;
for deltai = delta_arr
    [xi,~] = sample(alpha, deltai);
    x_arr = [x_arr, xi];
    dxdd = xbydelta(alpha, deltai);
    dxdd_arr = [dxdd_arr, dxdd];
end
figure
plot(delta_arr, x_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(x_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(delta_arr(2:length(delta_arr)-1), grad)
%analytic diff
plot(delta_arr, dxdd_arr);

title(['\partial x/\partial\delta, \alpha=',int2str(alpha)])
legend('x', '( x(\delta + \Delta\delta) - x(\delta - \Delta\delta) ) / 2\Delta\delta', 'analytic gradient')
xlabel('\delta [radians]')
hold off

%===========

%dy/dalpha
delta = 1;
% analytical way
dyda_an = ybyalpha(alpha, delta);

y_arr=[];
dyda_arr=[];
alpha_arr = 0.1:step:2;
for alphai = alpha_arr
    [~,yi] = sample(alphai, delta);
    y_arr = [y_arr, yi];
    dyda = ybyalpha(alphai, delta);
    dyda_arr = [dyda_arr, dyda];
end
figure
plot(alpha_arr, y_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(y_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(alpha_arr(2:length(alpha_arr)-1), grad)
%analytic diff
plot(alpha_arr, dyda_arr);
title(['\partial y/\partial\alpha, \delta=',int2str(delta)])
legend('y', '( y(\alpha + \Delta\alpha) - y(\alpha - \Delta\alpha) ) / 2\Delta\alpha', 'analytic gradient')
xlabel('\alpha [radians/m^2]')
hold off

%dy/ddelta
alpha = 1;
% analytical way
dydd_an = ybydelta(alpha, delta);

y_arr=[];
dydd_arr=[];
delta_arr = 0.1:step:1;
for deltai = delta_arr
    [~,yi] = sample(alpha, deltai);
    y_arr = [y_arr, yi];
    dydd = ybydelta(alpha, deltai);
    dydd_arr = [dydd_arr, dydd];
end
figure
plot(delta_arr, y_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(y_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(delta_arr(2:length(delta_arr)-1), grad)
%analytic diff
plot(delta_arr, dydd_arr);
title(['\partial y/\partial\delta, \alpha=',int2str(alpha)])
legend('y', '( y(\delta + \Delta\delta) - y(\delta - \Delta\delta) ) / 2\Delta\delta', 'analytic gradient')
xlabel('\delta [radians]')
hold off


function [xf, yf] = sample(alpha, delta)
L = sqrt(2*delta/alpha);
[x,y,~] = clothoid(alpha, L, 0);
xf = x(length(x));
yf = y(length(y));
end

function grad = xbyalpha(alpha, delta)
% dx/dalpha
L = sqrt(2*delta/alpha);
fun = @(u)u.*u.*sin(alpha.*u.*u/2);
grad = -sqrt(delta/(2*alpha^3))*cos(delta) - 0.5*integral(fun, 0, L);
end

function grad = xbydelta(alpha, delta)
L = sqrt(2*delta/alpha);
grad = (1/sqrt(2*delta*alpha))*cos(delta);
end

function grad = ybyalpha(alpha, delta)
L = sqrt(2*delta/alpha);
fun = @(u)u.*u.*cos(alpha.*u.*u/2);
grad = -sqrt(delta/(2*alpha^3))*sin(delta) + 0.5*integral(fun, 0, L); 
end

function grad = ybydelta(alpha, delta)
L = sqrt(2*delta/alpha);
grad = (1/sqrt(2*delta*alpha))*sin(delta); 
end