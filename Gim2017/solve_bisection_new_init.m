% find parameters for two clothoids, where the heading is greater than atan2(y,x) 
% Use the bisection method and heuristic from  
%
% Gim, S. et al. (2017) ‘Clothoids Composition Method for Smooth Path 
% Generation of Car-Like Vehicle Navigation’, Journal of Intelligent and 
% Robotic Systems: Theory and Applications, 88(1), pp. 129–146. doi: 10.1007/s10846-017-0531-8.
%
% solve_bisection() below corresponds to Algorithm 1 Case A: two clothoids generation pp135
% boundary condition is from Figure 7 pp139 
%


    Pi = [0, 0, pi/2, 0];
    Pf = [6, 8, 0 + 30*pi/180, 0];
    
    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    
    % reflect the boundary conditions in y=0 if they are outside the
    % positive quadrant. The reflection can be reversed at the end.
    reflect=0;
    if(boundary(3)<0)
        boundary = boundary.*[1, -1, -1];
        reflect=1;
    end
    
    assert( boundary(3) > atan2(boundary(2), boundary(1)) )

    
    sol = 0;
    iter = 0;
    
    dAlpha = 0.04;
    dDeflection = 0.2;
    
    % USE THE RESULT FROM FMINCON TO INITIALIZE TO SEE IF THE ANSWER CHANGES
    load('fmincon_solution.mat', 'x')
    %------------------------------------------------------------
    alpha1 = x(1)-dAlpha/2;%Kdot1; % initial guess for sharpness
    deflection1 = x(2)-dDeflection/2; % boundary angle will always be positive due to earlier reflection
   
    
    % calculate the number of iterations required to find roots to given
    % precision individually
    na = ceil(log(abs(dAlpha)/tol)/log(2));
    nd = ceil(log(abs(dDeflection)/tol)/log(2));
    
    % check the sign changes across the specified delta
    k1 = sqrt(2*deflection1*alpha1);
    k2 = k1;

    deflection2 = boundary(3) - deflection1;

    alpha2 = k2*k2/(2*deflection2);
    S1 = sqrt(2*deflection1/alpha1);
    S2 = sqrt(2*deflection2/alpha2);

    [x, y, psi, kappa] = plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    n = length(x);

    final_pose = [x(n), y(n), psi(n)];
    D = decompose(boundary(1:3), final_pose);
   
    lambdaTopHat = D(1);
    lambda = D(2);
    
    fprintf('%d:, alpha_1 %f, delta_1 %f forward DeTopHat %f, lateral De %f\n', iter, alpha1, deflection1, lambdaTopHat, lambda)
     
     
    alpha1 = alpha1 + dAlpha;
    deflection1 = deflection1 + dDeflection;
    
    % need to ensure sharpness and deflection are always positive
    % hopefully reflecting takes care of this
    while sol==0 && iter<80
        k1 = sqrt(2*deflection1*alpha1);
        k2 = k1;
        
        % I don't think taking the absolute value is the right thing to do here!!!
        % deflection must always be positive however
        deflection2 = boundary(3) - deflection1;
        
        alpha2 = k2*k2/(2*deflection2);
        S1 = sqrt(2*deflection1/alpha1);
        S2 = sqrt(2*deflection2/alpha2);

        [x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2);%plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));%
        n = length(x);
        
        final_pose = [x(n), y(n), psi(n)];
        D = decompose(boundary(1:3), final_pose);

        DeTopHat = D(1);
        De = D(2);
        
        iter = iter+1;
        fprintf('%d:, alpha_1 %f, delta_1 %f forward DeTopHat %f, lateral De %f\n', iter, alpha1, deflection1, DeTopHat, De)
        
        if abs(De)< tol
            if DeTopHat >= 0
                sol=1;
            end
        end
        if lambda*De<0
            dAlpha = dAlpha/2;
        else
            fprintf('------------De %d, lambda %d \n',De,  lambda)
%         if iter==1
%             error('root is not bracketed')
%         end
        end
        if lambdaTopHat*DeTopHat<0
            dDeflection = dDeflection/2;
        else
            fprintf('------------DeTopHat %d, lambdaTopHat %d \n',DeTopHat,  lambdaTopHat)
%             if iter==1
%                 error('root is not bracketed')
%             end
        end
        
        lambda = De;
        lambdaTopHat = DeTopHat;
        
        dAlpha = abs(dAlpha)*sign(lambda);
        dDeflection = -abs(dDeflection)*sign(lambdaTopHat);
        
        % this check should not be neccessary because the method should
        % never check outside the initial brackets
        if alpha1 + dAlpha > 0.02
            alpha1 = alpha1 + dAlpha;
        else
            alpha1 = alpha1 - dAlpha/2;
        end
        if deflection1 + dDeflection >0 && deflection1+dDeflection<boundary(3)
            deflection1 = deflection1 + dDeflection;
        end
        
        

            
    end
    
    [xx, yy, psipsi, kappakappa] = plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    n = length(xx);
    last = [xx(n), yy(n), psipsi(n), kappakappa(n)]
    total_path_length = S1 + S2 + norm(last(1:2)-boundary(1:2))
    maximum_sharpness = max([alpha1, alpha2])



