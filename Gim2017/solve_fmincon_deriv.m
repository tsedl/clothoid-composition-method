% solve for two matched clothoids reaching a point Pf
% use this to test the benefits of analytic derivatives

function [x, y, heading, curvature, s] = solve_fmincon_deriv(Pi, Pf)

if nargin==0
    Pi = [0, 0, 0, 0];
    Pf = [8, 6, 0 + 5*pi/180, 0]; % convergence in 6 iterations. Sweet.
    % It seems that (8,6,60) is a special case where no line segment is needed
    % at the end. In general there must be a line segment. For small angles
    % there must be four clothoids. AARG
    %Pf = [6, 8, 0 + 60*pi/180, 0]; % for some reason this final position does
    % not converge!!?
    %Pf = [8, 6, 0 + 20*pi/180, 0]; % this final position converges but to wrong 
    % heading - problem with matched_pairs_ function, according to Gim 2017 this
    % case requires four clothoids anyway
end

ksi_f = decompose(Pf, Pi);

phi = atan2(ksi_f(2), ksi_f(1));
%assert(ksi_f(3) > phi);
hsqrd = (Pf(2)-Pi(2))^2 + (Pf(1)-Pi(1))^2;
better_init_sharpness = 2*(Pf(3) - Pi(3))/hsqrd;
if ksi_f(2) > 0
    alpha_1 = 0.5;%better_init_sharpness
else
    alpha_1 = -0.5;%-better_init_sharpness
end

deflection_1 = ksi_f(3)/2;

[alpha, chainage] = matched_pairs_to_simple_clothoids_new(alpha_1, deflection_1, ksi_f(3));
line_length = 0;

[xo, yo, headingo, curvatureo, so] = IntegrateClothoidSequence(alpha, chainage);
init = [alpha_1, deflection_1, line_length];
objective_np = @(p)p(1)*p(1);
constraints_np = @(p)constraints(p, ksi_f(1:3));
A = [];
b = [];
Aeq = [];
beq = [];
lb = [];
ub =[];
options = optimoptions('fmincon','Display','iter','Algorithm', 'interior-point');%,'sqp');%, 'MaxFunctionEvaluations', 5000);
[p_opt,fval,exitflag,output,lambda,grad,hessian] = fmincon(objective_np, init, A, b, Aeq, beq, lb, ub, constraints_np, options);

[alpha, chainage] = matched_pairs_to_simple_clothoids_new(p_opt(1), p_opt(2), ksi_f(3))
line_length = p_opt(3);
alpha = [alpha, 0];
chainage = [chainage, line_length];
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alpha, chainage);

figure;
plot(xo, yo, '-r');
hold on
plot(x, y, '-b');
axis equal

[c, ceq] = constraints(p_opt, ksi_f)

end


function [c, ceq] = constraints(p, goal)
[alpha, chainage] = matched_pairs_to_simple_clothoids_new(p(1), p(2), goal(3));
line_length = p(3);
alpha = [alpha, 0];
chainage = [chainage, line_length];
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alpha, chainage);
n = length(x);
final = [x(n), y(n), heading(n), curvature(n)];

difference_in_local_frame = decompose(final, goal);
c = [];%-difference_in_local_frame(2);
ceq = [final(1)-goal(1), final(2)-goal(2)];%difference_in_local_frame(1);
end


