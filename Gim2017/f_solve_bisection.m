% find parameters for two clothoids, where the heading is greater than atan2(y,x) 
% Use the bisection method and heuristic from  
%
% Gim, S. et al. (2017) ‘Clothoids Composition Method for Smooth Path 
% Generation of Car-Like Vehicle Navigation’, Journal of Intelligent and 
% Robotic Systems: Theory and Applications, 88(1), pp. 129–146. doi: 10.1007/s10846-017-0531-8.
%
% solve_bisection() below corresponds to Algorithm 1 Case A: two clothoids generation pp135
% boundary condition is from Figure 7 pp139 
%

function [sharpness, chainage, start_pose, sol] = f_solve_bisection(Pi, Pf, plot_initial, verbose)
if nargin <4
    verbose = false;
end
if nargin <3
    plot_initial = false;
end
if nargin<2
     Pi = [0, 0, pi/2, 0];
end
if nargin<1
     Pf = [3.62, 10, pi/2 - 60*pi/180, 0];
end
    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    
    % reflect the boundary conditions in y=0 if they are outside the
    % positive quadrant. The reflection can be reversed at the end.
    reflect=0;
    if(boundary(3)<0)
        boundary = boundary.*[1, -1, -1];
        reflect=1;
    end
    
    failure=0;
    if boundary(3) < atan2(boundary(2), boundary(1))
        'f_solve_bisection failed assert(boundary(3) > atan2(boundary(2), boundary(1)) )'
        failure = 1
        %assert( boundary(3) > atan2(boundary(2), boundary(1)) )
    end
     
    alpha1 = 0.1;%Kdot1; % initial guess for sharpness
    deflection1 = boundary(3)/2; % boundary angle will always be positive due to earlier reflection
    
    sol = 0;
    iter = 0;
    
    dAlpha = 0.04;
    dDeflection = 0.2;
    
    % calculate the number of iterations required to find roots to given
    % precision individually
    na = ceil(log(abs(dAlpha)/tol)/log(2));
    nd = ceil(log(abs(dDeflection)/tol)/log(2));
    
    tStart = tic;
    
    % check the sign changes across the specified delta
    k1 = sqrt(2*deflection1*alpha1);
    k2 = k1;

    deflection2 = boundary(3) - deflection1;

    alpha2 = k2*k2/(2*deflection2);
    S1 = sqrt(2*deflection1/alpha1);
    S2 = sqrt(2*deflection2/alpha2);

    if plot_initial 
        [x, y, psi, kappa] = plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    else
        [x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2);
    end
    n = length(x);

    final_pose = [x(n), y(n), psi(n)];
    D = decompose(boundary(1:3), final_pose);
   
    lambdaTopHat = D(1);
    lambda = D(2);
    
    if verbose
        fprintf('%d:, alpha_1 %f, delta_1 %f forward DeTopHat %f, lateral De %f\n', iter, alpha1, deflection1, lambdaTopHat, lambda)
    end
     
    alpha1 = alpha1 + dAlpha;
    deflection1 = deflection1 + dDeflection;
    
    % need to ensure sharpness and deflection are always positive
    % hopefully reflecting takes care of this
    while sol==0 && iter<200 && failure==0
        k1 = sqrt(2*deflection1*alpha1);
        k2 = k1;
        
        % I don't think taking the absolute value is the right thing to do here!!!
        % deflection must always be positive however
        deflection2 = boundary(3) - deflection1;
        
        alpha2 = k2*k2/(2*deflection2);
        S1 = sqrt(2*deflection1/alpha1);
        S2 = sqrt(2*deflection2/alpha2);

        [x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2);%plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));%
        n = length(x);
        
        final_pose = [x(n), y(n), psi(n)];
        D = decompose(boundary(1:3), final_pose);
        if abs(D(3))>2*eps
            if verbose
                fprintf('raise hell');
            end
        end

        DeTopHat = D(1);
        De = D(2);
        
        iter = iter+1;
        if verbose
            fprintf('%d:, alpha_1 %f, delta_1 %f forward DeTopHat %f, lateral De %f\n', iter, alpha1, deflection1, DeTopHat, De)
        end
        if abs(De)< tol
            if DeTopHat >= 0
                sol=1;
            end
        end
        if lambda*De<0
            dAlpha = dAlpha/2;
        else
            if verbose
                fprintf('------------De %d, lambda %d \n',De,  lambda)
            end
        if iter==1
            %error('root is not bracketed')
        end
        end
        if lambdaTopHat*DeTopHat<0
            dDeflection = dDeflection/2;
        else
            if verbose
                fprintf('------------DeTopHat %d, lambdaTopHat %d \n',DeTopHat,  lambdaTopHat)
            end
            if iter==1
                %error('root is not bracketed')
            end
        end
        
        lambda = De;
        lambdaTopHat = DeTopHat;
        
        dAlpha = abs(dAlpha)*sign(lambda);
        dDeflection = -abs(dDeflection)*sign(lambdaTopHat);
        
        % this check should not be neccessary because the method should
        % never check outside the initial brackets
        if alpha1 + dAlpha > 0.0002
            alpha1 = alpha1 + dAlpha;
        else
            alpha1 = alpha1 - dAlpha/2;
        end
        if deflection1 + dDeflection >0 && deflection1+dDeflection<boundary(3)
            deflection1 = deflection1 + dDeflection;
        end
        
        

            
    end
    
    T = toc(tStart);
    fprintf('Bisection converged in %d iters, taking %d seconds \n', iter, T)
    
    [xx, yy, psipsi, kappakappa] = clothoid_pair(alpha1, S1, -alpha2, S2);
    n = length(xx);
    last = [xx(n), yy(n), psipsi(n), kappakappa(n)]
    total_path_length = S1 + S2 + norm(last(1:2)-boundary(1:2))
    maximum_sharpness = max(abs([alpha1, alpha2]))
    
        %check constraints
    Df = decompose(boundary, last);
    ceq = [Df(2), Df(3), last(4)]
    c = -Df(1)
    fprintf('Reached target with lateral error %d, heading error %d, curvature error %d \n', Df(2), Df(3), last(4))
    if(all(ceq < 1e-2, 'all') && c<1e-2)==false
        sol=false
        'lateral error above threshold temporarily downgraded to a warning'
    end
    
    chainage = [S1, S2, Df(1)];
    if reflect ==false
        sharpness = [alpha1, -alpha2, 0];  
    else
        sharpness = [-alpha1, alpha2, 0];
    end
    start_pose = Pi;
    
%    plot_sequence(sharpness, chainage, 0, norm(last(1:2)-boundary(1:2)), boundary(1:3), plot_initial);
    


end