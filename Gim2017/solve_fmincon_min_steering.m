% find parameters for two clothoids, where the heading is greater than atan2(y,x) 
% Use fmin to minimize the length
%
% Gim, S. et al. (2017) ‘Clothoids Composition Method for Smooth Path 
% Generation of Car-Like Vehicle Navigation’, Journal of Intelligent and 
% Robotic Systems: Theory and Applications, 88(1), pp. 129–146. doi: 10.1007/s10846-017-0531-8.
%
% solve_bisection() below corresponds to Algorithm 1 Case A: two clothoids generation pp135
% boundary condition is from Figure 7 pp139 
% 


    Pi = [0, 0, pi/2, 0];
    Pf = [6, 8, 0 + 30*pi/180, 0];
    
    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    
    % reflect the boundary conditions in y=0 if they are outside the
    % positive quadrant. The reflection can be reversed at the end.
    reflect=0;
    if(boundary(3)<0)
        boundary = boundary.*[1, -1, -1];
        reflect=1;
    end
    
    assert( boundary(3) > atan2(boundary(2), boundary(1)) )

     
    alpha1 = 0.07;%Kdot1; % initial guess for sharpness
    deflection1 = boundary(3)/2; % boundary angle will always be positive due to earlier reflection
    
    sol = 0;
    iter = 0;
    
    dAlpha = 0.04;
    dDeflection = 0.2;
    
    % calculate the number of iterations required to find roots to given
    % precision individually
    na = ceil(log(abs(dAlpha)/tol)/log(2));
    nd = ceil(log(abs(dDeflection)/tol)/log(2));
    
    % check the sign changes across the specified delta
    k1 = sqrt(2*deflection1*alpha1);
    k2 = k1;

    deflection2 = boundary(3) - deflection1;

    alpha2 = k2*k2/(2*deflection2);
    S1 = sqrt(2*deflection1/alpha1);
    S2 = sqrt(2*deflection2/alpha2);

    [x, y, psi, kappa] = plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    n = length(x);

    final_pose = [x(n), y(n), psi(n)];
    D = decompose(boundary(1:3), final_pose);
   
    lambdaTopHat = D(1);
    lambda = D(2);
    
    fprintf('%d:, alpha_1 %f, delta_1 %f forward DeTopHat %f, lateral De %f\n', iter, alpha1, deflection1, lambdaTopHat, lambda)
    
    
    % optimization over alpha1 and deflection1 to minimze the lateral and
    % heading error and curvature error at the end
    options = optimset('Display','iter','PlotFcns',@optimplotfval, 'TolFun', 1e-5);
    init = [alpha1, deflection1];
    objective_no_param = @(x)objective(x, boundary);
    constraints_no_param = @(x)constraints(x, boundary);
    A=[]; b=[]; Aeq=[]; beq=[]; lb=[0,0]; ub=[];
    [x,fval] = fmincon(objective_no_param, init, A,b,Aeq,beq,lb,ub, constraints_no_param, options)
    alpha1 = x(1);
    deflection1 = x(2);
    [alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(alpha1, deflection1, boundary(3))
    [xx, yy, psipsi, kappakappa] = plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    n = length(xx);
    last = [xx(n), yy(n), psipsi(n), kappakappa(n)]
    total_path_length = S1 + S2 + norm(last(1:2)-boundary(1:2))
    maximum_sharpness = max([alpha1, alpha2])
    
%        % plot optimization surface
%     
%     load('bisection_solution.mat', 'alpha1', 'deflection1')
%     %hacky way of sampling close to the bisection region
%     
%     figure()
% 
%     hold on
%     centre = [alpha1, deflection1];
%     alphaRange = centre(1)*0.1:centre(1)*2/100:centre(1)*8;
%     deflectionRange = centre(2)*0.1:centre(2)*2/100:centre(2)*2.6;
%     Z = zeros( length(alphaRange), length(deflectionRange));
%     for i=1:length(alphaRange)
%         for j=1:length(deflectionRange)
%             Z(i,j) = objective_no_param([alphaRange(i),deflectionRange(j)]);
%         end
%     end
%     
%     surf(deflectionRange, alphaRange, Z)
%     %surf(alphaRange, deflectionRange, Z)
%     hold on 
%     plot3(x(1), x(2), fval, 'xr', 'MarkerSize', 24)
%     
%     load('bisection_solution.mat', 'alpha1', 'deflection1')
%     plot3(alpha1, deflection1, objective_no_param([alpha1,deflection1]),'go', 'MarkerSize', 24)
%     plot3(alpha1, deflection1, objective_no_param([alpha1,deflection1]),'rx', 'MarkerSize', 24)
%     xlabel('alpha')
%     ylabel('deflection')
%     zlabel('cost')
%     
%     figure()
%     [px, py] = gradient(Z);
%     contour(deflectionRange, alphaRange, Z)
%     hold on 
%     quiver(deflectionRange, alphaRange, px, py)
%     plot(x(1), x(2), 'xr', 'MarkerSize', 24)
%     plot(alpha1, deflection1, 'go', 'MarkerSize', 24)
%     plot(alpha1, deflection1,'rx', 'MarkerSize', 24)
%     xlabel('alpha')
%     ylabel('deflection')
%     
%     % these point derivatives should be zero if the constraint is not
%     % active or normal to the constraint boundary if they are 
%     [px_fmincon, py_fmincon] = point_derivative(objective_no_param, x(1), x(2));
%     [px_bisection, py_bisection] = point_derivative(objective_no_param, alpha1, deflection1);
    


function fval = objective(x, boundary)
% endpoint is taken care of on constraints, minimize curvature and length
[alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(x(1), x(2), boundary(3));
fval =  abs(alpha1)+ abs(alpha2); %minimum steering!!
end

function [c, ceq] = constraints(x, boundary)
    [alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(x(1), x(2), boundary(3))
    [x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2);
    n = length(x);
    final = [x(n), y(n), psi(n), kappa(n)];
    D = decompose(boundary, final);
    ceq = [D(2), D(3), final(4)];
    c = -D(1);
end

function [px, py] = point_derivative(f, x, y)
step = 0.01;
xr = [x-step, x, x+step];
yr = [y-step, y, y+step];
z = zeros(length(xr), length(yr));
for i=1:length(xr)
    for j=1:length(yr)
        z(i,j) = f([xr(i), yr(j)]);
    end
end
[px, py] = gradient(z);

end


