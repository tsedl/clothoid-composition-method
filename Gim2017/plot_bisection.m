% find parameters for two clothoids, where the heading is greater than atan2(y,x) 
% Use the bisection method and heuristic from  
%
% Gim, S. et al. (2017) ‘Clothoids Composition Method for Smooth Path 
% Generation of Car-Like Vehicle Navigation’, Journal of Intelligent and 
% Robotic Systems: Theory and Applications, 88(1), pp. 129–146. doi: 10.1007/s10846-017-0531-8.
%
% solve_bisection() below corresponds to Algorithm 1 Case A: two clothoids generation pp135
% boundary condition is from Figure 7 pp139 
%


    Pi = [0, 0, pi/2, 0];
    Pf = [6, 8, 0 + 30*pi/180, 0];
    
    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    
    % reflect the boundary conditions in y=0 if they are outside the
    % positive quadrant. The reflection can be reversed at the end.
    reflect=0;
    if(boundary(3)<0)
        boundary = boundary.*[1, -1, -1];
        reflect=1;
    end
    
    assert( boundary(3) > atan2(boundary(2), boundary(1)) )

     
    alpha1 = 0.03;%Kdot1; % initial guess for sharpness
    deflection1 = boundary(3)/2; % boundary angle will always be positive due to earlier reflection
    
    sol = 0;
    iter = 0;
    
    dAlpha = 0.04;
    dDeflection = 0.2;
    
    % calculate the number of iterations required to find roots to given
    % precision individually
    na = ceil(log(abs(dAlpha)/tol)/log(2));
    nd = ceil(log(abs(dDeflection)/tol)/log(2));
    
    % check the sign changes across the specified delta
    k1 = sqrt(2*deflection1*alpha1);
    k2 = k1;

    deflection2 = boundary(3) - deflection1;

    alpha2 = k2*k2/(2*deflection2);
    S1 = sqrt(2*deflection1/alpha1);
    S2 = sqrt(2*deflection2/alpha2);

    [x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    n = length(x);

    final_pose = [x(n), y(n), psi(n)];
    D = decompose(boundary(1:3), final_pose);
   
    lambdaTopHat = D(1);
    lambda = D(2);
    
    fprintf('%d:, alpha_1 %f, delta_1 %f forward DeTopHat %f, lateral De %f\n', iter, alpha1, deflection1, lambdaTopHat, lambda)
     
     
    alpha1 = alpha1 + dAlpha;
    deflection1 = deflection1 + dDeflection;
    
    % need to ensure sharpness and deflection are always positive
    % hopefully reflecting takes care of this
    Alpha = 0.01:1e-3:alpha1;
    Deflection = deflection1;
    S = zeros(length(Alpha), length(Deflection));
    for i=1:length(Alpha)
        for j=1:length(Deflection)
            a = Alpha(i);
            d = Deflection(j);
            k1 = sqrt(2*d*a);
            k2 = k1;

            deflection2 = boundary(3) - d;

            alpha2 = k2*k2/(2*d);
            S1 = sqrt(2*d/a);
            S2 = sqrt(2*d/a);

            [x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
            n = length(x);

            final_pose = [x(n), y(n), psi(n)];
            D = decompose(boundary(1:3), final_pose);

            lambdaTopHat = D(1);
            lambda = D(2);
            S(i,j) = lambda;
        end
    end

%surf(S);
clf
plot(Alpha', S, '-');
hold on 
plot(Alpha, zeros(1, length(Alpha)));
title(['Deflection = ',num2str(deflection1)])
xlabel('\alpha [m^{-2}]')
ylabel('Lateral Error [m]')
