% solve for two matched clothoids reaching a point Pf
% use this to test the benefits of analytic derivatives
% encountered problems with sign constraints so use relative parameters and
% >0 bounds
% init = [alpha_1, deflection_1/alpha_1];


Pi = [0, 0, 0, 0];
Pf = [8, 6, 0 + 60*pi/180, 0]; % convergence in 6 iterations. Sweet.
Pf = [6, 8, 0 + 60*pi/180, 0]; % for some reason this final position does
% not converge!!?
%Pf = [8, 6, 0 + 20*pi/180, 0]; % this final position converges but to wrong 
% heading - problem with matched_pairs_ function, according to Gim 2017 this
% case requires four clothoids anyway

ksi_f = decompose(Pf, Pi);

if ksi_f(2) > 0
    alpha_1 = 0.5;
else
    alpha_1 = -0.5;
end

deflection_1 = ksi_f(3)/2;

[alpha, chainage] = matched_pairs_to_simple_clothoids_new(alpha_1, deflection_1, ksi_f(3));

[xo, yo, headingo, curvatureo, so] = IntegrateClothoidSequence(alpha, chainage);
init = [alpha_1, deflection_1/alpha_1];
objective_np = @(p)p(1)*p(1);
constraints_np = @(p)constraints(p, ksi_f(1:3));
A=[];
b=[];
Aeq = [];
beq = [];
lb = [-inf, 0];
ub =[+inf, +inf];
options = optimoptions('fmincon','Display','iter','Algorithm', 'interior-point');%,'sqp');%, 'MaxFunctionEvaluations', 5000);
[p_opt,fval,exitflag,output,lambda,grad,hessian] = fmincon(objective_np, init, A, b, Aeq, beq, lb, ub, constraints_np, options);

[alpha, chainage] = matched_pairs_to_simple_clothoids_new(p_opt(1), p_opt(2)*p_opt(1), ksi_f(3))
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alpha, chainage);

figure;
plot(xo, yo, '-r');
hold on
plot(x, y, '-b');

[c, ceq] = constraints(p_opt, ksi_f)


function [c, ceq] = constraints(p, goal)
[alpha, chainage] = matched_pairs_to_simple_clothoids_new(p(1), p(2)*p(1), goal(3));
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alpha, chainage);
n = length(x);
final = [x(n), y(n), heading(n), curvature(n)];
c = [];
ceq = final(1:2) - goal(1:2);
end


