% find parameters for two clothoids, where the heading is greater than atan2(y,x) 
% Use fmin to minimize the length
%
% Gim, S. et al. (2017) ‘Clothoids Composition Method for Smooth Path 
% Generation of Car-Like Vehicle Navigation’, Journal of Intelligent and 
% Robotic Systems: Theory and Applications, 88(1), pp. 129–146. doi: 10.1007/s10846-017-0531-8.
%
% solve_bisection() below corresponds to Algorithm 1 Case A: two clothoids generation pp135
% boundary condition is from Figure 7 pp139 
% 
function solve_fminsearch()

    Pi = [0, 0, pi/2, 0];
    Pf = [6, 8, 0 + 30*pi/180, 0];
    
    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    
    % reflect the boundary conditions in y=0 if they are outside the
    % positive quadrant. The reflection can be reversed at the end.
    reflect=0;
    if(boundary(3)<0)
        boundary = boundary.*[1, -1, -1];
        reflect=1;
    end
    
    assert( boundary(3) > atan2(boundary(2), boundary(1)) )

     
    alpha1 = 0.03;%Kdot1; % initial guess for sharpness
    deflection1 = boundary(3)/2; % boundary angle will always be positive due to earlier reflection
    
    sol = 0;
    iter = 0;
    
    dAlpha = 0.04;
    dDeflection = 0.2;
    
    % calculate the number of iterations required to find roots to given
    % precision individually
    na = ceil(log(abs(dAlpha)/tol)/log(2));
    nd = ceil(log(abs(dDeflection)/tol)/log(2));
    
    % check the sign changes across the specified delta
    k1 = sqrt(2*deflection1*alpha1);
    k2 = k1;

    deflection2 = boundary(3) - deflection1;

    alpha2 = k2*k2/(2*deflection2);
    S1 = sqrt(2*deflection1/alpha1);
    S2 = sqrt(2*deflection2/alpha2);

    [x, y, psi, kappa] = plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    n = length(x);

    final_pose = [x(n), y(n), psi(n)];
    D = decompose(boundary(1:3), final_pose);
   
    lambdaTopHat = D(1);
    lambda = D(2);
    
    fprintf('%d:, alpha_1 %f, delta_1 %f forward DeTopHat %f, lateral De %f\n', iter, alpha1, deflection1, lambdaTopHat, lambda)
    
    
    % optimization over alpha1 and deflection1 to minimze the lateral and
    % heading error and curvature error at the end
    options = optimset('Display','iter','PlotFcns',@optimplotfval, 'TolFun', 1e-5);
    init = [alpha1, deflection1];
    objective_no_param = @(x)objective(x, boundary);
    [x,fval] = fminsearch(objective_no_param, init, options)
    alpha1 = x(1);
    deflection1 = x(2);
    [alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(alpha1, deflection1, boundary(3))
    
    [xx, yy, psipsi, kappakappa] = plot_clothoid_pair(alpha1, S1, -alpha2, S2, boundary(1:3));
    n = length(xx);
    last = [xx(n), yy(n), psipsi(n), kappakappa(n)]
    total_path_length = S1 + S2 + norm(last(1:2)-boundary(1:2))
    maximum_sharpness = max([alpha1, alpha2])
    
    % plot optimization surface
    
    load('bisection_solution.mat', 'alpha1', 'deflection1')
    %hacky way of sampling close to the bisection region
    
    figure()
    plot3(x(1), x(2), fval, 'xr')
    hold on
    x(1) = alpha1;
    x(2) = deflection1;
    alphaRange = x(1)*0.1:x(1)*2/100:x(1)*8;
    deflectionRange = x(2)*0.1:x(2)*2/100:x(2)*2.6;
    Z = zeros( length(alphaRange), length(deflectionRange));
    for i=1:length(alphaRange)
        for j=1:length(deflectionRange)
            Z(i,j) = objective_no_param([alphaRange(i),deflectionRange(j)]);
        end
    end
    
    surf(deflectionRange, alphaRange, Z)
    %surf(alphaRange, deflectionRange, Z)
    hold on 
    plot3(x(1), x(2), fval, 'xr')
    
    load('bisection_solution.mat', 'alpha1', 'deflection1')
    plot3(alpha1, deflection1, objective_no_param([alpha1,deflection1]),'go')
    xlabel('alpha')
    ylabel('deflection')
    zlabel('cost')
end

function fval = objective(x, boundary)
    [alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(x(1), x(2), boundary(3));
    [x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2);
    n = length(x);
    final = [x(n), y(n), psi(n), kappa(n)];
    D = decompose(boundary, final);
    
    % minimize final(4) as the problem must end with a straight
    % line (and zero curvature)
    fval = abs(D(2)) + abs(D(3)) + abs(final(4));
end


