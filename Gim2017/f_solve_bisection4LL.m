% find parameters for two clothoids, where the heading is greater than atan2(y,x) 
% Use the bisection method and heuristic from  
%
% Gim, S. et al. (2017) ‘Clothoids Composition Method for Smooth Path 
% Generation of Car-Like Vehicle Navigation’, Journal of Intelligent and 
% Robotic Systems: Theory and Applications, 88(1), pp. 129–146. doi: 10.1007/s10846-017-0531-8.
%
% f_solve_bisection4LL() below corresponds to Algorithm 1 Case B: four clothoids generation pp135
% boundary condition is from Figure 7 pp139 
%

function [sharpness, chainage, mid_pose, sol] = f_solve_bisection4LL(Pi, Pf, plot_initial, verbose)
    if nargin <4
        verbose = false;
    end
    if nargin <3
        plot_initial = false;
    end
    if nargin<2
         Pi = [0, 0, pi/2, 0];
    end
    if nargin<1
         Pf = [3.62, 10, pi/2 , 0];
    end
    
    Pm = (Pi + Pf)/2;
    theta_init = Pm(3) - pi;
    for degrees_m = 1:2:360 
 

        Pm(3) = theta_init + degrees_m*pi/180;
        [sharpness1, chainage1, start_pose1, sol1] = f_solve_bisection(Pi, Pm, plot_initial, verbose);
        [sharpness2, chainage2, start_pose2, sol2] = f_solve_bisection(Pm, Pf, plot_initial, verbose);
        sol = sol1 && sol2;
        sharpness = [sharpness1, sharpness2];
        chainage = [chainage1, chainage2];
        mid_pose = start_pose2;
        if sol 
            break
        end
    end
    if ~sol
        'f_solve_bisection4LL could not find a feasible angle'
    end
     
 

end