



function c = elliptical_obstacle_constraint(x, y, obs)
    %OBSTACLE_CONSTRAINT return a vector with one element per inequality
    %constraint, c \leq 0 if the constraint is satisfied
    % x,y is a set of (x,y) samples along the path
    % obs = [xo, yo, rx, ry]
    % xo, yo is the central coordinate of the obstacle in 2D
    % rx, ry is the x and y axes of the obstacle ellipse
    xo = obs(1);
    yo = obs(2);
    rx = obs(3);
    ry = obs(4);
    n = length(x);
    assert(n==length(y))
    c = zeros(1,n);
    for i=1:n
        s = [x(i), y(i)];
        delta = s - [xo, yo];
        if(abs(rx)>eps && abs(ry)>eps)
            P = eye(2,2).*[1/rx/rx, 1/ry/ry];
        else
            P = zeros(2);
        end
            
        c(i) = 1.0 - delta*P*delta';
        % try alternative linear bounding box
        
        %c(i) = sqrt(2)*radius - sum(abs(s(1:2) - position)) ;
    end 

end

% %out
% x=5
% y=45
% xo = 20
% yo=35
% rx=15
% ry=10
% elliptical_obstacle_constraint_(x, y, [xo, yo, rx, ry])
% %in
% x=21
% y=36
% elliptical_obstacle_constraint_(x, y, [xo, yo, rx, ry])