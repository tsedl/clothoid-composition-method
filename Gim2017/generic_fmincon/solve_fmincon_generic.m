% find parameters for four clothoids, where the heading is greater than atan2(y,x) 
% Use fmin to minimize the length
%

% Pi = [10, 5, 45*pi/180, 0];
% Pf = [6, 8, 0*pi/180, 0]; 
% [x, y, psi, kappa, s] = solve_fmincon_generic_(Pi, Pf);
% figure
% plot(x, y, '-')
% hold on
% plot(Pi(1), Pi(2), 'or')
% plot(Pf(1), Pf(2), 'ob')
% 
% axis equal
% xlabel('x[m]')
% ylabel('y[m]')
% n_constraints = 5;
% [x, y, ~] = DownsampleClothoid(x, y, psi, kappa, s, n_constraints);
% plot(x,y,'x')
% plot([Pi(1); Pf(1)],[Pi(2); Pf(2)], '-g')
 
function [xo, yo, psio, kappao, so] = solve_fmincon_generic(Pi, Pf)
obs_pos = [0,0];
radius=0;
    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    
    % reflect the boundary conditions in y=0 if they are outside the
    % positive quadrant. The reflection can be reversed at the end.
    reflect=0;
    if(boundary(2)<0)
        boundary = boundary.*[1, -1, -1];
        reflect=1;
    end
    
    phi = atan2(boundary(2), boundary(1));
    inflect = ( boundary(3) <= phi );
    
    half_m = 2;
    [alphaset, deflectionset, angleset] = initialise_this_many_clothoid_pairs(boundary, half_m)
    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)])
    PlotClothoidSequence(alpha, chainage, [0,0,0]);
    if( boundary(3) > atan2(boundary(2), boundary(1)) && mod(half_m, 2)~=0)
        fprintf('Warning: An inflection is required to reach this final pose, use an even number of clothoid pairs')
    end        
     
    optimize=1
    if optimize
        options = optimset('Display','iter', 'TolFun', 1e-5 , 'Algorithm', 'sqp');%,'MaxFunEvals', 1000 , 'PlotFcns', @optimplotfval);
        init = [alphaset, deflectionset, angleset];

        upperalph = (sign(alphaset) + 0.9999)*10;
        loweralph = (sign(alphaset) - 0.9999)*10;
        upperdefl = (sign(deflectionset) + 0.9999)*pi/2;
        lowerdefl = (sign(deflectionset) - 0.9999)*pi/2;
        upperangle = (sign(angleset) + 0.9999)*pi/2;
        lowerangle = (sign(angleset) - 0.9999)*pi/2;
        ub = [upperalph, upperdefl, upperangle];
        lb = [loweralph, lowerdefl, lowerangle];
        
        objective_no_param = @(x)objective(x, boundary, half_m);
        constraints_no_param = @(x)constraints(x, boundary, half_m, obs_pos, radius);
        A=[]; b=[]; Aeq=[]; beq=[];
        [x,fval] = fmincon(objective_no_param, init, A,b,Aeq,beq,lb,ub, constraints_no_param, options)
        alphaset = x(1:half_m);
        deflectionset = x(1+half_m:2*half_m);
        angleset = x(1+2*half_m:-1+3*half_m);
    end
    

    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);
    %figure
    [xx1, yy1, psipsi1, kappakappa1, ss1] = PlotClothoidSequence(alpha, chainage);
    n = length(xx1);
    final = [xx1(n), yy1(n), psipsi1(n), kappakappa1(n)];
    D = decompose(boundary, final);
    line_length = D(1);
    [xx, yy, psipsi, kappakappa, ss] = join([xx1, yy1, psipsi1, kappakappa1, ss1] , [line_length,0, 0, 0, line_length]);
    
    total_path_length = sum(chainage) + abs(line_length)
    maximum_sharpness = max(alpha)
    
    [xo, yo, psio, kappao, so] = integrate_reflected(alpha, chainage, reflect, Pi, boundary);


 end


function fval = objective(p, boundary, half_m)
% endpoint is taken care of on constraints, minimize curvature and length
%[alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(x(1), x(2), boundary(3));
  
 alphaset = p(1:half_m);
     deflectionset = p(1+half_m:2*half_m);
         angleset = p(1+2*half_m:-1+3*half_m);

%[x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2);
    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);

    dt = 0.01;
    n_samples = 1000;
    T = dt*n_samples;
    % change the average speed so the entire spline(whatever the length) is traversed in a fixed
    % time horizon/ number of samples
    v = sum(chainage)/T;
    
    if(all(sign(deflectionset)-sign(alphaset)==0))
        [x, y, psi, kappa, s] = ResampleClothoidSequence(alpha, chainage, v, dt);
        n = length(x);
        final = [x(n), y(n), psi(n), kappa(n)];
        D = decompose(boundary, final);
    else
        D = [-1, -1, -1];
    end
line_length = D(1);

 fval =  ( sum(abs(chainage)) + abs(line_length) )^2;

 %fval =  sum((alphaset).^2); % to give minmimum steering rate
end 

function [c, ceq] = constraints(p, boundary, half_m, obs_pos, radius)
%     [alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(x(1), x(2), boundary(3));

    alphaset = p(1:half_m);
    deflectionset = p(1+half_m:2*half_m);
        angleset = p(1+2*half_m:-1+3*half_m);
    
%[x, y, psi, kappa] = clothoid_pair(alpha1, S1, -alpha2, S2);
    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);
    dt = 0.01;
    n_samples = 1000;
    T = dt*n_samples;
    % change the average speed so the entire spline(whatever the length) is traversed in a fixed
    % time horizon/ number of samples
    v = sum(chainage)/T;
    [x, y, psi, kappa, s] = ResampleClothoidSequence(alpha, chainage, v, dt);
    n = length(x);
    final = [x(n), y(n), psi(n), kappa(n)];
    D = decompose(boundary, final);
    
    
    ceq = [D(2), D(3), final(4), sign(deflectionset)-sign(alphaset)];
    c = [-D(1)];

end


    





