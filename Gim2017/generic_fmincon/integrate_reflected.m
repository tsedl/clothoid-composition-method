function [x, y, psi, kappa, s] = integrate_reflected(alpha, chainage, reflect, Pi, PfRelative)
    %figure
    [xx1, yy1, psipsi1, kappakappa1, ss1] = PlotClothoidSequence(alpha, chainage);
    n = length(xx1);
    final = [xx1(n), yy1(n), psipsi1(n), kappakappa1(n)];
    D = decompose(PfRelative, final);
    line_length = D(1);
    [xx, yy, psipsi, kappakappa, ss] = join([xx1, yy1, psipsi1, kappakappa1, ss1] , [line_length,0, 0, 0, line_length]);
    
    if reflect
        Samples_rel = [xx, -yy, -psipsi, -kappakappa, ss];
    else
        Samples_rel = [xx, yy, psipsi, kappakappa, ss];
    end
    
    [x, y, psi, kappa, s] = join([Pi,0], Samples_rel);
end