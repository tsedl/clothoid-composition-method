function c = obstacle_constraint(x, y, position, radius)
    %OBSTACLE_CONSTRAINT return a vector with one element per inequality
    %constraint, c \leq 0 if the constraint is satisfied
    % x,y is a set of (x,y) samples along the path
    % position is the central coordinate of the obstacle in 2D
    % radius is the radius of the circular obstacle
    n = length(x);
    assert(n==length(y))
    c = zeros(1,n);
    for i=1:n
        s = [x(i), y(i)];
        c(i) = (radius)*(radius) - (s - position)*(s - position)' ;
        % try alternative linear bounding box
        
        %c(i) = sqrt(2)*radius - sum(abs(s(1:2) - position)) ;
    end 

end

%         % intend to put the obstacle at [3,2], so the path must divert
%         % around it
%         obs_pos = [6,-4];
%         radius = 1;
%         cpart = zeros(1,n_samples);
%         %n_actual = n
% %         for i=1:n
% %             s = [x(i), y(i)];
% %             cpart(i) = (radius)*(radius) - (s - obs_pos)*(s - obs_pos)' ;
% %         end