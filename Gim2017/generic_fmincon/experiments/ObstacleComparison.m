% objective is to compare methods for smooth planning around an single
% static obstacle, represented by a bounding circle
% The vehicle is represented by a point at it's centroid, with the
% obstacle enlarged by the largest dimension of the vehicle
% In the test case a reference path exists

% method 1 should be an optimisation using heading and curvature matching
% to reduce the number of degrees of freedom to two per clothoid pair 

% original path is also a clothoid
Pi = [0,0,pi/2,0];
Pf = [4, 4, 0, 0];
[xr, yr, headingr, curvaturer, sr] = solve_fmincon_generic(Pi, Pf);
plot(xr, yr)

