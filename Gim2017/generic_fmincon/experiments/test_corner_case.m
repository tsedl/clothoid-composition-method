% load('corner_case.mat')
[alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, angleset);
[x,y,psi, k, s] = PlotClothoidSequence(alpha, chainage, [0,0,0]);
axis equal