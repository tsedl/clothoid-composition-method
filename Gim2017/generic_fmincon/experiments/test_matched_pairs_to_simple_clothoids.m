Pi = [0, 0, 0, 0];
Pf = [8, 6, -0.175,0];%10*pi/180, 0];
   
% express the final pose in the frame of reference defined by the
% initial pose
ksi_f = decompose(Pf, Pi)
tol = 1e-5;
boundary = ksi_f;

% reflect the boundary conditions in y=0 if they are outside the
% positive quadrant. The reflection can be reversed at the end.
reflect=0;
if(boundary(2)<0)
    boundary = boundary.*[1, -1, -1];
    reflect=1;
end

half_m = 2;
[alphaset, deflectionset, angle_set] = initialise_this_many_clothoid_pairs(boundary, half_m)
[alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, angle_set)
[x,y,psi, k, s] = PlotClothoidSequence(alpha, chainage, [0,0,0]);
axis equal

