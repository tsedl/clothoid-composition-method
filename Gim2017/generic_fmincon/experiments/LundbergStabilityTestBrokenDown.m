% find parameters for four clothoids, where the heading is greater than atan2(y,x) 
% Use fmin to minimize the length
%

Pi = [5, 45, 135*pi/180, 0];
Pf = [50, 35, 45*pi/180, 0]; % - 60*pi/180


% [xo, yo, rx, ry]
obstacle_1 = [20, 35, 15, 10];
obstacle_2 = [40, 70, 15, 30];
obstacle_3 = [75, 60, 15, 10];
obs_list = [obstacle_1; obstacle_2; obstacle_3];
   
    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    
    % reflect the boundary conditions in y=0 if they are outside the
    % positive quadrant. The reflection can be reversed at the end.
    reflect=0;
    if(boundary(2)<0)
        boundary = boundary.*[1, -1, -1];
        reflect=1;
    end
    
    phi = atan2(boundary(2), boundary(1));
    inflect = ( boundary(3) <= phi );
   
    join_pose = [boundary(1)/2, boundary(2)/2, boundary(3)/2];
    
    boundary_in_frame_of_join = decompose(boundary, join_pose);
    if(boundary_in_frame_of_join(2)<0)
        boundary_in_frame_of_join = boundary_in_frame_of_join.*[1, -1, -1];
        reflect_join=1;
    else
        reflect_join=0;
    end
    
    half_m = 2;
    [alphaset1, deflectionset1, angleset1] = initialise_this_many_clothoid_pairs(join_pose, half_m);
    [alphaset2, deflectionset2, angleset2] = initialise_this_many_clothoid_pairs(boundary_in_frame_of_join, half_m);
    if reflect_join
        alphaset2 = - alphaset2;
        deflectionset2 = - deflectionset2;
        angleset2 = - angleset2;
    end
    
   alphaset = [alphaset1, alphaset2];
   deflectionset = [deflectionset1, deflectionset2];
   angleset = [angleset1, join_pose(3)-sum(angleset1),angleset2, boundary(3)-(join_pose(3)+sum(angleset2))];

    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, angleset);
    PlotClothoidSequence(alpha, chainage, [0,0,0]);
    if( boundary(3) > atan2(boundary(2), boundary(1)) && mod(half_m, 2)~=0)
        fprintf('Warning: An inflection is required to reach this final pose, use an even number of clothoid pairs')
    end        
    hold off
     
    optimize=1
    if optimize
        options = optimset('Display','iter','PlotFcns',@optimplotfval, 'TolFun', 1e-5 , 'Algorithm', 'active-set');%,'MaxFunEvals', 1000 );
        init = [alphaset, deflectionset, angleset];
        

        upperalph = (sign(alphaset) + 0.9999)*10;
        loweralph = (sign(alphaset) - 0.9999)*10;
        upperdefl = (sign(deflectionset) + 0.9999)*pi/2;
        lowerdefl = (sign(deflectionset) - 0.9999)*pi/2;
        upperangle = (sign(angleset) + 0.9999)*pi/2;
        lowerangle = (sign(angleset) - 0.9999)*pi/2;
        ub = [upperalph, upperdefl, upperangle];
        lb = [loweralph, lowerdefl, lowerangle];
        %lb=[];ub=[];
        objective_no_param = @(x)objective(x, boundary, half_m, reflect, Pi);
        constraints_no_param = @(x)constraints(x, boundary, half_m, obs_list, reflect, Pi);
        A=[]; b=[]; Aeq=[]; beq=[]; %lb=[0.001,-10, 0, -pi/4, -pi/2]; ub=[10, -0.001, pi/4, 0, pi/2];
        [x,fval] = fmincon(objective_no_param, init, A,b,Aeq,beq,lb,ub, constraints_no_param, options)
        
        alphaset = x(1:2*half_m);
        deflectionset = x(1+2*half_m:4*half_m);
        angleset = x(1+4*half_m:-1+6*half_m);
    end
    
    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, angleset);
    [xo, yo, psio, kappao, so] = integrate_reflected(alpha, chainage, reflect, Pi, boundary);

    figure
    plot(xo, yo, '-')
    hold on
    plot(Pi(1), Pi(2), 'or')
    plot(Pf(1), Pf(2), 'ob')
    plot_ellipse(obs_list(1,:));
    plot_ellipse(obs_list(2,:));
    plot_ellipse(obs_list(3,:));
    axis equal
    xlabel('x[m]')
    ylabel('y[m]')
    n_constraints = 5;
    [x, y, ~] = DownsampleClothoid(xx1, yy1, psipsi1, kappakappa1, ss1, n_constraints);
    plot(x,y,'x')
% end


function fval = objective(p, boundary, half_m, reflect, Pi)
% endpoint is taken care of on constraints, minimize curvature and length
%[alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(x(1), x(2), boundary(3));
  
    alphaset = p(1:2*half_m);
    deflectionset = p(1+2*half_m:4*half_m);
    angleset = p(1+4*half_m:-1+6*half_m);
    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, angleset);
    if(all(sign(deflectionset)-sign(alphaset)==0))
        [x, y, psi, kappa, s] = integrate_reflected(alpha, chainage, reflect, Pi);
    else
        D = [-1, -1, -1];
    end
    line_length = D(1);
    n = length(s);
    % fval =  ( sum(abs(chainage)) + abs(line_length) )^2;

    fval = s(n)*s(n) + line_length^2 + sum((alphaset).^2); % to give minmimum steering and length with equal weighting - 1m = 1radian per metre squared
end 

function [c, ceq] = constraints(p, boundary, half_m, obs_list, reflect, Pi)
    alphaset = p(1:2*half_m);
    deflectionset = p(1+2*half_m:4*half_m);
    angleset = p(1+4*half_m:-1+6*half_m);
    
  [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);
%     
    if(all(sign(deflectionset)-sign(alphaset)==0))
        
        [x, y, psi, kappa, s] = integrate_reflected(alpha, chainage, reflect, Pi);
        n_constraints = 5;
        [xx, yy, ~] = DownsampleClothoid(x,y,psi, kappa, s, n_constraints);
        n = length(x);
        final = [x(n), y(n), psi(n), kappa(n)];
        D = decompose(boundary, final);
        
        cpart = elliptical_obstacle_constraint(xx, yy, obs_list(1,:));
    else
        D = [-1, -1, -1];
        final = [D, -1];
    end
    
    ceq = [D(2), D(3), final(4), sign(deflectionset)-sign(alphaset)];
    % inequality to ensure segments never have zero length
    c = [abs([angleset,boundary(3)-sum(angleset)] - abs(deflectionset) - eps)];%, cpart,elliptical_obstacle_constraint(xx, yy, obs_list(2,:)),elliptical_obstacle_constraint(xx, yy, obs_list(3,:))];%cpart;%[-D(1), cpart];%
    %num_ineq_cons = length(c)
end

function plot_ellipse(ellipse)
    xo = ellipse(1);
    yo = ellipse(2);
    rx = ellipse(3);
    ry = ellipse(4);

    theta = linspace(0, 2*pi);
    x = xo + rx*cos(theta); 
    y = yo + ry*sin(theta);
    plot(x,y,'g-')
end

% function [x, y, psi, kappa, s] = undo_reflect(alphaset, deflectionset, angleset, reflect, Pi)
%     [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);
%     %figure
%     [xx1, yy1, psipsi1, kappakappa1, ss1] = PlotClothoidSequence(alpha, chainage);
%     n = length(xx1);
%     final = [xx1(n), yy1(n), psipsi1(n), kappakappa1(n)];
%     D = decompose(boundary, final);
%     line_length = D(1);
%     [xx, yy, psipsi, kappakappa, ss] = join([xx1, yy1, psipsi1, kappakappa1, ss1] , [line_length,0, 0, 0, line_length]);
%     
%   
%     if reflect
%         Samples_rel = [xx, -yy, -psipsi, -kappakappa, ss];
%     else
%         Samples_rel = [xx, yy, psipsi, kappakappa, ss];
%     end
%     [x, y, psi, kappa, s] = join([Pi,0], Samples_rel);
% end

    





