%The reference path is a clothoid but could be any shape - dense set of samples
% need curvature at every point - posisble to calculate from heading change
x_s = linspace(10, 20, 1000);
y_s = linspace(10, 20, 1000);
psi_s = (45*pi/180)*ones(1, 1000);
kappa_s = zeros(1,1000);

obs_pos = [5, -0.5];
obs_radius =  1; 

Pi = [0,0,0,0];%[-0.5,-0.5,5*pi/180,0];
Pf = [10,0,0,0];%[10, 10, (45*pi/180), 0]; 
% [x_ref, y_ref, psi_ref, kappa_ref, s_ref] = solve_fmincon_generic(Pi, Pf);
% save('ref_path.mat', 'x_ref', 'y_ref', 'psi_ref', 'kappa_ref', 's_ref')
%load('ref_path.mat');
x_ref = Pf(1)/1000: Pf(1)/1000: Pf(1);
y_ref = zeros(1,1000);
[x, y, psi, kappa, s] = solve_fmincon_ref_obs(Pi, x_ref, y_ref, obs_pos, obs_radius);

% x_ref = [x_ref, x_s];
% y_ref = [y_ref, y_s];
% psi_ref = [psi_ref, psi_s];
% kappa_ref = [kappa_ref, kappa_s];
% s_Ref = [s_ref, s_s];
clf
plot(x_ref, y_ref, 'c*')
hold on
plot(x, y, '-')
hold on
plot(Pi(1), Pi(2), 'or')
plot(Pf(1), Pf(2), 'ob')
viscircles(obs_pos,obs_radius)
axis equal
xlabel('x[m]')
ylabel('y[m]')
n_constraints = 20;
[x_, y_, psi_, kappa_, s_] = DownsampleClothoid(x, y, psi, kappa, s, n_constraints);
plot(x_,y_,'x')
