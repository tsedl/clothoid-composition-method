% sharpness = [-0.5, 0.2]
% chainage = [2, 2]
% v = 10
% dt = 0.01
% [x,y, heading, kappa, s] = ResampleClothoidSequence_(sharpness, chainage, v, dt)
% n_samples = length(x)
% plot(x, y, '.')
% axis equal
% xlabel('x[m]')
% ylabel('y[m]')
function [x,y, heading, curvature, s] = ResampleClothoidSequence(sharpness, chainage, v, dt)
    %RESAMPLECLOTHOIDSEQUENCE
    % return a series of pose samples for a sequence of clothoids defined by
    % the rate of change of curvature sharpness and length chainage, 
    % with curvature and heading continuity maintained between each one
    % The number of samples is guaranteed to be sum(chainage)/(v*dt)
    
    assert(length(sharpness)==length(chainage))
    q_0 = [0, 0, 0, 0];
    x = q_0(1);
    y = q_0(2);
    heading = q_0(3);
    curvature = q_0(4);
    s = 0;
    
    total_length = sum(chainage);
    average_step_size = v*dt;
    samples_total = total_length/average_step_size;
    remaining_samples = samples_total;
    for i=1:length(sharpness)
        
        if i ~= length(sharpness)
            n_samples = ceil(chainage(i)/average_step_size);
        else
            n_samples = remaining_samples;
        end
        
        
        [X,Y,Heading,Curvature, S] = clothoid_n(sharpness(i), chainage(i), q_0(4), n_samples);
        if(n_samples<1)
            %assert(n_samples>0)
            chainage
        end
            
        n = length(X);
        if(n<1)
            %assert(n>0)
            chainage
        end
        remaining_samples = remaining_samples - n;
        q_0 = [X(n), Y(n), Heading(n), Curvature(n), S(n)];

        produce_graph = 0;
        if(n_samples>=1)
            [x, y, heading, curvature, s] = join([x, y, heading, curvature, s], [X,Y,Heading,Curvature, S], produce_graph);
        end
    end
end