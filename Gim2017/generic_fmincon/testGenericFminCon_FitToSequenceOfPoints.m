%  Pi = [10, 5, 45*pi/180, 0];
%  Pf = [6, 8, 0*pi/180, 0]; 
%  [x, y, psi, kappa, s] = solve_fmincon_generic(Pi, Pf);
%  figure
%  plot(x, y, '-')
%  hold on
%  plot(Pi(1), Pi(2), 'or')
%  plot(Pf(1), Pf(2), 'ob')
%  
%  axis equal
%  xlabel('x[m]')
%  ylabel('y[m]')
%  n_constraints = 5;
%  [x, y, ~] = DownsampleClothoid(x, y, psi, kappa, s, n_constraints);
%  plot(x,y,'x')
%  plot([Pi(1); Pf(1)],[Pi(2); Pf(2)], '-g')
 

 path_corners = [[5, 2.5]', [6,2.5]', [11., 0]']; % add the goal on the end
 theta = zeros(1, length(path_corners(1,:)));
 last_point = [0,0]';
 last_theta=0;
x=[];y=[];psi=[];kappa=[];s=[];
 for i = 1:length(path_corners(1,:))
    point_i = path_corners(:,i);
    relative_pos_i = point_i - last_point;
    theta(i) = atan2(relative_pos_i(2), relative_pos_i(1));
    Pi = [last_point', last_theta, 0];
    Pf = [point_i', theta(i), 0]; 
    
    plot(Pf(1), Pf(2), 'ob')
    hold on
    [xn, yn, psin, kappan, sn] = solve_fmincon_generic(Pi, Pf);
    x = [x,xn];
    y = [y,yn];
    psi = [psi, psin];
    kappa = [kappa,kappan];
    s = [s, sn];

    last_point = point_i;
    last_theta = psin(length(psin));
    plot(x,y, 'c')
 end
 