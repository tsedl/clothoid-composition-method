function plot_ellipse(ellipse)
% does what it says on the tin
% ellipse = [x, y, rx, ry]
xo = ellipse(1);
yo = ellipse(2);
rx = ellipse(3);
ry = ellipse(4);

theta = linspace(0, 2*pi);
x = xo + rx*cos(theta); 
y = yo + ry*sin(theta);
plot(x,y,'g-')

end
