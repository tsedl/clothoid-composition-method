%% Obstacle Avoidance Test
% The tactical planner component for the warehouse forklift problem must be
% able to generate an alternative path which minimizes some cost over its
% length e.g the minimum lateral deviation from the reference path
% meet constraints on the lateral deviation
% constraints on the turning radius and the sharpness (curvature rate)
% constraints arising from an obstruction to the reference path

max_yp = 2; % 2m from path
max_yn = 2; % 2m from path
% path is a straight line along x axis
x_ref = linspace(0, 10, 1000);
y_ref = 0*ones(1, 1000);

width = 2;%2m body width

obs_pos = [3, 0];
obs_radius = 0.5;
Pi = [0,0,0];

[x, y, psi, kappa, s] = solve_fmincon_ref_obs(Pi, x_ref, y_ref, obs_pos, obs_radius);
plot_ellipse([obs_pos, obs_radius, obs_radius]);
hold on
plot(x, y);
axis equal
hold off