x_ref = linspace(0, 8.886, 1000);
y_ref = 0*ones(1, 1000);
obs_pos = [5,0];
radius = 1;
alphaset = [0.5, -0.5, -0.5, 0.5];
deflectionset = [pi/8, -pi/8, -pi/8, pi/8];
angleset = [pi/4, -pi/4, -pi/4, pi/4];
% alphaset = [1e-3, -1e-3, -1e-3, 1e-3];
% deflectionset = [1e-3, -1e-3, -1e-3, 1e-3];
% angleset = [2e-3, -2e-3, -2e-3, 2e-3];
[sharpness, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, angleset);
[x, y, heading, curvature, s] = PlotClothoidSequence(sharpness, chainage); 

Pi = [];
optimize = 1;
[xo, yo, psio, kappao, so] = solve_fmincon_ref_obs(Pi, x_ref, y_ref, obs_pos, radius, alphaset, deflectionset, angleset, optimize);
figure
h1 = plot(x, y, '-');
hold on
h2 = plot(xo, yo, '-');
hold on
% plot(Pi(1), Pi(2), 'or')
% plot(Pf(1), Pf(2), 'ob')
h3 = viscircles(obs_pos,radius);
axis equal
xlabel('x[m]')
ylabel('y[m]')
n_constraints = 20;
[x, y, ~] = DownsampleClothoid(xo, yo, psio, kappao, so, n_constraints);
h4 = plot(x,y,'xb');
h5 = plot(x_ref, y_ref, 'xg');
 
legend([h1, h2, h3, h4, h5], {'init', 'opt', 'obstacle', 'constraints', 'reference path'})
% end