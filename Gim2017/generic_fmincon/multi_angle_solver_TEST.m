
%%
Thetaf = linspace(pi/4, -pi/4, 6); % check positions around a circle
n = length(Thetaf);
Psif = zeros(1, n*n);
Xf = zeros(1, n*n);
Yf = zeros(1, n*n);
for i = 1:n
    Psif((i-1)*n+1 : i*n) = Thetaf;
    Xf((i-1)*n+1 : i*n) = 2 + 1*cos(Thetaf);
    Yf((i-1)*n+1 : i*n) = 2 + 1*sin(Thetaf);
end

Pi = [0,0,0,0];
for j = 1:n*n
    Pf = [Xf(j), Yf(j), Psif(j), 0];
    [x, y, heading, curvature, s] = function_under_test(Pi, Pf);
    end_relative = decompose(Pf, [x(length(x)), y(length(y)), heading(length(heading))]);
    assert(abs(end_relative(1))<3*eps, join(['final x', string(x(length(x))), 'does not match', string(Pf(1)) ]));
    assert(end_relative(2)<0);
    assert(abs(heading(length(heading))-Pf(3))<3*eps);
    assert(abs(curvature(length(curvature))-Pf(4))<3*eps);
end
%%
function [x, y, heading, curvature, s] = function_under_test(Pi, Pf)
[x, y, heading, curvature, s] = solve_fmincon_generic(Pi, Pf);
end