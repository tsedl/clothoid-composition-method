function [alpha, deflection, angle] = initialise_this_many_clothoid_pairs(ksi_f, half_m)
    % half_m is the number of matched clothoid pairs to be used (integer)
    % if an inflection point is needed, half_m must be an even number
    % m is the number of clothoid segments to be used
    % The inflection rule is only correct for four clothoids positive quadrant ksi_f!!
    phi = atan2(ksi_f(2), ksi_f(1));
    
    alpha_basic = 0.5;
    inflect = ( ksi_f(3) <= phi );
    mid_angle = 2*phi - 0.5*ksi_f(3); % my own invented function that seems to work ok
    
    if ksi_f(3)==0
        heading_dir = 1;
    else
        heading_dir = sign(ksi_f(2));
    end
    
    if ~inflect
        alpha = alpha_basic*heading_dir*ones(1,half_m);
        m = 2*half_m;
        deflection_step = ksi_f(3)/m;

    else
        alpha = [alpha_basic*sign(ksi_f(3))*ones(1,half_m/2), -alpha_basic*heading_dir*ones(1,half_m/2)];
        deflection_step_before = mid_angle/half_m;
        deflection_step_after = ( mid_angle - ksi_f(3))/half_m;

    end
    deflection = zeros(size(alpha));
    
    
    if ~inflect
        for i = 1:half_m
            alpha(i) = alpha_basic*heading_dir;
            deflection(i) = deflection_step;
            k1 = sqrt(2*deflection(i)*alpha(i));
            k2 = k1;
            deflection2 = deflection_step;

            alpha2 = k2*k2/2*deflection2;
            s1 = sqrt(2*deflection(i)/alpha(i));
            s2 = sqrt(2*deflection2/alpha2);
        end
    else
        for i = 1:half_m/2
            alpha(i) = alpha_basic*heading_dir;
            deflection(i) = deflection_step_before;
            k1 = sqrt(2*deflection(i)*alpha(i));
            k2 = k1;
            deflection2 = deflection_step_before;

%         alpha2 = k2*k2/2*deflection2;
%         s1 = sqrt(2*deflection(i)/alpha(i));
%         s2 = sqrt(2*deflection2/alpha2);
        end
        for i = 1+half_m/2:half_m
            alpha(i) = -alpha_basic*heading_dir;
            deflection(i) = -deflection_step_after;
            k1 = sqrt(2*deflection(i)*alpha(i));
            k2 = k1;
            deflection2 = -deflection_step_after;

%         alpha2 = k2*k2/2*deflection2;
%         s1 = sqrt(2*deflection(i)/alpha(i));
%         s2 = sqrt(2*deflection2/alpha2);
        end
        
    end
    % angle change after a pair of clothoids
    deflection_per_pair = 2*deflection;%2*deflection_step*ones(1, length(deflection));
    % the final angle is given by the boundary condition
    angle = deflection_per_pair(1:length(deflection_per_pair)-1);

end

