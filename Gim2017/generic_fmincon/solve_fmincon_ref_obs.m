% find parameters for eight clothoids which meet obstacle constraints 
% Use fmin to minimize the deviation from a reference path given by a dense
% set of x,y points : x_ref, y_ref
% Pi must contain four elements [x, y, heading, curvature] otherwise the
% start of the reference path will be used


function [x, y, psi, kappa, s] = solve_fmincon_ref_obs(Pi, x_ref, y_ref, obs_pos, radius, alphaset, deflectionset, angleset, optimize)
    
    x_ref_d = diff(x_ref);
    y_ref_d = diff(y_ref);

    psi_ref = atan2(y_ref_d, x_ref_d);
    psi_ref = [psi_ref(1), psi_ref]; % repeat first value to keep length same
    kappa_ref = diff(psi_ref);
    kappa_ref = [kappa_ref(1), kappa_ref]; % repeat first value to keep length same
    n = length(x_ref);
    if(length(Pi)<4)
        Pi = [x_ref(1), y_ref(1), psi_ref(1), kappa_ref(1)]
    end
    Pf = [x_ref(n), y_ref(n), psi_ref(n-1), kappa_ref(n-2)]
    


    % express the final pose in the frame of reference defined by the
    % initial pose
    ksi_f = decompose(Pf, Pi)
    tol = 1e-5;
    boundary = ksi_f;
    %%
    % calculate the initial params
    % if not provided

    if nargin <8 

        % reflect the boundary conditions in y=0 if they are outside the
        % positive quadrant. The reflection can be reversed at the end.
        reflect=0;
        if(boundary(2)<0)
            boundary = boundary.*[1, -1, -1];
            reflect=1;
        end

        phi = atan2(boundary(2), boundary(1));
        inflect = ( boundary(3) <= phi );

        % Need to understand how important this join pose is in the solution
        join_pose = [obs_pos + [0,radius], (0 + boundary(3))/2];

        boundary_in_frame_of_join = decompose(boundary, join_pose);
        if(boundary_in_frame_of_join(2)<0)
            boundary_in_frame_of_join = boundary_in_frame_of_join.*[1, -1, -1];
            reflect_join=1;
        else
            reflect_join=0;
        end

        half_m = 2;
        [alphaset1, deflectionset1, angleset1] = initialise_this_many_clothoid_pairs(join_pose, half_m);
        [alphaset2, deflectionset2, angleset2] = initialise_this_many_clothoid_pairs(boundary_in_frame_of_join, half_m);
        if reflect_join
            alphaset2 = - alphaset2;
            deflectionset2 = - deflectionset2;
            angleset2 = - angleset2;
        end
        alphaset = [alphaset1, alphaset2];
        deflectionset = [deflectionset1, deflectionset2];
        angleset = [angleset1, join_pose(3)-sum(angleset1), angleset2, boundary(3)-(join_pose(3)+sum(angleset2))];
        optimize=1
    %%
    else
        reflect=0;
        %reflect_join=0;
        half_m = 2; % used to indicate we are using two pairs of clohoids
    end
    
    


    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, angleset);
    PlotClothoidSequence(alpha, chainage, [0,0,0]);
    if( boundary(3) > atan2(boundary(2), boundary(1)) && mod(half_m, 2)~=0)
        fprintf('Warning: An inflection is required to reach this final pose, use an even number of clothoid pairs')
    end        
    
     
    
    init = [alphaset, deflectionset, wrapToPi(angleset)];
        
    %check if init satisfies constraints
    f_init = objective(init, Pi, boundary,reflect, half_m, x_ref, y_ref, psi_ref)
    [c_init, ceq_init] = constraints(init, Pi, boundary, reflect, half_m, obs_pos, radius)
    
    if optimize
        options = optimset('Display','iter','PlotFcns',@optimplotfval, 'Algorithm', 'interior-point','MaxFunEvals', 3000 );

        upperalph = (sign(alphaset) + 0.9999)*10;
        loweralph = (sign(alphaset) - 0.9999)*10;
        upperdefl = (sign(deflectionset) + 0.9999)*pi/2;
        lowerdefl = (sign(deflectionset) - 0.9999)*pi/2;
        upperangle = (sign(angleset) + 0.9999)*pi;
        lowerangle = (sign(angleset) - 0.9999)*pi;
        ub = [upperalph, upperdefl, upperangle];
        lb = [loweralph, lowerdefl, lowerangle];
        %lb=[];ub=[];
        objective_no_param = @(x)objective(x, Pi, boundary,reflect, half_m, x_ref, y_ref, psi_ref);
        constraints_no_param = @(x)constraints(x, Pi, boundary, reflect, half_m, obs_pos, radius);
        A=[]; b=[]; Aeq=[]; beq=[]; 
        [x,fval,exitflag] = fmincon(objective_no_param, init, A,b,Aeq,beq,lb,ub, constraints_no_param, options)
        alphaset = x(1:2*half_m);
        deflectionset = x(1+2*half_m:4*half_m);
        angleset = x(1+4*half_m:-1+6*half_m);
        %check if x satisfies constraints
        f_init = objective(x, Pi, boundary,reflect, half_m, x_ref, y_ref, psi_ref)
        [c_init, ceq_init] = constraints(x, Pi, boundary, reflect, half_m, obs_pos, radius)
        %check bounds are active
        fromupperbounds = ub - x
        fromlowerbounds = x - lb
    end
    

    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);
    %figure
    [xx1, yy1, psipsi1, kappakappa1, ss1] = PlotClothoidSequence(alpha, chainage);
    n = length(xx1);
    final = [xx1(n), yy1(n), psipsi1(n), kappakappa1(n)];
    D = decompose(boundary, final);
    line_length = D(1);
    [xx, yy, psipsi, kappakappa, ss] = join([xx1, yy1, psipsi1, kappakappa1, ss1] , [line_length,0, 0, 0, line_length]);
    
    total_path_length = sum(chainage) + abs(line_length)
    maximum_sharpness = max(alpha)
    
    if reflect
        Samples_rel = [xx, -yy, -psipsi, -kappakappa, ss];
    else
        Samples_rel = [xx, yy, psipsi, kappakappa, ss];
    end
    [x, y, psi, kappa, s] = join([Pi, 0], Samples_rel);
    



 end


function fval = objective(p, Pi, boundary,reflect, half_m, x_ref, y_ref, psi_ref)
% now minimize deviation from reference path
% boundary still needed to match heading
  
    alphaset = p(1:2*half_m);
    deflectionset = p(1+2*half_m:4*half_m);
    angleset = p(1+4*half_m:-1+6*half_m);

    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);

    [x, y, psi, kappa, s] = integrate_reflected(alpha, chainage, reflect, Pi, boundary);
    
    n_samples = 20;
    [xx, yy, ~] = DownsampleClothoid(x,y,psi, kappa, s, n_samples);
    [xx_ref, yy_ref, ~] = DownsampleClothoid(x_ref, y_ref, psi_ref, zeros(1,length(x_ref)), zeros(1,length(x_ref)), n_samples);
    d = (xx-xx_ref).*(xx-xx_ref) + (yy-yy_ref).*(yy-yy_ref);
    fval = sum(d); 
end 

function [c, ceq] = constraints(p, Pi, boundary,reflect, half_m, obs_pos, radius)
%     [alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(x(1), x(2), boundary(3));

    alphaset = p(1:2*half_m);
    deflectionset = p(1+2*half_m:4*half_m);
        angleset = p(1+4*half_m:-1+6*half_m);
    
    [alpha, chainage] = matched_pairs_to_simple_clothoids(alphaset, deflectionset, [angleset,boundary(3)-sum(angleset)]);
    [x, y, psi, kappa, s] = integrate_reflected(alpha, chainage, reflect, Pi, boundary);
    n_constraints = 20;
    [xx, yy, ~] = DownsampleClothoid(x,y,psi, kappa, s, n_constraints);
    n = length(x);
    final = [x(n), y(n), psi(n), kappa(n)];
    D = decompose(boundary, final);
        
    cpart = obstacle_constraint(xx, yy, obs_pos, radius);

    
    ceq = [ D(1), D(2), D(3), final(4), sign(deflectionset)-sign(alphaset)];
    % inequality to ensure segments never have zero length
   % is this really needed?, it seems arbitrary and wrong
   % it appears to force the angles(which are the sum of the deflection on
   % each pair) to be more than the deflection + eps. The abs around the
   % whole thing in an inequality constraint is insane because inequlaities
   % must be f(x)< 0 -> I think it should be an equality constraint if used 
%c = [abs([angleset,boundary(3)-sum(angleset)] - abs(deflectionset) - eps), cpart];%cpart;%[-D(1), cpart];%
 c = cpart;
    %num_ineq_cons = length(c)
end


    





