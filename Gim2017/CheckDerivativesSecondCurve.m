% Attempted to find some analytical derivatives in x and y 
% test them by comparing the output to central difference
all = uint8(15);
test = uint8(all)
%dx/dalpha
delta = 0.5;
psi_g = 1.1; % needs to be slightly larger than the deflection of the first curve 
% otherwise the deflection of the second curve goes to zero and the
% sharpness becomes infinite to meet the boundary conditions

options = optimoptions(@fmincon,'Algorithm','sqp');
options = optimoptions(options,'SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true);
lb = [ ]; ub = [ ];   % No upper or lower bounds
% [x,fval] = fmincon(@objfungrad,x0,[],[],[],[],lb,ub,... 
%    @confungrad,options);

%dx/dalpha
if bitand(test, uint8(1))>0
    
    step=0.01;
    x_arr=[];
    dxda_arr=[];
    alpha_arr = 0.1:step:2;
    for alphai = alpha_arr
        [xi, yi, wi] = sample(alphai, delta, psi_g);
        x_arr = [x_arr, xi];
        dxda = xbyalpha(alphai, delta, psi_g);
        dxda_arr = [dxda_arr, dxda];
    end
    plot(alpha_arr, x_arr)
    hold on;
    %numeric diff
    filt = [1, 0, -1];
    grad = conv(x_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(alpha_arr(2:length(alpha_arr)-1), grad)
    %analytic diff
    plot(alpha_arr, dxda_arr);

    title(['\partial x/\partial\alpha, \delta=',int2str(delta)])
    legend('x', '( x(\alpha + \Delta\alpha) - x(\alpha - \Delta\alpha) ) / 2\Delta\alpha', 'analytic gradient')
    xlabel('\alpha [radians/m^2]')
    hold off
end

%dx/ddelta
if bitand(test, uint8(2))>0
    step=0.01;
    alpha = 1;
    x_arr=[];
    dxdd_arr=[];
    delta_arr = 0.1:step:1;
    for deltai = delta_arr
        [xi,yi, wi] = sample(alpha, deltai, psi_g);
        x_arr = [x_arr, xi];
        dxdd = xbydelta(alpha, deltai, psi_g);
        dxdd_arr = [dxdd_arr, dxdd];
    end
    figure
    plot(delta_arr, x_arr)
    hold on;
    %numeric diff
    filt = [1, 0, -1];
    grad = conv(x_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
    plot(delta_arr(2:length(delta_arr)-1), grad)
    %analytic diff
    plot(delta_arr, dxdd_arr);

    title(['\partial x/\partial\delta, \alpha=',int2str(alpha)])
    legend('x', '( x(\delta + \Delta\delta) - x(\delta - \Delta\delta) ) / 2\Delta\delta', 'analytic gradient')
    xlabel('\delta [radians]')
    hold off
end


%===========

%dy/dalpha
if bitand(test, uint8(4))>0
delta = 1;
step=0.01;
y_arr=[];
w_arr = [];
dyda_arr=[];
alpha_arr = 0.1:step:2;
for alphai = alpha_arr
    [xi,yi, wi] = sample(alphai, delta, psi_g);
    y_arr = [y_arr, yi];
    w_arr = [w_arr, wi];
    dyda = ybyalpha(alphai, delta, psi_g);
    dyda_arr = [dyda_arr, dyda];
end
figure
plot(alpha_arr, y_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(y_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(alpha_arr(2:length(alpha_arr)-1), grad)
%analytic diff
plot(alpha_arr, dyda_arr);

plot(alpha_arr, w_arr, 'cx')

title(['\partial y/\partial\alpha, \delta=',int2str(delta)])
legend('y', '( y(\alpha + \Delta\alpha) - y(\alpha - \Delta\alpha) ) / 2\Delta\alpha', 'analytic gradient', 'w')
xlabel('\alpha [radians/m^2]')
hold off
end

%dy/ddelta
if bitand(test, uint8(8))>0
alpha = 1;
step=0.01;
y_arr=[];
w_arr=[];
dydd_arr=[];
delta_arr = 0.1:step:1;
for deltai = delta_arr
    [xi,yi, wi] = sample(alpha, deltai, psi_g);
    y_arr = [y_arr, yi];
    dydd = ybydelta(alpha, deltai, psi_g);
    dydd_arr = [dydd_arr, dydd];
    
    w_arr = [w_arr, wi];
end
figure
plot(delta_arr, y_arr)
hold on;
%numeric diff
filt = [1, 0, -1];
grad = conv(y_arr, filt/(2*step), 'valid'); % valid removes the results at the ends calculated with zero padding
plot(delta_arr(2:length(delta_arr)-1), grad)
%analytic diff
plot(delta_arr, dydd_arr);

plot(delta_arr, w_arr, 'cx')

title(['\partial y/\partial\delta, \alpha=',int2str(alpha)])
legend('y', '( y(\delta + \Delta\delta) - y(\delta - \Delta\delta) ) / 2\Delta\delta', 'analytic gradient', 'w')
xlabel('\delta [radians/m^2]')
hold off
end


function [xf, yf, wf] = sample(alpha, delta, psi_g)
delta_2 = (psi_g - delta);
alpha_2 = -alpha*delta/delta_2;
L_2 = delta_2*sqrt(2/(alpha*delta));
k_m = sqrt(2*alpha*delta);

 [x,y,~] = clothoid(alpha_2, L_2, k_m);
 xf = x(length(x));
 yf = y(length(y));

% funx = @(u)cos(k_m.*u + alpha_2*u.*u/2);
% xf = integral(funx, 0, L_2); 
% funy = @(u)sin(k_m.*u + alpha_2*u.*u/2);
% yf = integral(funy, 0, L_2);

%     a = 0;
%     b = (psi_g - delta)*sqrt(2/delta);
%     c = sqrt(2*delta);
%     d = (-1/2)*delta/(psi_g - delta);
%     y = a + (b/sqrt(alpha));
% fun = @(u) sin( c*sqrt(alpha).*u + d*alpha*u.*u );
% wf = integral(fun, 0, y);

a = psi_g*sqrt(2/alpha);
b = -sqrt(2/alpha);
c = sqrt(2*alpha);
d = -alpha/2;
e = psi_g;

x = delta;
y = a/sqrt(x) + b*sqrt(x);

fun = @(u) sin( c*sqrt(x).*u + d*x*u.*u/(e-x) );

wf = integral(fun, 0, y);
end

function grad = xbyalpha(alpha, delta, psi_g)
% dx/dalpha
delta_2 = psi_g - delta;
alpha_2 = -alpha*delta/delta_2; % if you miss the minus sign here, result is closer than with it??!?!
L_2 = delta_2*sqrt(2/(alpha*delta));

k_m = sqrt(2*alpha*delta);
fun1 = @(u)(u.*u/2).*sin(k_m.*u + alpha_2*u.*u/2);

fun2 = @(u)u.*sin(k_m.*u + alpha_2*u.*u/2);

dL_2byda = -delta_2*sqrt(1/(2*delta*alpha^3));
dxbydL_2 = cos(-delta_2);
dxbyda_2 = -integral(fun1, 0, L_2);
da_2byda = -delta/delta_2;
dxbydk = -integral(fun2, 0, L_2);
dkbyda = sqrt(delta/(2*alpha));
%grad = ( (delta-psi_g)*sqrt(2/(delta*alpha^3))*cos(psi_g - delta) - (alpha*delta/(psi_g - delta))*integral(fun, 0, L_2) ); % factor *0.27 is very close; 
grad = dxbydL_2*dL_2byda + dxbyda_2*da_2byda + dxbydk*dkbyda;
end


function grad = xbydelta(alpha, delta, psi_g)
% dx/ddelta
a = psi_g*sqrt(2/alpha);
b = -sqrt(2/alpha);
c = sqrt(2*alpha);
d = -alpha/2;
e = psi_g;

x = delta;
y = a/sqrt(x) + b*sqrt(x);

    %%

    function grad = eval_dHbydx(a, b, c, d, e, x, y)
    fun = @(u) (  (1/2)*c*u*x^(-1/2) + ( d*e/(e-x)^2 )*u.*u  )*(-1).*sin( c*sqrt(x).*u + d*x.*u.*u/(e-x) );
    grad = integral(fun, 0, y);
    end

    function grad = eval_dHbydy(a, b, c, d, e, x, y)
    grad = cos(c*sqrt(x)*y + d*x*y*y/(e-x));
    end

    function grad = eval_dybydx(a, b, c, d, e, x, y)
    grad = (-1/2)*a*x^(-3/2) + (1/2)*b*x^(-1/2);
    end
    %--

grad = eval_dHbydx(a, b, c, d, e, x, y) + eval_dybydx(a, b, c, d, e, x, y)*eval_dHbydy(a, b, c, d, e, x, y);
end

function grad = ybyalpha(alpha, delta, psi_g)
% dy/dalpha

a = 0;
b = (psi_g - delta)*sqrt(2/delta);
c = sqrt(2*delta);
d = (-1/2)*delta/(psi_g - delta);
y = a + (b/sqrt(alpha));
fun1 = @(u) ((1/2)*c*u/sqrt(alpha) + d*u.*u).*cos( c*sqrt(alpha).*u + d*alpha*u.*u );

dHbydalpha = integral(fun1, 0 , y);

dHbydy = sin(c*sqrt(alpha)*y + d*alpha*y*y);
dybydalpha = (-1/2)*b*alpha^(-3/2);

grad = dHbydalpha + dHbydy*dybydalpha;


end

function grad = ybydelta(alpha, delta, psi_g)
% dy/ddelta
a = psi_g*sqrt(2/alpha);
b = -sqrt(2/alpha);
c = sqrt(2*alpha);
d = -alpha/2;
e = psi_g;

x = delta;
y = a/sqrt(x) + b*sqrt(x);

    %function local functions?
    function grad = eval_dHbydx(a, b, c, d, e, x, y)
    fun = @(u) (  (1/2)*c*u*x^(-1/2) + ( d*e/(e-x)^2 )*u.*u  ).*cos( c*sqrt(x).*u + d*x.*u.*u/(e-x) );
    grad = integral(fun, 0, y);
    end

    function grad = eval_dHbydy(a, b, c, d, e, x, y)
    grad = sin(c*sqrt(x)*y + d*x*y*y/(e-x));
    end

    function grad = eval_dybydx(a, b, c, d, e, x, y)
    grad = (-1/2)*a*x^(-3/2) + (1/2)*b*x^(-1/2);
    end
    %--

%fun = @(u) (  (1/2)*c*u*x^(-1/2) + ( d/(e-x) + (-1)*(-1)*d*x/(e-x)^2 )*u.*u  ).*cos( c*sqrt(x).*u + d*x.*u.*u/(e-x) );
dHbydx = eval_dHbydx(a, b, c, d, e, x, y);%integral(fun, 0, y);

dHbydy = eval_dHbydy(a, b, c, d, e, x, y);%sin(c*sqrt(x)*y + d*x*y*y/(e-x));

dybydx = eval_dybydx(a, b, c, d, e, x, y);%(-1/2)*a*x^(3/2) + (1/2)*b*x^(-1/2);

grad = dHbydx + dHbydy*dybydx;



end