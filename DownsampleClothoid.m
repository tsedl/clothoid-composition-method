
% sharpness = [0.5, -0.2]
% chainage = [2, 2]
% v = 10
% dt = 0.01
% [x,y, heading, curvature, s] = ResampleClothoidSequence(sharpness, chainage, v, dt);
% n_samples = length(x)
% plot(x, y, '.')
% hold on 
% [xx, yy, hheading, ccurvature, ss] = DownsampleClothoid_(x,y,heading, curvature, s, 2);
% plot(xx, yy, 'x')
% axis equal
% xlabel('x[m]')
% ylabel('y[m]')

function [xx, yy, hheading, ccurvature, ss] = DownsampleClothoid(x,y,heading, curvature, s, n)
% This function is useful when you want less samples without affecting the
% accuracy of the final position, as you would if integrating with a
% diferent step size
xx = zeros(1, n);
yy = zeros(1, n);
hheading = zeros(1, n);
ccurvature = zeros(1, n);
ss = zeros(1, n);
m = length(x);
step = ceil(m/n);
j=n;
% doing this by iteration is slower than multiplying by a matrix with ones
% at the values you want to pick out and zeros everywhere else. Only
% implement if this function becomes the bottleneck
for i=1:step:m
    k = 1 + m - i;
    xx(j) = x(k);
    yy(j) = y(k);
    hheading(j) = heading(k);
    ccurvature(j) = curvature(k);
    ss(j) = s(k);
    j=j-1;
    
end
end

