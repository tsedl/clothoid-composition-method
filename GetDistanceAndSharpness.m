function [alpha1, alpha2, S1, S2] = GetDistanceAndSharpness(alpha1, deflection1, total_deflection)
    k1 = sqrt(2*deflection1*alpha1);
    k2 = k1;
    deflection2 = total_deflection - deflection1;
    alpha2 = k2*k2/(2*deflection2);
    S1 = sqrt(2*deflection1/alpha1);
    S2 = sqrt(2*deflection2/alpha2);
end
