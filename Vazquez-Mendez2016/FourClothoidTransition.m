% define each straight line with a pose - 2D position plus heading making a three vector
% P_1 = [0, 0, 10*pi/180];
% P_2 = [3, 3, 80*pi/180];
P_1 = [0, 0, 0*pi/180];
P_2 = [8, 6, 10*pi/180];%atan2(6,8)-2*pi/180];%1*pi/180];
 
 % a minimum radius in metres (1/maximum curvature)
R = 2;
omega = 0;
[sharpness, chainage, start_pose] = FourClothoidTransitionPP(P_1, P_2, R, omega);


subplot(2, 1, 1)
[x, y, heading, curvature, s] = PlotClothoidSequence(sharpness, chainage, start_pose);
hold on
plot_len = 1;

subplot(2,1,1)
plot(P_1(1), P_1(2), 'rx');
hold on
plot(P_2(1), P_2(2), 'gx');
axis equal
plot([P_1(1),P_1(1) + plot_len*cos(P_1(3))], [P_1(2), P_1(2)+plot_len*sin(P_1(3))], 'r');
hold on
plot([P_2(1), P_2(1) + plot_len*cos(P_2(3))], [P_2(2), P_2(2)+plot_len*sin(P_2(3))], 'g');

plot(x, y, '.')

R = 1/max(abs(curvature));
xlabel('x [m]')
ylabel('y [m]')
title(['Minimum R = ', num2str(R), 'm'])

subplot(2, 1, 2)
plot(s, curvature, '.')
xlabel('arc length s [m]')
ylabel('curvature \kappa [m^{-1}]')

function [sharpness, chainage, start_pose] = FourClothoidTransitionPP(P_1, P_2, R, omega)
%calculate transition to join two straight lines

% change in heading between entry and exit lines
heading_change = P_2(3) - P_1(3);

phi = atan2(P_2(2) - P_1(2), P_2(1) - P_1(1));
phi_hat = phi - P_1(3);

if heading_change < phi_hat
     P_m = ( P_1 + P_2 )/2;
     P_m(3) = 2*phi_hat - 0.5*heading_change;% + pi/4;
%    [R, P_m] = RadiusAndMidpointForLeftArcfollowedByRightArc(P_2);
    [sharpness1, chainage1, start_pose1] = TwoClothoidTurn(P_1, P_m, R, omega);
    [sharpness2, chainage2, start_pose2] = TwoClothoidTurn(P_m, P_2, R, omega);
    sharpness = [sharpness1, sharpness2];
    chainage = [chainage1, chainage2];
    start_pose = start_pose1;
else
    [sharpness, chainage, start_pose] = TwoClothoidTurn(P_1, P_2, R, omega);
end




end





