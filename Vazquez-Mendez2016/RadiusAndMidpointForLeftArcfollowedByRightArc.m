function [radius, midpoint] = RadiusAndMidpointForLeftArcfollowedByRightArc(P2)
%RADIUSANDMIDPOINTFORLEFTARCFOLLOWEDBYRIGHTARC largest radius and
% P1 is assumed to be (0,0,0), use P2dash = decompose(P2,P1) to get the right P2
%   appropriate midpoint and angle based on arcs
sf = sin(P2(3));
cf = cos(P2(3));
dx = P2(1);
dy = P2(2);
a = -2 + 2*cf ;
b = 2*dx*sf - 2*dy*(cf + 1) ;
c = dx^2 + dy^2;

% quadratic formula. One root is the outer intercept, one is where one
% circle is inside the other.

if(a == 0)
    radius = -c/b;
else
    assert( (b^2 - 4*a*c)>0 )
    R1 = (-b + sqrt (b^2 - 4*a*c))/2*a;
    R2 = (-b - sqrt (b^2 - 4*a*c))/2*a;
    % TODO: test the roots to find the outer one
    if R1>0%test_inside(R1, P2)==1
        radius=R1;
    elseif R2>0%test_inside(R2, P2)==1
        radius=R2;
    else
        fprintf('both roots are inside , WHAT??');
    end
end

xc1 = 0;
yc1 = radius;
xc2 = P2(1) + radius*sf;
yc2 = P2(2) - radius*cf;

sqr_dist = (dx + radius*sf)^2 + (dy - radius*(cf+1))^2
centre_distance = norm([xc1, yc1]-[xc2,yc2])
assert(norm([xc1, yc1]-[xc2,yc2])==2*radius)

A = atan2(yc2-yc1, xc2-xc1);

midpoint(1:2) = ([xc1,yc1]+[xc2,yc2])/2;
midpoint(3) = wrapToPi(A + pi/2);

end

function valid = test_inside(R, P2)
xc1 = 0;
yc1 = R;
xc2 = P2(1) + R*sf;
yc2 = P2(2) - R*cf;
if(norm([xc1,yc1]-[xc2,yc2])>R)
    valid = 1;
else
    valid=0;
end
end