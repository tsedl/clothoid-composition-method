function [Va] = LineIntersection(p1, p2)
%LINEINTERSECTION find the intersection of two lines,
%   each one described by a 2D point and an angle
p1x = p1(1);
p1y = p1(2);
th1 = p1(3);
s1 = sin(th1);
c1 = cos(th1);
p2x = p2(1);
p2y = p2(2);
th2 = p2(3);
s2 = sin(th2);
c2 = cos(th2);
dpx = p2x-p1x;
dpy = p2y-p1y;
b = (dpy*c1 - dpx*s1)/(c2*s1 - s2*c1);
a = (dpx + b*c2)/c1;

%b = ((p2y - p2y) + (s1/c2)*(p1x - p2x))/(s2 - s1);
% intersection point V
Va = p1(1:2) + a.*[c1, s1];
Vb  = p2(1:2) + b.*[c2, s2];
if(a==0)
    Va=Vb
elseif(b==0)
    Vb=Va
end
assert( norm(Va - Vb)< 10*eps); 
end

