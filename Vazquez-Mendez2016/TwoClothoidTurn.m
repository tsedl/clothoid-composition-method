function [sharpness, chainage, start_pose] = TwoClothoidTurn(P_1, P_2, R, omega)
%TWOCLOTHOIDTURN Find the parameters of two clothoids joining two points
%   with radius R and an arc of subtend omega

% signs of the clothoids
lambda_1  = 1;
lambda_2 = - lambda_1;

% change in heading between entry and exit lines (acute)
heading_change = P_2(3) - P_1(3);

% if this assertion fails, we need four clothoids
% assert( heading_change > atan2(P_2(2)-P_1(2),P_2(1)-P_1(1)) ) 

% the angle between the two lines (obtuse)
beta = pi - heading_change;

% maybe I could bring in my L = k/alpha thing here?
% tangent angle at end of clothoid - tau
tau = (pi - (omega + beta))/2;

R = R*sign(heading_change);

% length of clothoid
L = 2*R*tau;

% sharpness 
alpha = 1/(R*L); 

% compute proper distances
[X,Y,Heading,Curvature] = clothoid(alpha, L, 0);
n = length(X);
q_L = [X(n), Y(n), Heading(n), Curvature(n)];

TJ = X(n);
JH = Y(n)*tan(tau);
HV = (R + Y(n)/cos(tau))*(sin(omega/2)/sin(beta/2));
TV = TJ + JH + HV;

% find intersection somehow
% according to my algebra >.> ...
% V  = P2 + b.[c2, s2]
p1x = P_1(1);
p1y = P_1(2);
th1 = P_1(3);
s1 = sin(th1);
c1 = cos(th1);
p2x = P_2(1);
p2y = P_2(2);
th2 = P_2(3);
s2 = sin(th2);
c2 = cos(th2);
b = ((p2y - p2y) + (s1/c2)*(p1x - p2x))/(s2 - s1);
% intersection point V
OV  = P_2(1:2) + b.*[c2, s2];
OV = LineIntersection(P_1, P_2);

% some vectors
P_1_V =  OV - P_1(1:2);
P_2_V = OV - P_2(1:2);
OT1 = OV - TV * P_1_V / norm(P_1_V);
OT2 = OV - TV*P_2_V / norm(P_2_V);


n1 = [c1, s1];
n2 = [c2, s2];

straight_L1 = (OT1-P_1(1:2))*n1';
if ( straight_L1 < 0 )
    fprintf('S1 = %d, there was not enough space for P_1, choose a smaller R \n', straight_L1);
    straight_L1 =0;
end
straight_L2 = -(OT2-P_2(1:2))*n2'; 
if ( straight_L2 < 0 )
    fprintf('S2 = %d,  there was not enough space for P_2, choose a smaller R \n', straight_L2);
    straight_L2 = 0;
end
sharpness = [0, alpha, 0, -alpha, 0];
chainage = [straight_L1, L, R*omega, L, straight_L2];
start_pose = P_1;
    
    
end

