%calculate transition to join two straight lines

% define each straight line with a pose - 2D position plus heading making a three vector
% P_1 = [0, 0, 10*pi/180];
% P_2 = [3, 3, 80*pi/180];
P_1 = [0, 0, 0*pi/180];
P_2 = [8, 6, atan2(6,8)+20*pi/180];%1*pi/180];

% a minimum radius in metres (1/maximum curvature)
Rmin = 2;

% signs of the clothoids
lambda_1  = 1;
lambda_2 = - lambda_1;

% change in heading between entry and exit lines (acute)
heading_change = P_2(3) - P_1(3);
% the angle between the two lines (obtuse)
beta = pi - heading_change;
omega = 0;%heading_change/2; % wild guess, not clear how you work out the angle is
% subtended by the arc and how much by the clothoid. I say half. Now none.

% maybe I could bring in my L = k/alpha thing here?
% tangent angle at end of clothoid - tau
tau = (pi - (omega + beta))/2;

R = Rmin;

% length of clothoid
L = 2*R*tau;

% sharpness (parameter related to sharpness defining clothoids anyway)
A = (R*L)^0.5;
alpha = 1/(A*A) 

% compute proper distances
[X,Y,Heading,Curvature, S] = clothoid(alpha, L, 0);
n = length(X);
q_L = [X(n), Y(n), Heading(n), Curvature(n), S(n)];

TJ = X(n);
JH = Y(n)*tan(tau);
HV = (R + Y(n)/cos(tau))*(sin(omega/2)/sin(beta/2));
TV = TJ + JH + HV;

% find intersection somehow
% according to my algebra >.> ...
% V  = P2 + b.[c2, s2]
p1x = P_1(1);
p1y = P_1(2);
th1 = P_1(3);
s1 = sin(th1);
c1 = cos(th1);
p2x = P_2(1);
p2y = P_2(2);
th2 = P_2(3);
s2 = sin(th2);
c2 = cos(th2);
b = ((p2y - p2y) + (s1/c2)*(p1x - p2x))/(s2 - s1);
% intersection point V
OV  = P_2(1:2) + b.*[c2, s2];
OV = LineIntersection(P_1, P_2);

% some vectors
P_1_V =  OV - P_1(1:2);
P_2_V = OV - P_2(1:2);
OT1 = OV - TV * P_1_V / norm(P_1_V);
OT2 = OV - TV*P_2_V / norm(P_2_V);

plot_len = 1;
close all
figure

subplot(2,1,1)
plot(P_1(1), P_1(2), 'rx');
hold on
plot(P_2(1), P_2(2), 'gx');
axis equal
plot([P_1(1),P_1(1) + plot_len*cos(P_1(3))], [P_1(2), P_1(2)+plot_len*sin(P_1(3))], 'r');
hold on
plot([P_2(1), P_2(1) + plot_len*cos(P_2(3))], [P_2(2), P_2(2)+plot_len*sin(P_2(3))], 'g');
plot(OV(1), OV(2), 'cx');

plot(OT1(1), OT1(2), 'bo');
plot(OT2(1), OT2(2), 'bx');
xlabel('x [m]')
ylabel('y [m]')
title(['Minimum R = ', num2str(R), 'm'])

% rotate clothoids into position: at the T1 position with the curve heading
C1 = [X,Y,Heading,Curvature, S];
TPose = [OT1,th1, 0, 0];
[Xs,Ys,Headings,Curvatures,ss] = join(TPose, C1, 0);
plot(Xs, Ys);

FPose = [Xs(n), Ys(n), Headings(n), Curvatures(n), ss(n)];
%for zero arc length
[X2,Y2,Heading2,Curvature2, S2] = clothoid(-alpha, L, FPose(4));
C2 = [X2,Y2,Heading2,Curvature2, S2];
m = length(X2);
[Xs,Ys,Headings,Curvatures] = join(FPose, C2, 0);
plot(Xs, Ys);

subplot(2,1,2)
plot(X, Y, 'b-')
axis equal

subplot(2,1,2)
P1T1 = norm(OT1-P_1(1:2));
P2T2 = norm(OT2-P_2(1:2));
plot([0,P1T1],[0,0],'b-')
hold on
plot(P1T1 + 0.01*(1:n), Curvature, 'b-')
offset = P1T1 + 0.01*(n);
plot(offset + 0.01*(1:m), Curvature2, 'b-')
offset = P1T1 + 0.01*(n + m);
plot([offset,offset+P2T2],[0,0],'b-')
xlabel('arc length s [m]')
ylabel('curvature \kappa [m^{-1}]')
axis([0,12.1,-0.1,0.6])

[sharpness, chainage, start_pose] = TwoClothoidTurn(P_1, P_2, R, omega)
hold on
subplot(2, 1, 1)
[x, y, heading, curvature, s] = PlotClothoidSequence(sharpness, chainage, start_pose);

figure()
subplot(2,1,1)
plot(P_1(1), P_1(2), 'rx');
hold on
plot(P_2(1), P_2(2), 'gx');
axis equal
plot([P_1(1),P_1(1) + plot_len*cos(P_1(3))], [P_1(2), P_1(2)+plot_len*sin(P_1(3))], 'r');
hold on
plot([P_2(1), P_2(1) + plot_len*cos(P_2(3))], [P_2(2), P_2(2)+plot_len*sin(P_2(3))], 'g');
plot(OV(1), OV(2), 'cx');
plot(x, y, '.')

plot(OT1(1), OT1(2), 'bo');
plot(OT2(1), OT2(2), 'bx');
xlabel('x [m]')
ylabel('y [m]')
title(['Minimum R = ', num2str(R), 'm'])

subplot(2, 1, 2)
plot(s, curvature, '.')
xlabel('arc length s [m]')
ylabel('curvature \kappa [m^{-1}]')





