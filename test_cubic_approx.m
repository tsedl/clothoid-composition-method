% plot approximatiuon error with path length for two values of sharpness \alpha
% Errors increase with increasing L/2R, and do so at a lower rate  
% for larger \alpha 
% The \alpha relationship is not obvious, curve fitting may be useful here
% for alpha = 0.001:0.05:0.5
%     test_cubic_approx_(alpha)
% end
alpha1 = 0.001
test_cubic_approx_(alpha1)
alpha2 = 0.5
test_cubic_approx_(alpha2)


function test_cubic_approx_(alpha)

R_over_2L_limit = 0.5;
L = 1/sqrt(2*alpha*R_over_2L_limit);

figure
[x, y, psi, kappa, t] = cubic_clothoid(alpha, L, 0);
subplot(2, 1, 1)
plot(x, y)
[xc, yc, psic, kappac, tc] = clothoid(alpha, L, 0);
hold on 
plot(xc, yc)
xlabel('x')
ylabel('y')
legend('cubic approx', 'integrated clothoid', 'Location','North')

r = 1./kappa;
p = t./(2*r);

error = sqrt( (x-xc).*(x-xc) + (y-yc).*(y-yc) );

e_max = max(error);
L;
e_max_over_L_Sqr = e_max/(L*L)

subplot(2, 1, 2)
plot(p, error)
xlabel('L/2R')
ylabel('error')
title1 = num2str(alpha);
title(['\alpha = ', title1] )
end


    
