% This function uses the sharpness and deflection of the first clothoid in
% a pair which terminates at a given angle, and computes the implied
% sharpness and deflection of the second clothoid in the pair sop that it
% reaches the specified angle. It should work with any number of pairs, one
% and two have been tested extensively

%%
name = 'ONE pair test +'
alpha1set = 0.5;
deflection1set = 0.5; 
thetaset = 1;
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    assert_dp_equal(heading(s==sum(chainageset(1:2*i))), sum(thetaset(1:i)));
    assert(curvature(s==sum(chainageset(1:2*i)))==0);
end

fprintf([name,' PASSED!\r\n']);

%%
name = 'ONE pair test - '
alpha1set = -0.5;
deflection1set = -0.5; 
thetaset = -1;
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    assert_dp_equal(heading(s==sum(chainageset(1:2*i))), sum(thetaset(1:i)));
    assert(curvature(s==sum(chainageset(1:2*i)))==0);
end

fprintf([name,' PASSED!\r\n']);

%%
%this test fails for all implementations because it is impossible
name = 'ONE pair test opposing sign theta'
alpha1set = 0.5;
deflection1set = 0.5; 
thetaset = -1;
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    try
        assert_dp_equal(heading(s==sum(chainageset(1:2*i))), sum(thetaset(1:i)));
        assert(curvature(s==sum(chainageset(1:2*i)))==0);
    catch ME
        if strcmp(ME.identifier,'MATLAB:assertion:failed')
            fprintf([name,' caught expected exception ', ME.identifier, ' on impossible test \r\n' ]);
            stack = ME.stack
            fprintf('Paused >> \r\n');
            pause
        else
            rethrow(ME)
        end
    end
end

fprintf([name,' PASSED!\r\n']);

%%
%this test fails for all implementations because it is impossible
name = 'ONE pair test opposing sign delta'
alpha1set = 0.5;
deflection1set = -0.5; 
thetaset = 1;
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    
    try
        assert_dp_equal(heading(s==sum(chainageset(1:2*i))), sum(thetaset(1:i)));
        assert(curvature(s==sum(chainageset(1:2*i)))==0);
    catch ME
        if strcmp(ME.identifier,'MATLAB:assertion:failed')
            fprintf([name,' caught expected exception ', ME.identifier, ' on impossible test \r\n' ]);
            stack = ME.stack
            fprintf('Paused >> \r\n');
            pause
        else
            rethrow(ME)
        end
    end
end

fprintf([name,' PASSED!\r\n']);

%%
name = 'ONE pair test opposing sign alpha '
alpha1set = -0.5;
deflection1set = 0.5; 
thetaset = 1;
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    try
        assert_dp_equal(heading(s==sum(chainageset(1:2*i))), sum(thetaset(1:i)));
        assert(curvature(s==sum(chainageset(1:2*i)))==0);
    catch ME
        if strcmp(ME.identifier,'MATLAB:assertion:failed')
            fprintf([name,' caught expected exception ', ME.identifier, ' on impossible test \r\n' ]);
            stack = ME.stack
            fprintf('Paused >> \r\n');
            pause
        else
            rethrow(ME)
        end
    end
end

fprintf([name,' PASSED!\r\n']);

%%
% this test demonstrates theta is taken as the deflection over the whole pair rather than an absolute angle 
clear;
name = 'TWO pair test theta>phi'
alpha1set = [0.5, 0.5];
deflection1set = [0.5, 0.5]; 
thetaset = [1, 1];
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    assert_dp_equal(heading(s==sum(chainageset(1:2*i))), sum(thetaset(1:i)));
    assert(curvature(s==sum(chainageset(1:2*i)))==0);
end
fprintf([name,' PASSED!\r\n']);

%%
clear;
name = 'TWO pair test theta<phi'
alpha1set = [0.5, -0.5];
deflection1set = [0.5, -0.5]; 
thetaset = [1, -1];
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    assert_dp_equal(heading(k), sum(thetaset(1:i)));
    assert(curvature(k)==0);
end
fprintf([name,' PASSED!\r\n']);

%%
clear;
name = 'THREE pair test theta>phi'
alpha1set = [0.5, 0.5, 0.5];
deflection1set = [0.5, 0.5, 0.5]; 
thetaset = [1, 1, 1];
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
[x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
for i = 1:length(thetaset)
    k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
    assert_dp_equal(heading(k), sum(thetaset(1:i)));
    assert(curvature(k)==0);
end
fprintf([name,' PASSED!\r\n']);

%%
clear;
name = 'N pair test theta>phi'
for  n = 4:10
    alpha1set = 0.5*ones(1, n);
    deflection1set = 0.5*ones(1, n);
    thetaset = 1*ones(1, n);
    [alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
    [x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
    for i = 1:length(thetaset)
        k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
        assert_dp_equal( heading(k), wrapToPi(sum(thetaset(1:i))) );
        assert(curvature(k)==0);
    end
end
fprintf([name,' PASSED!\r\n']);

%%
clear;
name = 'N pair test theta<phi'

for  n = 4:10
    alpha1set = 0.5*alternating_sign_ones(1, n);
    deflection1set = 0.5*alternating_sign_ones(1, n);
    thetaset = 1*alternating_sign_ones(1, n);
    [alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
    [x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
    for i = 1:length(thetaset)
        k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
        assert_dp_equal( heading(k), wrapToPi(sum(thetaset(1:i))) );
        assert(curvature(k)==0);
    end
end
fprintf([name,' PASSED!\r\n']);

%%
clear;
name = 'Angle test'

n = 4;
for scale = 0.1:2.1
    alpha1set = 0.5*alternating_sign_ones(1, n);
    deflection1set = scale*alternating_sign_ones(1, n);
    thetaset = 2*scale*alternating_sign_ones(1, n);
    [alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha1set, deflection1set, thetaset);
    [x, y, heading, curvature, s] = IntegrateClothoidSequence(alphaset, chainageset);
    for i = 1:length(thetaset)
        k = [s==sum(chainageset(1:2*i))]; % s is monotonic so the exact value will always be present at a unique index
        assert_dp_equal( heading(k), wrapToPi(sum(thetaset(1:i))) );
        assert(curvature(k)==0);
    end
end

fprintf([name,' PASSED!\r\n']);


%%
function assert_dp_equal(a, b)
% forced to increase this threshold for higher number of clothoids. Saw an
% error exactly the size of 2*eps for n=8
assert(abs(a-b)<3*eps);
end

%%
function alternating = alternating_sign_ones(i, j)
alternating = zeros(i,j);
alternating(1) = 1;
if(i*j>1)
    for k = 2:i*j
        alternating(k) = -1*alternating(k-1);
    end
end
end
%%
function [alphaset, chainageset] = matched_pairs_to_simple_clothoids_LOCAL(alpha, deflection, theta)
[alphaset, chainageset] = matched_pairs_to_simple_clothoids_new(alpha, deflection, theta);
end