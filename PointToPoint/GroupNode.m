classdef GroupNode
    %GROUPNODE has an id, position vector, a list of groups, a list of
    %links
    %   Offers AddLink() to add the id of another linked GroupNode to the 
    %   list 
    
    properties
        id
        pos
        groups
        links
    end
    
    methods
        function obj = GroupNode(id, pos, groups)
            obj.id = id;
            obj.pos = pos;
            obj.groups = groups;
            obj.links = [];
        end
        
        function obj = AddLink(obj, id)
            obj.links = [obj.links, id];
        end
        
        function result = IsMember(obj, group_id)
            result = 0;
            for i = 1:length(obj.groups)
                if obj.groups(i)==group_id
                    result = 1;
                end
            end
                    
        end
    end
end

