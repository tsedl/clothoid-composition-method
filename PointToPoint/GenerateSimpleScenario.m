
%%%%
% early test scenario - excusion left
region1 = [0.0, 5.0, -5.0, 5.0];
region2 = [0.0, 11.0, 2.5, 5.0];
region3 = [6.0, 11.0, -5.0, 5.0];
region_list = [region1; region2; region3];
nr = size(region_list, 1);
start = [0,0,0];
goal = [11,0,0*pi/180];
n_pieces = nr;
H = eye(n_pieces, nr);%no need to hard code it it should be correctly
waypoint_list = [0.0, 0.0, 0.0;
    5.0, 2.5, 0.0
    6.0, 2.5, 0.0
    11.0, 0.0, 0.0];
% computed below
grid_compound = CompoundRegion(region_list);
% [waypoint_list, H] = GetWaypointsAndRegionAssignment(grid_compound, start, goal );
waypoint_list_no_duplicates = waypoint_list(1,1:3);
H_no_duplicates = H(1, :);
for i = 2:(size(waypoint_list, 1)-1)
    arc_length = norm(waypoint_list_no_duplicates(end, 1:3) - waypoint_list(i,1:3));
    if arc_length > eps
        waypoint_list_no_duplicates = [waypoint_list_no_duplicates; waypoint_list(i,1:3)];
        H_no_duplicates = [H_no_duplicates; H(i, :)]; 
    end
end
waypoint_list_no_duplicates = [waypoint_list_no_duplicates; waypoint_list(end,1:3)];
H_no_duplicates(end, :) =  H(end, :); 

%%%%
load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
n_cells = length(waypoint_data);
waypoint_data{n_cells+1} = waypoint_list_no_duplicates
region_data{n_cells+1} = region_list
H_data{n_cells+1} = H_no_duplicates
save('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')