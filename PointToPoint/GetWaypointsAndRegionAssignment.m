function [waypoint_list, H] = GetWaypointsAndRegionAssignment(compound_region, start, goal )
    if nargin <1
        clear
        load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
        n_cells = length(waypoint_data)
%%%%%%%%%%%%% run with no args and set the problem_id to find H for one example
        problem_id = 3
%%%%%%%%%  Then call  
%     H_data{problem_id} = H
%     save('waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
%%%%%%%%% to save the results to a mat file
        waypoint_list = waypoint_data{problem_id}
        start = waypoint_list(1, 1:3)
        goal = waypoint_list(end, 1:3)
        region_list = region_data{problem_id}
        compound_region = CompoundRegion(region_list);
    end
    region_list = compound_region.polygon_region_list;
    
    directed_graph = compound_region.GetExtremalPointGraph(start(1:2), goal(1:2));
    adjacency = directed_graph.GetAdjacencyMatrix();

    current_waypoint = directed_graph.node_map(1);
    % use dummy item 1: [2, 56.1] to initialize the map - id 1 is overwritten later
    forward_lookup = containers.Map(1, [2, 56.1]); 
    is_open = containers.Map(directed_graph.node_map.keys, ones(size(directed_graph.node_map.keys)));
    duplicate_count = 0;
    while(norm(current_waypoint.pos(1:2) - goal(1:2))>eps)
        if is_open(current_waypoint.id) == 0
            'already searched this node!! Do something else here!'
        end
        neighbour_list = current_waypoint.links;
        best_cost = 999;
        best_to_id = 0;
        for i = 1:length(neighbour_list)
            to_id = neighbour_list(i);
            cost = adjacency(current_waypoint.id, to_id) + norm(directed_graph.node_map(to_id).pos(1:2) - goal(1:2));
            if cost < best_cost && is_open(to_id) == 1
                best_cost = cost;
                best_to_id = to_id;
            end
        end
        forward_lookup(current_waypoint.id) = [best_to_id, best_cost];
        is_open(current_waypoint.id) = 0;
        current_waypoint = directed_graph.node_map(best_to_id);
        
    end


    waypoint_list = zeros(forward_lookup.Count+1, 3);
    H = zeros(forward_lookup.Count, size(region_list, 1)); 
    path_id_list = zeros(1, forward_lookup.Count+1);
    id = 1;
    path_id_list(1) = id;
    waypoint_list(1, 1:3) = start;
    next_id_cost_pair = forward_lookup(id);
    for k = 2:forward_lookup.Count
        id = next_id_cost_pair(1);
        path_id_list(k) = id;
        waypoint_list(k, 1:2) = directed_graph.node_map(id).pos;
        next_id_cost_pair = forward_lookup(id);
    end
    waypoint_list(end, 1:3) = goal; % why doesnt the goal appear in the lookup naturally??
    path_id_list(end) = next_id_cost_pair(1);
    
    for p = 1:length(path_id_list)-1
        id = path_id_list(p);
        groups = directed_graph.node_map(id).groups;
        next_id = path_id_list(p+1);
        next_groups = directed_graph.node_map(next_id).groups;
        for m = 1:length(groups)
            group_id = groups(m);
            for n = 1:length(next_groups)
                next_group_id = next_groups(n);
                if group_id == next_group_id
                   H(p, group_id) = 1;
                end
            end
        end        
    end
    
    
   %%%%%%%%%  Then call  
%     H_data{problem_id} = H
%     save('waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
%%%%%%%%% to save the results to a mat file 
    
end