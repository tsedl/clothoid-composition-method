                                            First-order      Norm of
 Iter F-count            f(x)  Feasibility   optimality         step
    0      37    8.906302e+02    2.099e+00    1.777e+02
    1      74    7.337526e+02    3.875e+00    4.997e+02    8.923e+00
    2     111    7.163867e+02    8.401e+00    4.625e+02    3.171e+00
    3     148    6.914666e+02    7.219e+00    3.495e+02    2.323e+00
    4     185    6.767803e+02    5.971e+00    1.300e+02    1.588e+00
    5     222    6.732357e+02    4.783e+00    9.122e+01    8.280e-01
    6     261    6.779495e+02    3.936e+00    3.492e+02    1.223e+00
    7     300    6.891803e+02    3.292e+00    5.119e+02    1.841e+00
    8     338    7.044110e+02    2.036e+00    7.441e+01    2.504e+00
    9     375    7.132093e+02    1.671e+00    7.794e+01    1.297e+00
   10     414    7.187527e+02    1.498e+00    8.190e+01    7.475e-01
   11     451    7.238996e+02    1.348e+00    8.756e+01    5.710e-01
   12     489    7.303478e+02    1.186e+00    9.397e+01    7.912e-01
   13     526    7.423745e+02    9.218e-01    1.693e+02    1.084e+00
   14     563    7.490516e+02    7.548e-01    1.646e+02    8.380e-01
   15     600    7.614412e+02    4.848e-01    1.604e+02    9.963e-01
   16     637    7.729030e+02    2.248e-01    9.373e+01    7.248e-01
   17     675    7.783590e+02    1.175e-01    3.179e+01    5.869e-01
   18     713    7.809812e+02    6.111e-02    1.588e+01    3.317e-01
   19     750    7.836869e+02    3.455e-02    1.677e+01    6.093e-01
   20     787    7.837126e+02    5.808e-03    2.015e+01    2.073e-01
   21     825    7.833508e+02    2.787e-03    2.509e+01    4.531e-01
   22     863    7.832277e+02    6.471e-04    4.577e+00    2.058e-01
   23     900    7.832016e+02    9.854e-04    5.296e+00    1.155e-01
   24     938    7.831916e+02    1.649e-05    1.474e+00    8.259e-02
   25     975    7.825418e+02    1.661e-03    1.628e+00    9.981e-02
   26    1012    7.825208e+02    2.560e-03    1.967e+00    1.052e-01
   27    1049    7.825177e+02    4.623e-04    2.535e+00    4.197e-02
   28    1086    7.825186e+02    4.340e-05    1.299e+00    1.034e-02
   29    1123    7.825174e+02    2.127e-04    6.585e-01    1.632e-02
   30    1160    7.823894e+02    4.067e-04    6.398e-01    1.806e-02

                                            First-order      Norm of
 Iter F-count            f(x)  Feasibility   optimality         step
   31    1197    7.823884e+02    4.175e-05    6.246e-01    1.071e-02
   32    1234    7.823881e+02    2.604e-05    7.587e-01    1.227e-02
   33    1271    7.823879e+02    8.302e-06    7.301e-01    8.979e-03
   34    1308    7.823878e+02    1.810e-06    2.475e-01    5.764e-03
   35    1345    7.823878e+02    4.810e-07    1.291e-01    2.397e-03
   36    1382    7.823622e+02    1.672e-05    1.195e-01    3.956e-03
   37    1419    7.823622e+02    7.282e-07    1.922e-01    2.087e-03
   38    1456    7.823621e+02    4.545e-06    3.046e-01    4.754e-03
   39    1493    7.823621e+02    4.695e-07    1.479e-01    1.815e-03
   40    1530    7.823621e+02    8.466e-08    3.297e-02    5.940e-04
   41    1567    7.823621e+02    1.109e-09    3.211e-03    9.414e-05
   42    1604    7.823557e+02    1.112e-06    1.468e-02    8.421e-04
   43    1641    7.823557e+02    6.076e-09    4.628e-03    6.397e-05
   44    1678    7.823557e+02    4.673e-10    4.015e-04    2.913e-05
   45    1715    7.823557e+02    1.326e-12    1.393e-04    2.665e-06
   46    1753    7.823557e+02    8.445e-13    1.697e-05    1.485e-06

<a href = "matlab: helpview([docroot '/toolbox/optim/msg_csh/optim_msg_csh.map'],'local_min_found','CSHelpWindow');">Local minimum found that satisfies the constraints</a>.

Optimization completed because the objective function is non-decreasing in 
<a href = "matlab: helpview([docroot '/toolbox/optim/msg_csh/optim_msg_csh.map'],'feasible_directions','CSHelpWindow');">feasible directions</a>, to within the default value of the <a href = "matlab: helpview([docroot '/toolbox/optim/msg_csh/optim_msg_csh.map'],'optimality_tolerance_msgcsh','CSHelpWindow');">optimality tolerance</a>,
and constraints are satisfied to within the default value of the <a href = "matlab: helpview([docroot '/toolbox/optim/msg_csh/optim_msg_csh.map'],'constraint_tolerance','CSHelpWindow');">constraint tolerance</a>.

<<a href = "matlab: createExitMsg('barrier',1.000000e+00,true,true,'fmincon',4.848117e-07,'default',1.000000e-06,4.022759e-13,'default',1.000000e-06,0.000000e+00,'',0.000000e+00);">stopping criteria details</a>>

Elapsed time is 11.008114 seconds.

c =

  Columns 1 through 8

   -7.5000  -17.5000   -5.0000   -0.0000   -5.7244  -19.2756   -4.5667   -0.4333

  Columns 9 through 16

   -1.4669   -3.5331   -7.5000  -17.5000   -0.0000   -5.0000   -2.5000  -22.5000

  Columns 17 through 24

  -17.5000   -7.5000   -1.1935   -3.8065   -8.9669  -16.0331   -0.0000   -5.0000

  Columns 25 through 32

   -3.1454   -1.8546  -19.8083   -5.1917   -0.0000   -5.0000   -8.6935  -16.3065


ceq =

   1.0e-12 *

  Columns 1 through 8

         0         0         0   -0.0349    0.3755    0.1253    0.3136    0.3482

  Columns 9 through 16

   -0.1403    0.0853   -0.0107   -0.1719    0.7283    0.0373   -0.0178   -0.2003

  Columns 17 through 19

    0.8445    0.0649    0.1905

InvestigatePathClothoidPairPolygonRegionFittingOpt

n_cells =

     6


jt_list_clothoid =

     []

