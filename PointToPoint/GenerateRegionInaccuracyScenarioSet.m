% Investigate objective change due to 0->0.5m error from sensing
% base on item fetch env4
load('data/waypoint_data_shifted6.mat', 'waypoint_data', 'region_data', 'H_data', 'offset_values')

obs_height = 7.5;
obs_width = 1;
obs_x = 5;
obs_y = -5;

offset_values = 0.0:0.1:0.5;
JT_list = zeros(length(offset_values));
for i = 1:length(offset_values)
    offset = offset_values(i);
    region_list = [0, obs_x + offset, -5, 5;
                    0, 11, obs_y + offset + obs_height, 5;
                    0, 11, -5, obs_y + offset;
                    obs_x + offset + obs_width, 11, -5, 5];

    grid_compound = CompoundRegion(region_list);
    start = [0.0, 0.0, 0.0];
    goal = [11.0, 0.0, 0.0];
    [waypoint_list, H] = GetWaypointsAndRegionAssignment(grid_compound, start, goal );
    %manually remove an annoying duplicate waypoint and H row
    waypoint_list = [waypoint_list(1:2, 1:3); waypoint_list(4:5, 1:3)]; 
    H = [H(1:2, 1:4);H(4:4, 1:4)]; 

    %waypoint_list = waypoint_data{4} % cheat with hardcoded waypoints
    p_init = Path8P.FromWaypoints(waypoint_list);
    JT_list(i) = Path8P.objective(p_init);
    
    n_cells = length(waypoint_data);
    waypoint_data{n_cells + 1} = waypoint_list;
    region_data{n_cells + 1} = region_list;
    H_data{n_cells+1} = H;


    
%     figure
%     [x_init, y_init, phi_init, k_init, s_init] = Path8P.IntegratePath(p_init);
%     subplot(2,1,1)
%     plot_region_list(region_list);
%     hold on
%     plot(x_init, y_init, '--')
%         plot(waypoint_list(:, 1), waypoint_list(:,2), 'x')
%     Path8P.PlotPose(start)
%     Path8P.PlotPose(goal)
    
    
end

 %save('data/waypoint_data_shifted6.mat', 'waypoint_data', 'region_data', 'H_data', 'offset_values')
