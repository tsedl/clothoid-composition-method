classdef Path8P
    %PATH8P Encapsulates a 2D cubic polynomial path
    % With 8 parameters per segment. x(t) = a + b*t + c*t.^2 + d*t^3
            % y(t) = e + f*t + g*t^2 + h*t^3  
    % init = FromWaypoints(waypoint_list), PlotPath(p), objective(p),
    % constraints(p, regions_list, H)
    methods(Static)
        function test_script
            load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
            n_cells = length(waypoint_data);
            for test_id = 1:n_cells
                waypoint_list = waypoint_data{test_id}; 
                region_list = region_data{test_id}; % all waypoints are based on the same 
                H = H_data{test_id};

                start = waypoint_list(1, (1:3));
                goal = waypoint_list(end, (1:3));

                init = Path8P.FromWaypoints(waypoint_list);
                [x, y, psi, kappa, s] = Path8P.IntegratePath(init);
%                 figure
%                 plot(s, x(1:end-1))
%                 hold on
%                 plot(s, y(1:end-1))
%                 plot(s, psi(1:end-1))
%                 plot(s, kappa)
                 
                
                
%                 figure
%                 plot_region_list(region_list)
%                 hold on
%                 plot(x, y)
%                 plot(waypoint_list(:, 1), waypoint_list(:,2), 'x')
%                 Path8P.PlotPose(start)
%                 Path8P.PlotPose(goal)
                %[opt_output, total_length] = f_PolynomialPath6P(region_list, start , goal, w, plot_all, init, H);
                '========TEST 1======='
                assert(norm([x(1), y(1)]-start(1:2))<5*eps)
                assert(norm([x(end), y(end)]-goal(1:2))<5*eps)
                '=========PASSED====='
                '========TEST 2======='
                assert(Path8P.objective(init)>0)
                '=========PASSED====='
                '========TEST 3======='
                [c, ceq] = Path8P.constraints(init, start, goal, region_list, H)
                assert(all(c<=0, 'all'))
                %accuracy limited by A\b computation, 5*eps from experiment
                assert(all(abs(ceq)<5*eps, 'all'))
                '=========PASSED====='
            end
        end
           
        function init = FromWaypoints(waypoint_list)
            % use matrix algebra to solve for parameters of n segments
            % constrained to meet the heading at start and at the goal
            % each piece meets the next with smooth first derivative and zero 
            % second derivative
            % one piece is defined by x(t) = a + b*t + c*t.^2 + d*t^3
            % y(t) = e + f*t + g*t^2 + h*t^3
            % so eight parameters per piece
            % there is not sufficient freedom to meet the start and end heading
            % with zero acceleration
            % x and y can be solved separately
            n_pieces = size(waypoint_list, 1)-1;
            start = waypoint_list(1,1:3);
            goal = waypoint_list(end, 1:3);
            n_param_x = n_pieces*4;
            Ax = zeros(n_param_x, n_param_x);
            bx = zeros(n_param_x, 1);
            Ax(1:2, 1:4) = [1, 0, 0, 0;
                            0, 1, 0, 0];
            bx(1:2) = [start(1), cos(start(3))];
            for i = 1:n_pieces-1
                A_part = [1, 1, 1, 1, 0, 0, 0, 0; % meet waypoint
                            1, 1, 1, 1, -1, 0, 0, 0; % 0th deriv
                            0, 1, 2, 3, 0, -1, 0, 0; % 1st deriv
                            0, 0, 2, 6, 0, 0, -2, 0]; % 2nd deriv
                b_part = [waypoint_list(i+1,1), 0, 0, 0];
                Ax(2+4*i-3:2+4*i, 4*i-3:4*i+4) = A_part;
                bx(2+4*i-3:2+4*i) = b_part;
            end
            Ax(n_param_x-1:n_param_x, n_param_x-3:n_param_x) = [1, 1, 1, 1;
                                                                0, 1, 2, 3];
            bx(n_param_x-1:n_param_x) = [goal(1), cos(goal(3))];
            x_params = Ax\bx; % equivalent to Inv(Ax)*bx

            n_param_y = n_pieces*4;
            Ay = zeros(n_param_y, n_param_y);
            by = zeros(n_param_y, 1);
            Ay(1:2, 1:4) = [1, 0, 0, 0;
                            0, 1, 0, 0];
            by(1:2) = [start(2), sin(start(3))];
            for i = 1:n_pieces-1
                A_part = [1, 1, 1, 1, 0, 0, 0, 0; % meet waypoint
                            1, 1, 1, 1, -1, 0, 0, 0; % 0th deriv
                            0, 1, 2, 3, 0, -1, 0, 0; % 1st deriv
                            0, 0, 2, 6, 0, 0, -2, 0]; % 2nd deriv
                b_part = [waypoint_list(i+1,2), 0, 0, 0];
                Ay(2+4*i-3:2+4*i, 4*i-3:4*i+4) = A_part;
                by(2+4*i-3:2+4*i) = b_part;
            end
            Ay(n_param_y-1:n_param_y, n_param_y-3:n_param_y) = [1, 1, 1, 1;
                                                                0, 1, 2, 3];
            by(n_param_y-1:n_param_y) = [goal(2), sin(goal(3))];
            y_params = Ay\by; % equivalent to Inv(Ay)*by

            init= [];
            for i = 1:n_pieces
                init = [init; x_params(4*i-3:4*i); y_params(4*i-3:4*i)];
            end
        end
        
        function [xx, yy, psipsi, kappakappa, ss] = IntegratePath(p, step)
            if nargin < 2
                step = 0.02;
            end
            n_pieces = length(p)/8;
            xx=[];
            yy=[];
            psipsi = [];
            for i = 1:n_pieces
                % we choose to start from 0, starting from step is also
                % valid, but then we cant test the first point matches start 
                t_range = 0:step:1;
                a = p(8*i - 7);
                b = p(8*i - 6);
                c = p(8*i - 5);
                d = p(8*i - 4);
                e = p(8*i - 3);
                f = p(8*i - 2);
                g = p(8*i - 1);
                h = p(8*i );
                
                t = t_range;
                x = a + b*t + c*t.^2 + d*t.^3;
                y = e + f*t + g*t.^2 + h*t.^3;
                [u,v] = Path8P.first_derivative(p(8*i-7: 8*i), t);
                psi = atan2(v, u);
                xx = [xx, x];
                yy = [yy, y];
                psipsi= [psipsi, psi];
            end
            ds = sqrt(diff(xx).^2 + diff(yy).^2);
            kappakappa = [0, diff(psipsi)./ ds];
            ss = [0, cumsum(ds)];
        end
        
        function PlotPose(pose)
            plot(pose(1), pose(2), 'o')
            hold on
            plot([pose(1), pose(1)+cos(pose(3))], [pose(2), pose(2)+sin(pose(3))], '-')

        end
        
        function cost = objective(p)
            % objective based on the squared sum of the first
            %derivative, where 
            % dx(t)/dt=b+2*c*t+3*d*t^2, dy(t)/dt=f+2*g*t+3*h*t^2
            % and we just use the value at t=1 rather than integrate
            n_pieces = length(p)/8;
            cost = 0;
            for i = 1:n_pieces
                [u,v] = Path8P.third_derivative(p(8*i-7:8*i), 1);
                sqr_sum = u*u + v*v;
                cost = cost + sqr_sum;
            end
        end
        
        function [c_ineq, ceq] = constraints(p, start, goal, region_list, H)
            % equality constraints each piece is continuous and meets the start and goal
            % inequality constraints ensure each piece remains inside the
            % region assigned in H
            n_pieces = length(p)/8;
%            np = size(H, 1)
            nr = size(H, 2);
            n_samples = 50;
            c_ineq=[];
            ceq=[];
            for i = 1:n_pieces
                % how many points along the path to constraint
                % can I use sum of squares to do this exactly?
                t_range = (1/n_samples):(1/n_samples):1;
                [x,y] = Path8P.point_on_path(p(8*i - 7:8*i), t_range);
                for j = 1:nr
                    if H(i,j)==true
                        [sigma1, sigma2] = Path8P.SumOfSquares(p(8*i-7: 8*i), region_list(j,:), t_range(end));
                        [beta1, beta2, beta3] = Path8P.ConeParameters(p(8*i-7: 8*i), region_list(j,:));
                        sdp_leq = [-beta1', -beta3', (beta2.^2 - 4*beta1.*beta3)']; 
                        
                        xmin = region_list(j,1);
                        xmax = region_list(j,2);
                        ymin = region_list(j,3);
                        ymax = region_list(j,4);
                        c_ineq = [c_ineq,xmin-x, x-xmax, ymin-y, y-ymax];
%                        c_ineq = [c_ineq, sdp_leq];
                    end
                end
               if i < n_pieces
                   % continuity constraints
                   [u,v] = Path8P.first_derivative(p(8*i - 7:8*i), 1);
                   [u2,v2] = Path8P.second_derivative(p(8*i - 7:8*i), 1);
                   [xnext, ynext] = Path8P.point_on_path(p(8*(i+1)-7: 8*(i+1)), 0);   
                   [unext,vnext] = Path8P.first_derivative(p(8*(i+1) - 7:8*(i+1)), 0);
                   [u2next,v2next] = Path8P.second_derivative(p(8*(i+1) - 7:8*(i+1)), 0);
                   ceq = [ceq; xnext - x(end); ynext - y(end); unext - u; vnext - v; u2next-u2; v2next - v2];  
               end
                
                if i==1
                    [x_start,y_start] = Path8P.point_on_path( p(8*i-7: 8*i), 0 );
                    [u_start,v_start] = Path8P.first_derivative( p(8*i-7: 8*i), 0 );
                    ceq = [ceq;
                        x_start - start(1);
                        y_start - start(2);
                        u_start - cos(start(3));
                        v_start - sin(start(3))];
                end
                if i==n_pieces
                    [u2_end,v2_end] = Path8P.first_derivative(p(8*i - 7:8*i), 1);
                    ceq = [ceq; 
                        x(n_samples) - goal(1);
                        y(n_samples) - goal(2);
                        u2_end - cos(goal(3));
                        v2_end - sin(goal(3))];
                end
           end
        end
        
        function [x, y] = point_on_path(segment_params, t)
            a = segment_params(1);
            b = segment_params(2);
            c = segment_params(3);
            d = segment_params(4);
            e = segment_params(5);
            f = segment_params(6);
            g = segment_params(7);
            h = segment_params(8);
            x = a + b*t + c*t.^2 + d*t.^3;
            y = e + f*t + g*t.^2 + h*t.^3;
        end
        function [u,v] = first_derivative(segment_params, t)
%             a = segment_params(1);
            b = segment_params(2);
            c = segment_params(3);
            d = segment_params(4);
%             e = segment_params(5);
            f = segment_params(6);
            g = segment_params(7);
            h = segment_params(8);
            u = b + 2*c*t + 3*d*t.*t;
            v = f + 2*g*t + 3*h*t.*t; 
        end
        function [u,v] = second_derivative(segment_params, t)
%             a = segment_params(1);
%             b = segment_params(2);
            c = segment_params(3);
            d = segment_params(4);
%             e = segment_params(5);
%             f = segment_params(6);
            g = segment_params(7);
            h = segment_params(8);
            u = 2*c + 6*d*t;
            v = 2*g + 6*h*t; 
        end
        function [u,v] = third_derivative(segment_params, t)
%             a = segment_params(1);
%             b = segment_params(2);
%            c = segment_params(3);
            d = segment_params(4);
%             e = segment_params(5);
%             f = segment_params(6);
%            g = segment_params(7);
            h = segment_params(8);
            u = 6*d;
            v = 6*h; 
        end
        
        function [sigma1, sigma2] = SumOfSquares(segment_params, region, t)
            a = segment_params(1);
            b = segment_params(2);
            c = segment_params(3);
            d = segment_params(4);
            e = segment_params(5);
            f = segment_params(6);
            g = segment_params(7);
            h = segment_params(8);
            xmin = region(1);
            xmax = region(2);
            ymin = region(3);
            ymax = region(4);
            sigma1 = [xmax - a - b - c*t - d*t.*t;
                      a + b + c*t + d*t.*t - xmin;
                      ymax - e - f - g*t - h*t.*t;
                      e + f + g*t + h*t.*t - ymin];
            sigma2 =   [xmax - a;
                        a - xmin;
                        ymax - e;
                        e - ymin];
        end
        
        function[beta1, beta2, beta3] = ConeParameters(segment_params, region)
            % SumofSquares sigma_i = beta1 + beta2*t + beta3t.*t
            % so each beta parameter contains one element for each element
            % in sigma1 and one for each element in sigma2
            a = segment_params(1);
            b = segment_params(2);
            c = segment_params(3);
            d = segment_params(4);
            e = segment_params(5);
            f = segment_params(6);
            g = segment_params(7);
            h = segment_params(8);
            xmin = region(1);
            xmax = region(2);
            ymin = region(3);
            ymax = region(4);
            beta1 = [xmax - a - b;
                    a + b - xmin;
                    ymax - e - f;
                    e + f - ymin;
                    xmax - a;
                    a - xmin;
                    ymax - e;
                    e - ymin];
            beta2 = [-c;c;-g;g;0;0;0;0];
            beta3 = [-d;d;-h;h;0;0;0;0];
            
        end
    end
end