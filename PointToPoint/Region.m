classdef Region
    % REGION Encapsulates a convex polygonal region
    %{
    GetOnlyInteriorPoints(obj, point_list) returns only those points which
    are inside the region
    %}
    properties
        vertex_list
        A
        b
    end
    methods(Static)
        function test_script()
            example = Region([0, 0; 1, 0; 1, 1; 0, 1])
            'TEST 1'
            [A, b] = example.GetConstraintInequality()
            x_interior = [0.5, 0.5]';
            A*x_interior <= b
            assert(all(A*x_interior <= b))
            
            x_exterior = [1.5, 0.5]'
            A*x_exterior <= b
            assert(not(all(A*x_exterior <= b)))
            'PASSED'

%         print(numpy.matmul( x_exterior, A))
%         print(numpy.matmul( x_exterior, A)<=b)
%         print('xA',numpy.matmul( x_exterior, A),'b',b)
%         assert(numpy.all(numpy.matmul( x_exterior, A)<=b) == False)
%         x_exterior=[[0.5, 1.5]]
%         print(numpy.matmul( x_exterior, A))
%         print(numpy.matmul( x_exterior, A)<=b)
%         assert(numpy.all(numpy.matmul( x_exterior, A)<=b) == False)
%         x_exterior=[[-0.5, -0.5]]
%         print(numpy.matmul( x_exterior, A))
%         print(numpy.matmul( x_exterior, A)<=b)
%         assert(numpy.all(numpy.matmul( x_exterior, A)<=b) == False)
% 
% 
        '======================='
        'TEST 2'
        example2 = Region([1, 1; 2, 1; 2, 2; 1, 2])
        [A, b] = example2.GetConstraintInequality()
        x_interior = [1.5, 1.5]'
        A*x_interior <= b
        assert(all(A*x_interior <= b))

        x_exterior=[0.5, 0.5]'
        A*x_exterior <= b
        assert(not(all(A*x_exterior <= b)))
        'PASSED'

        '======================='
        'TEST 3'
        x_list = [	1.1,1.1;
                    1.9,1.1;
                    1.9,1.9;
                    1.1,2.1;
                    5 ,5 ]'
        is_interior = [1,1,1,0,0]
        'Matrix containing the result of every point i with every line j'
        A*x_list
        M = A*x_list<=b
%         # all(M) returns True iff every element of M is True, otherwise returns False
        shape = size(M)
        for i= 1:shape(2)
            M(:,i)
            if(is_interior(i))
                assert(all(M(:,i)))
            else
                assert(not(all(M(:,i))))
            end
        end  
        'PASSED'

        region1 = Region([0, 0; 2, 0; 2, 2; 0, 2])
        region2 = Region([1, 1; 3, 1; 3, 3; 1, 3])
        
        region1.GetConstraintInequality()
        
%TODO:        intercept_list_12 = region1.FindEveryIntercept(region2)
        
        interior_list = region1.GetOnlyInteriorPoints([0, 0; 2, 0; 2, 2; 0, 2; 1, 1; 3, 1; 3, 3; 1, 3]')

        interior_list == [0, 0; 2, 0; 2, 2; 0, 2; 1, 1]'
        all(interior_list == [0, 0; 2, 0; 2, 2; 0, 2; 1, 1]')
        % the 'all' argument is needed to test every element, not just the
        % the default axis, or a specific axis
        assert(all(interior_list == [0, 0; 2, 0; 2, 2; 0, 2; 1, 1]', 'all'))
        end
    end
    methods
        function obj = Region(region_vector)
            Xmin = region_vector(1,:);
%            [xmax, ymin] = region_vector(2,:);
            Xmax = region_vector(3,:);
%            [xmin, ymax] = region_vector(4,:);
            xmin = Xmin(1);
            ymin = Xmin(2);
            xmax = Xmax(1);
            ymax = Xmax(2);
            obj.vertex_list = [xmin, ymin; xmax, ymin; xmax, ymax; xmin, ymax]; 
            obj.b = [-xmin, xmax, -ymin, ymax]';
            obj.A = [-1, 0;
                1, 0;
                0, -1;
                0, 1];
        end
        function interior_list = GetOnlyInteriorPoints(obj, point_list)
            A = obj.A;
            b = obj.b;
            shape = size(point_list);
            interior_list = []
            truth_values = A*point_list<=b
            for i = 1:shape(2)
                point = point_list(:,i)
                v1 = A*point <=b
                v2 = truth_values(:,i)
                if(all(v1))
                    interior_list = [interior_list, point]
                end
            end
        end
        function [A, b] = GetConstraintInequality(obj)
            A = obj.A;
            b = obj.b;
        end
        function pos = GetCentroid(obj)
            pos = sum(obj.vertex_list, 1);
        end
    end
end

