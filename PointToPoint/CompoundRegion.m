classdef CompoundRegion
%{
CompoundRegion is a collection of polygonal regions
Construct it from a list of rectangular regions in the form 
regionlist = [xmin1, xmax1, ymin1, ymax1;
               xmin2, xmax2, ymin2, ymax2;
                ...                     ]
GetGraphMatrix returns a graph representation of the corner points in
the union of all the regions, all points in the same region are fully
connected, containment is semi-definite so points on a boundary are members
of both regions

%}
properties
    polygon_region_list
end
methods(Static)
    function test_script
        %regions are axis aligned: [xmin, xmax, ymin, ymax]
        region1 = [0.0, 5.0, -5.0, 5.0];
        region2 = [0.0, 11.0, 2.5, 5.0];
        region3 = [6.0, 11.0, -5.0, 5.0];

        region_list = [region1; region2; region3];

        'TEST 1'
        % These two regions should only overlap at (1,1)
        example1 = [0.0, 1.5, 0.0, 1.5]
        example2 = [1.0, 2.0, 1.0, 2.0]
        start = [0, 0]
        goal = [2, 2]
        compound12 = CompoundRegion([example1; example2])
        intercept_points = compound12.GetInterceptPoints(compound12.polygon_region_list(1),compound12.polygon_region_list(2))
        assert(all(intercept_points == [1.5, 1.0; 1.0, 1.5], 'all'));
        'PASSED ========'
        'TEST 2'
        example3 = [-5.0, 5.0, -1.0, 1.0]
        example4 = [-1.0, 1.0, -5.0, 5.0]
        compound34 = CompoundRegion([example3; example4]);
        intercept_points = compound34.GetInterceptPoints(compound34.polygon_region_list(1),compound34.polygon_region_list(2))
        assert(size(intercept_points, 1)==4)
        'PASSED========='
        'TEST 3'
        %graph_mat = compound12.GetGraphMatrix(start, goal) % deprecated
  
        expected_graph_mat =  [       0    2.1213    1.8028    1.8028    1.4142       Inf
                               2.1213         0    0.5000    0.5000     0.7071    0.7071
                                1.8028    0.5000         0    0.7071    0.5000    1.1180
                                1.8028    0.5000    0.7071         0    0.5000    1.1180
                                1.4142    0.7071    0.5000    0.5000         0    1.4142
                                   Inf    0.7071    1.1180    1.1180    1.4142         0 ];
    
        expected_n_nodes = length(diag(expected_graph_mat));
        
        graph = compound12.GetExtremalPointGraph(start, goal);
        assert(all(graph.node_map(1).pos == start, 'all'))
        
        graph_mat = graph.GetAdjacencyMatrix()
        % check it's a square nxn matrix with the right n 
        assert(all(size(graph_mat)==expected_n_nodes))
        %check the the diagonal
        assert(all(diag(graph_mat)==0, 'all')) % should the diagonal be zero?
        exact_match = graph_mat == expected_graph_mat;
        for i=1:expected_n_nodes
            for j=1:expected_n_nodes
                if exact_match(i, j) == false
                    assert(norm(graph_mat(i,j) - expected_graph_mat(i,j))<1e-4)
                end
            end
        end
        
        'PASSED'
    end
    function intercept_points = GetInterceptPoints(region1, region2)
        intercept_points = [];
        [A1, b1] = region1.GetConstraintInequality();
        [A2, b2] = region2.GetConstraintInequality();
        n_rows1 = length(b1);
        n_rows2 = length(b2);
        for i = 1:n_rows1
            for j =1:n_rows2
                Ai = A1(i, :);
                bi = b1(i);
                Aj = A2(j, :);
                bj = b2(j);
                Ap = [Ai;-Aj];
                bp = [bi;-bj];
                if(rank(Ap) == size(Ap, 2))
                    line_crossing_point = bp'*Ap^-1;
                    interior = ~isempty(region1.GetOnlyInteriorPoints(line_crossing_point)) && ~isempty(region2.GetOnlyInteriorPoints(line_crossing_point));
                    if(interior)
                        intercept_points = [intercept_points; line_crossing_point];
                    end
                end
            end
        end
    end
end
methods
    function obj = CompoundRegion(region_list)
        n_regions = size(region_list, 1);
        polygon_region_list = PolygonRegion.empty(0, n_regions);
        for i=1:n_regions
            region = region_list(i, :);
            p_list = [region(1),region(3);
                region(1),region(4);
                region(2),region(3);
                region(2),region(4)];
            polygon_region_list(i) = PolygonRegion(p_list);
        end
        obj.polygon_region_list = polygon_region_list;

        
    end
    function graph = GetExtremalPointGraph(obj, start, goal)
        assert(length(start) == length(goal))
        graph = DirectedGraph();
        start_groups = [];
        goal_groups = [];
        for k=1:length(obj.polygon_region_list)
            if ~isempty(obj.polygon_region_list(k).GetOnlyInteriorPoints(start))
                start_groups = [start_groups, k];
            end
            if ~isempty(obj.polygon_region_list(k).GetOnlyInteriorPoints(goal))
                goal_groups = [goal_groups, k];
            end
        end
        graph = graph.AddNode(start, start_groups);
        n_regions  = size(obj.polygon_region_list, 2);
        for i = 1:n_regions
            for j = 1:n_regions
                if(i~=j)
                    vertex_list_i = obj.polygon_region_list(i).vertex_list;
                    vertex_list_within_j = obj.polygon_region_list(j).GetOnlyInteriorPoints(vertex_list_i);
                    n_within_j = size(vertex_list_within_j, 1);
                    for m = 1:n_within_j
                        graph = graph.AddNode(vertex_list_within_j( m, :), [i, j]);
                    end
                    if j>i % only need to check for intersection one way round
                        intercept_points = obj.GetInterceptPoints(obj.polygon_region_list(i),obj.polygon_region_list(j));
                        n_intercept_points = size(intercept_points, 1);
                        for p = 1:n_intercept_points
                            graph = graph.AddNode(intercept_points(p,:), [i,j]);
                        end
                    end
                end
            end
        end
        graph = graph.AddNode(goal, goal_groups);
        
        %connect up the groups
        for k = 1:graph.node_map.Count
            from_node = graph.node_map(k);
            groups = from_node.groups;
            group_count = length(groups);
            for q = 1:group_count
                group_id = groups(q);
                for r = 1:graph.node_map.Count
                    if(r~=k && graph.node_map(r).IsMember(group_id) )
                        graph = graph.AddLink(from_node.id, graph.node_map(r).id);
                    end
                end
            end
        end
        
    end
   
end

end





