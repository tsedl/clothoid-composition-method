load('data/waypoint_data_shifted6.mat', 'offset_values', 'jt_list', 'jt_list_clothoid')
figure
hold on
plot(offset_values, jt_list/jt_list(1))
plot(offset_values, jt_list_clothoid/jt_list_clothoid(1))
xlabel('Variation [m]')
ylabel('Objective Value (Normalised)')
title('Impact of Region Boundary Variation')
%axis([0,0.6,0.9,2.7])
legend('Cubic', 'Clothoid w=1')