%Investigate reginConstraints2segments and p2p_mulitple_shooting on
%different regions - measure impact of different init vectors
load('data/waypoint_data_shifted6.mat', 'waypoint_data', 'region_data', 'H_data', 'offset_values')
n_cells = length(waypoint_data);
jt_list = [];

for test_id = 5:5
    waypoint_list = waypoint_data{test_id}; 
    region_list = region_data{test_id}; % all waypoints are based on the same 
    H = H_data{test_id};
    %regions at the moment
    poly_region = PolygonRegion(region_list);
    plot_all = true;

    start = waypoint_list(1, (1:3));
    goal = waypoint_list(end, (1:3));

    p_init = Path8P.FromWaypoints(waypoint_list);

    figure
    [x_init, y_init, phi_init, k_init, s_init] = Path8P.IntegratePath(p_init);
    subplot(2,1,1)
    plot_region_list(region_list);
    hold on
    plot(x_init, y_init, '--')
    [c, ceq] = Path8P.constraints(p_init, start, goal, region_list, H);
    plot(waypoint_list(:, 1), waypoint_list(:,2), 'x')
    Path8P.PlotPose(start)
    Path8P.PlotPose(goal)
    axis equal
    subplot(2,1,2)
    plot(s_init, phi_init)
    k_calc = diff(phi_init)./diff(s_init);
    plot( s_init, k_init, '--')
    hold on
    sharpness_init = diff(k_init)./diff(s_init);
    % doesnt look good on plot - spikes of 1e14
    %plot(s_init(1:length(sharpness_init)), sharpness_init, '--');


    %[opt_output, total_length] = f_PolynomialPath6P(region_list, start , goal, w, plot_all, init, H);
    A = [];
    b = [];
    Aeq = [];
    beq =[];
    ub =[];
    lb =[];
    objective = @(p)Path8P.objective(p);
    constraints = @(p)Path8P.constraints(p, start, goal, region_list, H);
    options = optimoptions('fmincon','Display','iter','Algorithm','interior-point');%, 'MaxIterations', 10000, 'MaxFunctionEvaluations', 200000);
    tic
    [p_opt, fval, exitflag, output] = fmincon(objective, p_init, A, b, Aeq, beq, lb, ub, constraints, options);
    toc
    %%%%

    [x_opt, y_opt, phi_opt, k_opt, s_opt] = Path8P.IntegratePath(p_opt);
    subplot(2,1,1)
    plot(x_opt, y_opt)
    xlabel('x [m]')
    ylabel('y [m]')
    subplot(2,1,2)
    %plot(s_opt, phi_opt(1:end-1))
    plot(s_opt, k_opt)
    sharpness_opt = diff(k_init)./diff(s_init);
    %plot(s_opt(1:length(sharpness_opt)), sharpness_opt);

    [c, ceq] = Path8P.constraints(p_opt, start, goal, region_list, H);
    legend('init ', 'min d2Xdt2 ')
    xlabel('Path Length [m]')
    ylabel('Curvature [m^{-1}]')
    %legend('curvature [m^{-1}]', 'sharpness [m^{-2}]', 'curvature [m^{-1}]', 'sharpness [m^{-2}]')
    
    jt_list = [jt_list, objective(p_opt)];
end

%save('data/waypoint_data_shifted6.mat', 'waypoint_data', 'region_data', 'H_data', 'offset_values', 'jt_list')
% plot(offset_values, jt_list)
% xlabel('Translation [m]')
% ylabel('d2Xdt2 Objective Value [m s^{-2}]')
% title('Impact of Bounding Region Variation')

