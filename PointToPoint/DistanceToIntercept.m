function [a,b] = DistanceToIntercept(first_waypoint, next_waypoint)
    vec_theta_1 = [cos(first_waypoint(3)); sin(first_waypoint(3))];
    n_theta_1 = [-vec_theta_1(2), vec_theta_1(1)];
    vec_theta_2 = [cos(next_waypoint(3)); sin(next_waypoint(3))];
    n_theta_2 = [-vec_theta_2(2), vec_theta_2(1)];
    diff12 = first_waypoint(1:2) - next_waypoint(1:2);
    diff21 = -diff12;
    b = (n_theta_1*diff12')/(n_theta_1*vec_theta_2);
    a = (n_theta_2*diff21')/(n_theta_2*vec_theta_1);
end

