%Investigate reginConstraints2segments and p2p_mulitple_shooting on
%different regions - measure impact of different init vectors
load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
n_cells = length(waypoint_data);
test_id = 1
waypoint_list = waypoint_data{test_id}; 
region_list = region_data{test_id}; % all waypoints are based on the same 
H = H_data{test_id};
%regions at the moment
poly_region = PolygonRegion(region_list);
plot_all = true;
w = 1;
start = waypoint_list(1, (1:3));
goal = waypoint_list(end, (1:3));

init = GenerateSingleShootingInit(waypoint_list);
[opt_output, total_length] = f_regionConstraints2Segments(region_list, start , goal, w, plot_all, init, H);

init = GenerateMultiShootingInit(waypoint_list, region_list);
[opt_output, total_length] = f_ptp_multiple_shooting(region_list, start , goal, H, w, plot_all , init);



% USe waypoint interpolation to satisy continuity constraints
% testing point to point result as initialization
plot_figure = false;
method_name = Gim%'VazquezMendez';%Gim
[init_sharpness, init_chainage, sequence_start_pose] = InterpolateWithPairs(waypoint_list, method_name, plot_figure);


[init_s0, init_sf, init_x0, init_y0, init_psi0] = InitParamsFromInterpolation(init_sharpness, init_chainage, waypoint_list);
% single shooting from point-to-point
% init = [init_alpha, init_chainage, init_s0, init_sf];
% [opt_output, total_length] = f_regionConstraints2Segments(region_list, start , goal, w, plot_all, init);
% multi shooting from point-to-point
init = [init_alpha, init_chainage, init_s0, init_sf, init_x0, init_y0, init_psi0];
[opt_output, total_length] = f_ptp_multiple_shooting(region_list, start , goal, H, w, plot_all , init);


function init = GenerateSingleShootingInit(waypoint_list)
    n_regions = size(waypoint_list,1)-1;
    init = zeros(1, 6*n_regions);
    for i = 1:n_regions
        relative = decompose(waypoint_list(i+1, 1:3), waypoint_list(i, 1:3));
        distance = norm(relative(1:2));    
        L_part = distance/4;

        deflection = relative(3);
        phi = atan2(relative(2), relative(1));
        alpha_hat = deflection/(L_part^2);
        alpha_1 = ( sign(phi) + (phi==0) )*alpha_hat;
        
        nr = n_regions;
        init(2*i-1:2*i) = [alpha_1, -alpha_1];
        init(2*(nr+i)-1:2*(nr+i)) = [L_part, L_part];
        init(4*nr+i) = L_part;
        init(5*nr+i) = L_part;
    end
end 
function init = GenerateMultiShootingInit(waypoint_list, region_list)
    n_pieces = size(waypoint_list,1)-1;
    init = zeros(1, 9*n_pieces);
    for i = 1:n_pieces
        relative = decompose(waypoint_list(i+1, 1:3), waypoint_list(i, 1:3));
        distance = norm(relative(1:2)); 
        L_part = distance/4;
        
        phi = atan2(relative(2), relative(1));
        deflection = phi/2; % this is the rule of thumb
        alpha_hat = deflection/(L_part^2);
        alpha_1 = ( sign(phi) + (phi==0) )*alpha_hat;
        
        nr = n_pieces;
        init(2*i-1:2*i) = [alpha_1, -alpha_1];
        init(2*(nr+i)-1:2*(nr+i)) = [L_part, L_part];
        init(4*nr+i) = L_part;
        init(5*nr+i) = L_part;
        init(6*nr+i) = waypoint_list(i, 1);
        init(7*nr+i) = waypoint_list(i, 2);
        init(8*nr+i) = waypoint_list(i, 3);
    
    end
end

function [init_s0, init_sf, init_x0, init_y0, init_psi0] = InitParamsFromInterpolation(init_sharpness, init_chainage, waypoint_list)
    n_trav = round(length(all_sharpness)/4);
    init_alpha = zeros(1, n_trav*2);
    init_chainage = zeros(1, n_trav*2);
    init_s0 = zeros(1, n_trav);
    init_sf = zeros(1, n_trav);
    init_x0 = zeros(1, n_trav);
    init_y0 = zeros(1, n_trav);
    init_psi0 = zeros(1, n_trav);
    for i = 1:n_trav
        s0 = all_chainage(4*i-3);
        chain_pair = all_chainage(4*i-2:4*i-1);
        sf = all_chainage(4*i);
        alpha_pair = all_sharpness(4*i-2:4*i-1);
        if(abs(all_sharpness(4*i-3))>eps || abs(all_sharpness(4*i))>eps)
            'interpolate failed, given sharpness not interspersed wih lines'
        end
        init_alpha(2*i-1:2*i) = alpha_pair;
        init_chainage(2*i-1:2*i) = chain_pair;
        init_s0(i) = s0;
        init_sf(i) = sf;
        init_x0(i) = waypoint_list(i, 1);
        init_y0(i) = waypoint_list(i, 2);
        init_psi0(i) = waypoint_list(i, 3);
    end
end



