%Investigate reginConstraints2segments and p2p_mulitple_shooting on
%different regions - measure impact of different init vectors
load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
%load('data/waypoint_data_shifted6.mat', 'waypoint_data', 'region_data', 'H_data', 'offset_values')
n_cells = length(waypoint_data)

jt_list_clothoid=[]
for test_id = 5:5
    diary(strjoin(['data/env', string(test_id), '_shift_w1.txt'], '') )
    waypoint_list = waypoint_data{test_id}; 
    region_list = region_data{test_id}; % all waypoints are based on the same 
    H = H_data{test_id};
    %regions at the moment
    poly_region = PolygonRegion(region_list);
    plot_all = true;

    start = waypoint_list(1, (1:3));
    goal = waypoint_list(end, (1:3));
    waypoint_list(2:end-1, 3) = atan2(goal(2)-start(2), goal(1)-start(1)); 
    p_init = PathClothoidPair.FromWaypoints(waypoint_list);

    % good init for test_id = 4
    %p_init = [    0.3121   -0.2688   -0.4424    0.4424    0.2688  -0.3121    1.6039    1.8621    1.9804    1.9804    1.8621    1.6039  0.0049    0.0031    1.4190    1.4190  0.0031    0.0049         0   3.8964    7.1035         0    2.5000    2.5000      0    0.8675   -0.8675 ];
    w = 1.0; % sometimes converges faster if w is very small e.g.0.001

    figure
    [x_init, y_init, phi_init, kappa_init, s_init] = PathClothoidPair.IntegratePath(p_init);
    subplot(2, 1, 1)
    plot_region_list(region_list);
    hold on
    plot(x_init, y_init, '--')
    axis equal

    [c, ceq] = PathClothoidPair.constraints(p_init, start, goal, region_list, H);
    plot(waypoint_list(:, 1), waypoint_list(:,2), 'x')
    Path8P.PlotPose(start)
    Path8P.PlotPose(goal)
    
    subplot(2,1,2)
  %  plot(s_init, phi_init, '--')
    hold on
    plot(s_init, kappa_init, '--')
    sharpness_init = diff(kappa_init)./diff(s_init);
    %plot(s_init(1:length(sharpness_init)), sharpness_init, '--');

    np = round(length(p_init)/9);
    A = [];
    b = [];
    Aeq = [];
    beq =[];
    ub = [inf*ones(1,np), inf*ones(1,np), inf*ones(1,np), inf*ones(1,np), inf*ones(1,np), inf*ones(1,np), inf*ones(1,np), inf*ones(1,np), 2*pi*ones(1,np)];
    lb = [-inf*ones(1,np), -inf*ones(1,np), 0*ones(1,np), 0*ones(1,np), 0*ones(1,np), 0*ones(1,np), -inf*ones(1,np), -inf*ones(1,np), -2*pi*ones(1,np)];
    objective = @(p)PathClothoidPair.objective(p, w);
    %test sharpness only objective - similar to w ->inf
    %objective = @(p)p(1:2*np)*p(1:2*np)';
    constraints = @(p)PathClothoidPair.constraints(p, start, goal, region_list, H);
    options = optimoptions('fmincon','Display','iter','Algorithm','interior-point', 'MaxIterations', 500, 'MaxFunctionEvaluations', 30000);%, 'TolCon', 1e-4);
    tic
    [p_opt, fval, exitflag, output] = fmincon(objective, p_init, A, b, Aeq, beq, lb, ub, constraints, options);
    toc
    %%%%

    [x_opt, y_opt, phi_opt, kappa_opt, s_opt] = PathClothoidPair.IntegratePath(p_opt);
    subplot(2,1,1)
    plot(x_opt, y_opt)
    subplot(2,1,2)
    %plot(s_opt, phi_opt)
    plot(s_opt, kappa_opt)
    sharpness_opt = diff(kappa_opt)./diff(s_opt);
    %plot(s_opt(1:length(sharpness_opt)), sharpness_opt);

    %legend('curvature [m^{-1}]', 'sharpness [m^{-2}]', 'curvature [m^{-1}]', 'sharpness [m^{-2}]')
    legend('init ', 'opt b=1.0 ')
    xlabel('Path Length [m]')
    ylabel('Curvature [m^{-1}]')

    [c, ceq] = PathClothoidPair.constraints(p_opt, start, goal, region_list, H)
    
    jt_list_clothoid = [jt_list_clothoid, objective(p_opt)];
      

end

% plot(offset_values, jt_list_clothoid)
% xlabel('Translation [m]')
% ylabel('w=1 Objective Value [m^2}]')
% title('Impact of Bounding Region Variation')
%save('data/waypoint_data_shifted6.mat', 'waypoint_data', 'region_data', 'H_data', 'offset_values', 'jt_list_clothoid')