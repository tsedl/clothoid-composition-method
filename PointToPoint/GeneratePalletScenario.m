
%%%%
% new test scenario (4) - excusion left
region1 = [0.0, 12.0, 0.0, 1.0];
region2 = [3.7, 5.0, 0.0, 8.0];
region3 = [0.0, 12.0, 7.0, 8.0];
region4 = [11.2, 12.0, 0.0, 8.0];
region_list = [region1; region2; region3; region4];
nr = size(region_list, 1);
start = [0,0,0];
goal = [12,8,0*pi/180];
n_pieces = 3;

% does not produce good waypoints on this environemnt
grid_compound = CompoundRegion(region_list);
[waypoint_list, H] = GetWaypointsAndRegionAssignment(grid_compound, start, goal );
% noticed that elements 3 and 4 form a needless spiral, just hard code

H_no_duplicates = eye(n_pieces, nr); % hard code, errors when computed
% cherry pick the good waypoints from the seven retiurned!!?
waypoint_list_no_duplicates = [waypoint_list(1,1:3); waypoint_list(5:7,1:3)];
% here ive hardcoded the good list in case GetWaypointsAndRegionAssignment
% changes
waypoint_list_no_duplicates =[
         0         0         0
    3.7000    1.0000         0
    5.0000    7.0000         0
   12.0000    8.0000         0];



%%%%
load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
n_cells = length(waypoint_data);
waypoint_data{n_cells} = waypoint_list_no_duplicates
region_data{n_cells} = region_list
H_data{n_cells} = H_no_duplicates
save('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')