function feasible = CheckFeasible(first_waypoint, next_waypoint)
    [a,b] = DistanceToIntercept(first_waypoint, next_waypoint);
    d = norm(first_waypoint(1:2) - next_waypoint(1:2));
    feasible = a>d/5 && a<5*d && b<-d/5 && b>-d*5; 
end
