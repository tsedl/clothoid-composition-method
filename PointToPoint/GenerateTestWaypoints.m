% Generate a set of waypoints to test the interpolation method
% random start, random goal, shortest path between
grid_spacing = 2;%metres
grid_width = 5;%metres
grid_height = 5;%metres
x_coords = 2:grid_spacing:grid_width;
y_coords = 0:grid_spacing:grid_height;

% the format of each region is [xmin, xmax, ymin, ymax]
region_list = zeros(length(x_coords)+length(y_coords), 4);
index = 1;
for x = x_coords
    xmin = x - grid_spacing/4; 
    xmax = x + grid_spacing/4;
    ymin = 0;
    ymax= grid_height;
    region_list(index, 1:4) = [xmin, xmax, ymin, ymax];
    index = index + 1;
end
for y = y_coords
    xmin = 0;
    xmax = grid_width;
    ymin = y - grid_spacing/4;
    ymax = y + grid_spacing/4;
    region_list(index, 1:4) = [xmin, xmax, ymin, ymax];
    index = index + 1;
end

grid_compound = CompoundRegion(region_list);
start = [rand*grid_width, -grid_spacing/4 + rand*grid_spacing/2, -pi + rand*2*pi]
goal = [rand*grid_width, grid_height - grid_spacing/4 - rand*grid_spacing/2, -pi + rand*2*pi]
[waypoints, H] = GetWaypointsAndRegionAssignment(grid_compound, start, goal )

load('waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
n_cells = length(waypoint_data);
waypoint_data{n_cells+1} = waypoint_list
region_data{n_cells+1} = region_list
H_data{n_cells+1} = H
save('waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')


