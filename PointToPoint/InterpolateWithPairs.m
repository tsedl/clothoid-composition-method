function [all_sharpness, all_chainage, sequence_start_pose] = InterpolateWithPairs(waypoint_list, method_name, plot_figure)
    method_1='HenrieWilde'; 
    method_2='VazquezMendez'; 
    method_3='Gim';
    
    if nargin<3
        plot_figure = false
    end
    if nargin<2
        method_name = method_3
    end
    if(nargin==0)
        plot_figure = true
        waypoint_list = [1, 1, 0;
            2, 3, pi/2;
            5, 6, 0];
        waypoint_list = [0, 0, pi/2;
            3.62, 10, pi/2 ;
            0, 20, pi/2]
        method_name = method_2
    end 

    if(plot_figure)
        figure
        title([method_name, ' Method'])
        hold on
        plot(waypoint_list(:, 1), waypoint_list(:,2))
        hold on
        for i = 1:size(waypoint_list, 1)
            plot([waypoint_list(i, 1), waypoint_list(i, 1) + 0.25*cos(waypoint_list(i, 3))], [waypoint_list(i, 2), waypoint_list(i, 2) + 0.25*sin(waypoint_list(i, 3))], '-g')
        end
    end
    n_waypoints = size(waypoint_list, 1)
    sequence_start_pose = waypoint_list(1, 1:3)

    all_sharpness = [];
    all_chainage = [];
    for i = 1:n_waypoints-1
        % each pair begins and ends with zero curvature
        first_waypoint = waypoint_list(i, 1:3);
        next_waypoint = waypoint_list(i+1, 1:3);
        relative = decompose(next_waypoint, first_waypoint)
        exist_mid_waypoint = 0;
    
        if abs(relative(2))<1e-3 %&& abs(relative(3))<1e-3
            %only a straight line will suffice
            L = relative(1);%norm(first_waypoint(1:2) - next_waypoint(1:2));
            pair_start_pose = first_waypoint;
            sharpness = [0, 0];
            chainage = [L/2, L/2];
        elseif ~CheckFeasible(first_waypoint, next_waypoint)
            'Infeasible!!'
            exist_mid_waypoint = 1;
            [sharpness, chainage, mid_waypoint] = QuadMethod(first_waypoint, next_waypoint, method_name);
            pair_start_pose = first_waypoint;
        else
            [sharpness, chainage, pair_start_pose] = PairMethod(first_waypoint, next_waypoint, method_name);
        end
        all_sharpness = [all_sharpness, sharpness];
        all_chainage = [all_chainage, chainage];
            

        
        if(plot_figure)
            plot(first_waypoint(1), first_waypoint(2), 'x')
            plot(next_waypoint(1), next_waypoint(2), 'o')
            if(exist_mid_waypoint)
                plot(mid_waypoint(1), mid_waypoint(2), '^')
                plot([mid_waypoint(1), mid_waypoint(1) + 0.25*cos( mid_waypoint(3))], [ mid_waypoint(2),  mid_waypoint(2) + 0.25*sin( mid_waypoint(3))], '-c')
  
            end
            PlotClothoidSequence(sharpness, chainage, pair_start_pose, true);
        end

    end
    
    %figure(2)
    %[x, y, heading, curvature, s] = PlotClothoidSequence(all_sharpness, all_chainage, sequence_start_pose, true);
end
function [sharpness, chainage, mid_waypoint] = QuadMethod(first_waypoint, next_waypoint, method_name)
    if nargin <3
        method_name = 'generic_turn';
    end
    if strcmp(method_name, 'generic_turn') 
        [sharpness, chainage] = generic_turn_params(first_waypoint, next_waypoint);
        [x, y, heading, curvature, s] = PlotClothoidSequence(sharpness(1:2), chainage(1:2), first_waypoint);
        mid_waypoint = [x(end), y(end), heading(end)];
    elseif strcmp(method_name, 'Gim')
        [sharpness, chainage, mid_waypoint, sol] = f_solve_bisection4LL(first_waypoint, next_waypoint);
    else
        %    elseif strcmp(method_name, 'angle_heuristic')
        mid_waypoint = (first_waypoint + next_waypoint)/2;
        good_angle = PairwiseMidAngleSearch(first_waypoint, mid_waypoint, next_waypoint);
        diff = next_waypoint - first_waypoint;
        tangent_angle = atan2(diff(2), diff(1));
        offset_to_good = wrapTo2Pi(good_angle - tangent_angle);
        mid_waypoint(3) = good_angle;
        [sharpness_first, chainage_first, pair_start_pose] = PairMethod(first_waypoint, mid_waypoint, method_name);
        [next_sharpness, next_chainage] = PairMethod(mid_waypoint, next_waypoint, method_name);
        sharpness = [sharpness_first, next_sharpness];
        chainage = [chainage_first, next_chainage];
        
    end
end
function [sharpness, chainage, pair_start_pose] = PairMethod(first_waypoint, next_waypoint, method_name)
   if strcmp(method_name, 'HenrieWilde')
        % In many ways the HenrieWilde Method is not comparable to the
        % other two - it is never able to meet a fixed position, only
        % the heading with two curves.
        % The generic_turn method is capable of meeting position and
        % heading constraints, but this uses four curves
%            [x, y, heading, curvature, s] = generic_turn(first_waypoint, next_waypoint);
        deflection = (next_waypoint(3) - first_waypoint(3))/2
        scale = norm(first_waypoint(1:2) - next_waypoint(1:2))/2      
        [k, alpha, L] = henriewilde(deflection, scale)
        pair_start_pose = first_waypoint;
        sharpness = [alpha, -alpha]
        chainage = [L, L]
    elseif strcmp(method_name, 'VazquezMendez')
        P_1 = first_waypoint;
        P_2 = next_waypoint;
        R = 0.5
        omega =0
        [sharpness, chainage, pair_start_pose] = TwoClothoidTurn(P_1, P_2, R, omega);
        [x, y, heading, curvature, s] = PlotClothoidSequence(sharpness, chainage, pair_start_pose);
    elseif strcmp(method_name, 'Gim')
        Pi = first_waypoint;
        Pf = next_waypoint;
        [sharpness, chainage, pair_start_pose] = f_solve_bisection(Pi, Pf);
    else
        'method not implemented, try one of these'
        method_1='HenrieWilde'; 
        method_2='VazquezMendez'; 
        method_3='Gim';
    end
end

