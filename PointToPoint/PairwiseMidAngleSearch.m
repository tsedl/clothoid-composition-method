function mid_angle = PairwiseMidAngleSearch(first_waypoint, mid_waypoint, next_waypoint)
    init_mid_angle = mid_waypoint(3);
    min_objective = 999;
    feasible_objective = 999;
    best_delta_degrees = 0;
    for delta_degrees = 1:360
        mid_waypoint(3) = pi*delta_degrees/180 + init_mid_angle;
        [a1, b1] = DistanceToIntercept(first_waypoint, mid_waypoint);
        [a2, b2] = DistanceToIntercept(mid_waypoint, next_waypoint);
        objective = (a1+b1)^2 + (a2+b2)^2;
        if objective<min_objective
            min_objective = objective;
            best_delta_degrees = delta_degrees;
        end
        if CheckFeasible(first_waypoint, mid_waypoint) && CheckFeasible(mid_waypoint, next_waypoint)
            feasible_delta_degrees = delta_degrees;
            feasible_objective = objective;
        end
    end
    mid_waypoint(3) = pi*best_delta_degrees/180 + init_mid_angle;
    [a1, b1] = DistanceToIntercept(first_waypoint, mid_waypoint);
    [a2, b2] = DistanceToIntercept(mid_waypoint, next_waypoint);
    if ~CheckFeasible(first_waypoint, mid_waypoint) || ~CheckFeasible(mid_waypoint, next_waypoint)
        'WARNING best objective is infeasible,' 
        if feasible_objective ~= 999
            'switching to feasible backup'
            mid_waypoint(3) = pi*feasible_delta_degrees/180 + init_mid_angle;
        else
            'no feasible angle found during search, PairMethod may struggle'
        end
    end
    mid_angle = rem(mid_waypoint(3) + pi, 2*pi) - pi;
end

