classdef DirectedGraph
    %DIRECTEDGRAPH Encapsulates a directed graph with non negative edge
    %weights
    %   AddNode() AddLink() can be used to build up the graph
    %   GetAdjacencyMatrix() returns a matrix representation
    
    properties
        node_map
        next_id
    end
    
    methods (Static)
        function test_script
            A_expected=[  0.0    2.5    inf;
                          2.5    0.0    2.5;
                          inf    inf    0.0];
            'TEST 1'
            test_graph = DirectedGraph();
            test_graph = test_graph.AddNode([0, 0]);
            test_graph = test_graph.AddNode([1.5, 2]);
            test_graph = test_graph.AddNode([3, 4]);
            
            test_graph = test_graph.AddLink(1, 2);
            test_graph = test_graph.AddLink(2, 3);
            test_graph = test_graph.AddLink(2, 1);
            
            A = test_graph.GetAdjacencyMatrix()

            assert(all(A==A_expected, 'all'))
            'TEST PASSED'
            '=============='
            

        end
    end
    methods
        function obj = DirectedGraph()
            n_nodes = 0;
            dummy_node = GroupNode(1, [0,0], []);
            % workaround for not allowed to initalize an empty Map with a custim valuetype
            obj.node_map = containers.Map(1, dummy_node); % this will be overwritten by the first real node
            obj.next_id = n_nodes + 1 ;
        end
        
        function obj = AddNode(obj, pos, groups)
            if nargin <3
                groups = [];
            end
            obj.node_map(obj.next_id) = GroupNode(obj.next_id, pos, groups);
            obj.next_id = obj.next_id + 1;
        end
        
        function obj = AddLink(obj, from_id, to_id)
            obj.node_map(from_id) = obj.node_map(from_id).AddLink(to_id);
        end
        
        function A = GetAdjacencyMatrix(obj)
            n_nodes = obj.node_map.Count;
            % initial value of matrix before checking links has zeros on
            % diagonal, inf everywhere else
            A = inf*(ones(n_nodes, n_nodes) - eye(n_nodes, n_nodes));
            for i = 1:n_nodes
                A(i,i) = 0;
            end
            
            for i = 1:n_nodes
%                keys = obj.node_map.keys;
%                id = keys(i); % why is this a cell array so it can t be
%                used as a key?
                node_i = obj.node_map(i);
                links = node_i.links;
                for j = 1:length(links)
                    node_j = obj.node_map(links(j)); 
                    A(i, links(j)) = norm(node_i.pos(1:2) - node_j.pos(1:2));
                end
                    
            end
        end
       
    end
end

