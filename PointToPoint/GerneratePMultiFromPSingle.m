% generate multi shooting init parameters from single shooting (reusing
% p_opt variable)
if ~exist('p_opt','var')
    start = [0,0,0];
    p_opt =    [    0.3121   -0.2688   -0.4424    0.4424    0.2688   -0.3121    1.6039    1.8621    1.9804    1.9804    1.8621    1.6039    0.0049    0.0031    1.4190    1.4190    0.0031    0.0049];
end

np = length(p_opt)/6;
alpha = p_opt(1:2*np);
L = p_opt(2*np+1:4*np);
s0 = p_opt(4*np+1:5*np);
sF = p_opt(5*np+1:6*np);
start_pose = start;
k = 0;
x_arr = zeros(1,np);
y_arr = zeros(1,np);
psi_arr = zeros(1,np);
curvaturem_arr = zeros(1,np);
for i = 1:np
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose(start_pose,[s0(i),0,0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF(i),0,0]);
    if i==1
        x_arr(i) = start_pose(1);
        y_arr(i) = start_pose(2);
        psi_arr(i) = start_pose(3);
        x_arr(i+1) = end_pose(1);
        y_arr(i+1) = end_pose(2);
        psi_arr(i+1) = end_pose(3);
    elseif i<np
        x_arr(i+1) = end_pose(1);
        y_arr(i+1) = end_pose(2);
        psi_arr(i+1) = end_pose(3);
    end
    start_pose = end_pose;
    curvaturem_arr(i) = curvaturem;
end
p_multishoot = [alpha, L, s0, sF, x_arr, y_arr, psi_arr];
%  p_multishoot = [    0.3121   -0.2688   -0.4424    0.4424    0.2688
%  -0.3121    1.6039    1.8621    1.9804    1.9804    1.8621    1.6039
%  0.0049    0.0031    1.4190    1.4190  0.0031    0.0049         0
%  3.8964    7.1035         0    2.5000    2.5000         0    0.8675   -0.8675 ];
[x_init, y_init] = PathClothoidPair.IntegratePath(p_multishoot);