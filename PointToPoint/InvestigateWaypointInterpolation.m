% script to generate interesting plots using InterpolateWithPairs
% load the waypoints generated randomly from a set of regions, then add
% headins
load('waypoint_data.mat', 'waypoint_data')
for i = 1: length(waypoint_data)
    waypoint_list = waypoint_data{i};
    p_total = size(waypoint_list, 1);
    n_angles = p_total-2;
    init = zeros(1, n_angles);
    for p_count = 2:p_total-1
        diff = waypoint_list(p_count+1,1:2) - waypoint_list(p_count-1,1:2);
        tangent_angle = atan2(diff(2), diff(1));
        waypoint_list(p_count, 3) = tangent_angle;
    end
%     for p_count = 2:p_total-1
%         diff = waypoint_list(p_count+1,1:2) - waypoint_list(p_count-1,1:2);
%         tangent_angle = atan2(diff(2), diff(1));
%         first_waypoint = waypoint_list(p_count-1, 1:3);
%         mid_waypoint = waypoint_list(p_count, 1:3);
%         next_waypoint = waypoint_list(p_count+1, 1:3);
%         good_angle = PairwiseMidAngleSearch(first_waypoint, mid_waypoint, next_waypoint);
%         offset_to_good = wrapTo2Pi(good_angle - tangent_angle)
%         mid_waypoint(3) = good_angle;
%         init(p_count-1)= mid_waypoint(3); 
%     end
%    [x_opt,FVAL,EXITFLAG,OUTPUT] = fminsearch(@(x)objective(x, waypoint_list), init)
%       x_lb = -pi*ones(size(init));
%       x_ub = pi*ones(size(init));
%       [x_opt,FVAL,EXITFLAG,OUTPUT] = fminbnd( @(x)objective(x, waypoint_list), x_lb, x_ub, optimset('TolX',1e-12,'Display','off') );
%      for p_count = 2:p_total-1
%          waypoint_list(p_count, 3) = x_opt(p_count-1);
%      end
    waypoint_list
    backup = 'Gim';'VazquezMendez';'HenrieWilde';
    [all_sharpness, all_chainage] = InterpolateWithPairs(waypoint_list, 'VazquezMendez' , true);
end
function f = objective(x, waypoint_list)
    p_total = size(waypoint_list, 1);
    a_all = zeros(1, p_total);
    b_all = zeros(1, p_total);
    for p_count = 1:p_total-1
        if(p_count>1)
            waypoint_list(p_count, 3) = x(p_count-1);
        end
        [a_all(p_count),b_all(p_count)] = DistanceToIntercept(waypoint_list(p_count, 1:3), waypoint_list(p_count+1, 1:3));
    end
    diff = (a_all + b_all);
    f = diff*diff';
end
