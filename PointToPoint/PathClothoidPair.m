classdef PathClothoidPair
    %PATHCLOTHOIDPAIR Encapsulates a path consisting of clothoid segments 
    % arranged in pairs with zero curvature at each end and straight lines
    % init = FromWaypoints(waypoint_list), [xx, yy] = IntegratePath(p), 
    % cost = objective(p), [c, ceq] = constraints(p, regions_list, H)
    methods(Static)
        function test_script
            load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
            n_cells = length(waypoint_data);
            for test_id = 1:n_cells
                waypoint_list = waypoint_data{test_id}; 
                region_list = region_data{test_id}; % all waypoints are based on the same 
                H = H_data{test_id};

                start = waypoint_list(1, (1:3));
                goal = waypoint_list(end, (1:3));

                init = PathClothoidPair.FromWaypoints(waypoint_list);
                [x, y, ~, ~, ~] = PathClothoidPair.IntegratePath(init);
                figure
                plot_region_list(region_list)
                hold on
                plot(x, y)
                plot(waypoint_list(:, 1), waypoint_list(:,2), 'x')
                Path8P.PlotPose(start)
                Path8P.PlotPose(goal)
                %[opt_output, total_length] = f_PolynomialPath6P(region_list, start , goal, w, plot_all, init, H);
                '========TEST 1======='
                assert(norm([x(1), y(1)]-start(1:2))<5*eps)
%                assert(norm([x(end), y(end)]-goal(1:2))<5*eps) % init
%                doesnt interpolate to reach all the waypoints yet
                '=========PASSED====='
                '========TEST 2======='
                w = 1;
                assert(PathClothoidPair.objective(init, w)>0)
                '=========PASSED====='
                '========TEST 3======='
                [c, ceq] = PathClothoidPair.constraints(init, start, goal, region_list, H)
                assert(all(c<=0, 'all'))
                %accuracy limited by A\b computation, 5*eps from experiment
                %                doesnt interpolate to reach all the waypoints yet
                %assert(all(abs(ceq)<5*eps, 'all'))
                '=========PASSED====='
            end
        end
           
        function init = FromWaypoints(waypoint_list)
            n_pieces = size(waypoint_list,1)-1;
            init = zeros(1, 9*n_pieces);
            for i = 1:n_pieces
                relative = decompose(waypoint_list(i+1, 1:3), waypoint_list(i, 1:3));
                distance = norm(relative(1:2)); 
                if distance==0
                    distance = 0.2
                end
                L_part = distance/4;

                phi = atan2(relative(2), relative(1));
                deflection = phi/2; % this is the rule of thumb
                alpha_hat = deflection/(L_part^2);
                alpha_1 = ( sign(phi) + (phi==0) )*alpha_hat;

                nr = n_pieces;
                init(2*i-1:2*i) = [alpha_1, -alpha_1];
                init(2*(nr+i)-1:2*(nr+i)) = [L_part, L_part];
                init(4*nr+i) = L_part;
                init(5*nr+i) = L_part;
                init(6*nr+i) = waypoint_list(i, 1);
                init(7*nr+i) = waypoint_list(i, 2);
                init(8*nr+i) = waypoint_list(i, 3);
            end
        end
        
        function [xx,yy,psipsi,kappakappa,tt] = IntegratePath(p, step)
            if nargin < 2
                step = 0.002;
            end
            np = length(p)/9;
            alpha = p(1:2*np);
            L = p(2*np+1:4*np);
            s0 = p(4*np+1:5*np);
            sF = p(5*np+1:6*np);
            x0 = p(6*np+1:7*np);
            y0 = p(7*np+1:8*np);
            psi0 = p(8*np+1:9*np);
            xx = [];
            yy = [];
            psipsi = [];
            kappakappa = [];
            tt = [];
            t=0;
            for i = 1: np
                [x,y,psi,kappa,t] = join([x0(i), y0(i), psi0(i), 0, t(end)], [s0(i), 0, 0, 0, s0(i)]);
                [x1,y1,psi1,kappa1,t1] = clothoid(alpha(2*i-1), L(2*i-1), 0, step);
                [x,y,psi,kappa,t] = join([x,y,psi,kappa,t], [x1,y1,psi1,kappa1,t1]);
                [x2,y2,psi2,kappa2,t2] = clothoid(alpha(2*i), L(2*i), kappa(end), step);
                [x,y,psi,kappa,t] = join([x,y,psi,kappa,t], [x2,y2,psi2,kappa2,t2]);
                [x,y,psi,kappa,t] = join([x, y, psi, kappa, t], [sF(i), 0, 0, 0, sF(i)]);
                xx = [xx,x];
                yy = [yy, y];
                psipsi = [psipsi, psi];
                kappakappa = [kappakappa, kappa];
                tt = [tt, t];
            end
        end
        
        function [f,g] = objective(p, w)
            weight_vector = ones(size(p));
            np = round(length(p)/9);
            weight_vector(1:2*np) = w;
            W = diag(weight_vector);
                f = p*W*p';
                if nargout > 1
                    g = 2*W*p';
                end
        end

        function [c, ceq] = constraints(p, start, goal, region_list, H)
            np = round(length(p)/9);
            nr = size(region_list, 1);

            alpha = p(1:2*np);
            L = p(2*np+1:4*np);
            s0 = p(4*np+1:5*np);
            sF = p(5*np+1:6*np);
            x0 = p(6*np+1:7*np);
            y0 = p(7*np+1:8*np);
            psi0 = p(8*np+1:9*np);

            xf_arr = zeros(1,np);
            x0_arr = zeros(1,np);
            yf_arr = zeros(1,np);
            y0_arr = zeros(1,np);
            psif_arr = zeros(1,np);
            psi0_arr = zeros(1,np);
            curvaturem_arr = zeros(1,np);
            curvaturem_ref = zeros(1,np); 
            c_ineq_stack = zeros(1,8*np);

            for i = 1:np
                segment_pose = [x0(i),y0(i),psi0(i)]';
                [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
                first_pose = compose(segment_pose,[s0(i),0,0]);
                curve_pair_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
                end_pose = compose( curve_pair_pose, [sF(i),0,0]);
                curvaturem_arr(i) = curvaturem;
                xf_arr(i) = end_pose(1);
                yf_arr(i) = end_pose(2);
                psif_arr(i) = end_pose(3);
                x0_arr(i) = segment_pose(1);
                y0_arr(i) = segment_pose(2);
                psi0_arr(i) = segment_pose(3);

                for j = 1:nr
                    if H(i, j)==1
                        region = region_list(j,:);
                        xmin = region(1);
                        xmax = region(2);
                        ymin = region(3);
                        ymax = region(4);
                        c_ineq_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-segment_pose(1), segment_pose(1)-xmax, ymin-segment_pose(2), segment_pose(2)-ymax];
                    end
                end

            end
            c = c_ineq_stack;%[c_ineq_stack, -L, -s0, -sF]; % all lengths must be positive - this may be more efficiently applied as a bound
            ceq = [[x0(1), y0(1), psi0(1)]-start(1:3), curvaturem_arr - curvaturem_ref, xf_arr - [x0_arr(2:length(x0_arr)), goal(1)], yf_arr - [ y0_arr(2:length(y0_arr)),goal(2)], psif_arr - [ psi0_arr(2:length(psi0_arr)),goal(3)] ];
        end
        

    end
end