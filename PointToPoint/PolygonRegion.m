classdef PolygonRegion
    % POLYGONREGION Encapsulates a convex polygonal region
    %{
    GetOnlyInteriorPoints(obj, point_list) returns only those points which
    are inside the region
    %}
    properties
        vertex_list
        A
        b
    end
    methods(Static)
        function test_script()
            example = Region([0, 0; 1, 0; 1, 1; 0, 1])
            'TEST 1'
            [A, b] = example.GetConstraintInequality()
            x_interior = [0.5, 0.5]';
            A*x_interior <= b
            assert(all(A*x_interior <= b))
            
            x_exterior = [1.5, 0.5]'
            A*x_exterior <= b
            assert(not(all(A*x_exterior <= b)))
            'PASSED'

%         print(numpy.matmul( x_exterior, A))
%         print(numpy.matmul( x_exterior, A)<=b)
%         print('xA',numpy.matmul( x_exterior, A),'b',b)
%         assert(numpy.all(numpy.matmul( x_exterior, A)<=b) == False)
%         x_exterior=[[0.5, 1.5]]
%         print(numpy.matmul( x_exterior, A))
%         print(numpy.matmul( x_exterior, A)<=b)
%         assert(numpy.all(numpy.matmul( x_exterior, A)<=b) == False)
%         x_exterior=[[-0.5, -0.5]]
%         print(numpy.matmul( x_exterior, A))
%         print(numpy.matmul( x_exterior, A)<=b)
%         assert(numpy.all(numpy.matmul( x_exterior, A)<=b) == False)
% 
% 
        '======================='
        'TEST 2'
        example2 = PolygonRegion([1, 1; 2, 1; 2, 2; 1, 2])
        [A, b] = example2.GetConstraintInequality()
        x_interior = [1.5, 1.5]'
        A*x_interior <= b
        assert(all(A*x_interior <= b))

        x_exterior=[0.5, 0.5]'
        A*x_exterior <= b
        assert(not(all(A*x_exterior <= b)))
        'PASSED'

        '======================='
        'TEST 3'
        x_list = [	1.1,1.1;
                    1.9,1.1;
                    1.9,1.9;
                    1.1,2.1;
                    5 ,5 ]'
        is_interior = [1,1,1,0,0]
        'Matrix containing the result of every point i with every line j'
        A*x_list
        M = A*x_list<=b;
%         # all(M) returns True iff every element of M is True, otherwise returns False
        shape = size(M);
        for i= 1:shape(2)
            if(is_interior(i))
                assert(all(M(:,i)))
            else
                assert(not(all(M(:,i))))
            end
        end  
        'PASSED'
        
        '======================='
        'TEST 4'

        region1 = PolygonRegion([0, 0; 2, 0; 2, 2; 0, 2]);
        region2 = PolygonRegion([1, 1; 3, 1; 3, 3; 1, 3]);
        
        [A, b] = region1.GetConstraintInequality()
              
        interior_list_hstack = region1.GetOnlyInteriorPoints([0, 0; 2, 0; 2, 2; 0, 2; 1, 1; 3, 1; 3, 3; 1, 3]')

        % the 'all' argument is needed to test every element, not just the
        % the default axis, or a specific axis
        assert(all(interior_list_hstack == [0, 0; 2, 0; 2, 2; 0, 2; 1, 1]', 'all'))
        'PASSED'
        '======================='
        
        'TEST 5'
      
        interior_list_vstack = region1.GetOnlyInteriorPoints([0, 0; 2, 0; 2, 2; 0, 2; 1, 1; 3, 1; 3, 3; 1, 3])

        % the 'all' argument is needed to test every element, not just the
        % the default axis, or a specific axis
        assert(all(interior_list_vstack == [0, 0; 2, 0; 2, 2; 0, 2; 1, 1], 'all'))
        'PASSED'
        end
    end
    methods
        function obj = PolygonRegion(region_vector)
            n_points = size(region_vector, 1);
            pos = region_vector(1, 1:2);
            xmin = pos(1);
            ymin = pos(2);
            xmax = pos(1);
            ymax = pos(2);
            for i = 2:n_points
                pos_i = region_vector(i, 1:2);
                if(pos_i(1) > xmax)
                    xmax = pos_i(1); 
                end
                if(pos_i(1) < xmin)
                    xmin = pos_i(1); 
                end
                if(pos_i(2) > ymax)
                    ymax = pos_i(2); 
                end
                if(pos_i(2) < ymin)
                    ymin = pos_i(2); 
                end
            end
                
            obj.vertex_list = [xmin, ymin; xmax, ymin; xmax, ymax; xmin, ymax]; 
            obj.b = [-xmin, xmax, -ymin, ymax]';
            obj.A = [-1, 0;
                1, 0;
                0, -1;
                0, 1];
        end
        function interior_list = GetOnlyInteriorPoints(obj, point_list)
            if(size(point_list, 1)==2)
                point_list_hstack = point_list;
                transpose_input = 0;
            else
                point_list_hstack = point_list';
                transpose_input = 1;
            end
            n_vertices = size(point_list_hstack, 2);
            interior_list = [];
            truth_values = obj.A*point_list_hstack<=obj.b;
            for i = 1:n_vertices
                point = point_list_hstack(1:2,i);
                v1 = obj.A*point <=obj.b;
                v2 = truth_values(:,i);
                if(all(v1))
                    interior_list = [interior_list, point];
                end
            end
            if transpose_input
                interior_list = interior_list';
            end
        end
        function [A, b] = GetConstraintInequality(obj)
            A = obj.A;
            b = obj.b;
        end
    end
end

