% GenerateScaleUpEnv loads waypoints and regions and scales them by the 
% vector [xscale, yscale]. It will not change the headings.
load('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
n_cells = length(waypoint_data);
xscale = 5;
yscale = 5;
for test_id = 1:1
    waypoint_list = waypoint_data{test_id}; 
    region_list = region_data{test_id}; % all waypoints are based on the same 
    H = H_data{test_id};
    nw = size(waypoint_list, 1);
    waypoint_list = [xscale, yscale, 1].*waypoint_list;
    region_list = [xscale, xscale, yscale, yscale].*region_list;
    
    start = waypoint_list(1, (1:3));
    goal = waypoint_list(end, (1:3));
    waypoint_list(2:end-1, 3) = atan2(goal(2)-start(2), goal(1)-start(1)); 
    p_init = PathClothoidPair.FromWaypoints(waypoint_list);

    figure
    [x_init, y_init, phi_init, kappa_init, s_init] = PathClothoidPair.IntegratePath(p_init);
    subplot(2, 1, 1)
    plot_region_list(region_list);
    hold on
    plot(x_init, y_init, '--')
    axis equal

    [c, ceq] = PathClothoidPair.constraints(p_init, start, goal, region_list, H);
    plot(waypoint_list(:, 1), waypoint_list(:,2), 'x')
    Path8P.PlotPose(start)
    Path8P.PlotPose(goal)
    
    n_cells = length(waypoint_data);
    waypoint_data{n_cells+1} = waypoint_list
    region_data{n_cells+1} = region_list
    H_data{n_cells+1} = H
    save('data/waypoint_data.mat', 'waypoint_data', 'region_data', 'H_data')
end