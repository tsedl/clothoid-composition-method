classdef ExampleClass 
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Property1
    end

    methods(Static)
        function script_code
            '========TEST 1======='
            u_inst = ExampleClass(1,2)
            assert(u_inst.Property1==3)
            '=========PASSED====='
        end
         
    end  
    methods
        function obj = ExampleClass(inputArg1,inputArg2)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            obj.Property1 = inputArg1 + inputArg2;
        end

        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
        
    end
end



