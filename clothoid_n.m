%% clothoid investigation with plotting

% aim to write a function which joins two curves to achieve a certain x, y, psi, kappa at the end
% free variables are k1, s1, k2, s2

function [x,y,psi,kappa,t] = clothoid_n(Kdot, S, kappa_0, n_samples)

if nargin<4
    dt = 0.01;
    n_samples = ceil(S/dt);
end


if(S<eps || n_samples<1)
    % fprintf('travel distance S must be greater than the step size %d m \n',dt);
    fprintf('%d samples over %d metres requested. Returning the minimum of 1 sample over zero distance \n',n_samples, S);
    t=0;
    flex_dt=0;
else
    flex_dt = S/n_samples;
    t = flex_dt*(1:round(n_samples));
    if length(t)~=round(n_samples)
        n_samples
        round(n_samples)
        length(t)
        assert(length(t)==n_samples)
    end
end

kappa = Kdot*t + kappa_0;


psi = Kdot*t.*t*0.5 + kappa_0*t;%cumsum(kappa*dt);
psi = wrapToPi(psi); % wrap angle to range (-pi, +pi) to prevent weirdness

   
x = cumsum(flex_dt*cos(psi)) ;
y = cumsum(flex_dt*sin(psi));
if(any(isnan(x)))
    assert(false)
end


end



