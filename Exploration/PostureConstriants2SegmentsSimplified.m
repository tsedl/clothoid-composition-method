xhat = 8;%10;%8;
yhat = 6;%3.62;%6;
psihat = 60*pi/180;%60*pi/180;%pi/4;
khat = 0;
goal = [xhat, yhat, psihat, khat]

%% Solve for the parameters to reach a point with any heading
% This script is very reliable at finding a minimum sharpness solution - crucially it is possible for lines to be included in the solution 
init = [1, -1, 1, 1, 0, 0];
A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf, -inf, 0, 0, 0, 0];

%weighting- radians per metre squared per metre
w = 1;
objective = @(p)p(1:2)*p(1:2)' + w*p(3:6)*p(3:6)';
constraints_np = @(p)constraints(p, goal);
options = optimoptions('fmincon','Display','iter','Algorithm','sqp','PlotFcns',@optimplotfval);%,'MaxIterations', 1000, 'MaxFunctionEvaluations', 3000);
[p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);
[c_opt, ceq_opt] = constraints(p_opt, goal)

alpha = p_opt(1:2);
L = p_opt(3:4);
s0 = p_opt(5);
sF = p_opt(6);
k=0;
[x, y, heading, curvature] = sample_n(alpha, L)
opt_output = output
figure
plot_sequence(alpha, L, s0, sF, goal, true)

function [c, ceq] = constraints(p, goal)
c = [];
alpha = p(1:2);
L = p(3:4) ;
s0 = p(5);
sF = p(6);
[xf, yf, headingf, curvaturef] = sample_n(alpha, L);
pre_end_pose = compose([s0,0,0],[xf, yf, headingf, curvaturef]);
end_pose = compose(pre_end_pose, [sF,0,0]);
ceq = [end_pose', curvaturef] - goal;
end
