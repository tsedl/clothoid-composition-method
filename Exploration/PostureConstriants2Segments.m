xhat = 10;%8;
yhat = 10;%6;
psihat = 100*pi/180;%60*pi/180;%pi/4;
khat = 0;
goal = [xhat, yhat, psihat, khat]

%% Solve for the parameters to reach a point with any heading
% I wrote a paper about how I could do this with a straight line at either
% end. I need to handle theta < phi with a four clothoid special case 
init = [1, -1, 1, 1, 1, 1];
A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf, -inf, 0, 0, 0, 0];
objective = @(p)p*p';
constraints_np = @(p)constraints(p, goal);
options = optimoptions('fmincon','Display','iter','Algorithm','interior-point');
[p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);
[c_opt, ceq_opt] = constraints(p_opt, goal)
alpha = [p_opt(1:2),-p_opt(1:2)];
L = [p_opt(3:4), p_opt(3:4)] ;
s0 = p_opt(5);
sF = p_opt(6);
k=0;
[x, y, heading, curvature] = sample_n(alpha, L)
opt_output = output

[X, Y, Heading, Curvature, S] = PlotClothoidSequence(alpha, L);
len = length(X);
%final = [X(len), Y(len), Heading(len), Curvature(len)];
[xf, yf, headingf, curvaturef] = sample_n(alpha, L)
clf
plot(X, Y)
hold on
plot(linspace(0, sum(L),length(Heading)), Heading)
plot(linspace(0, sum(L),length(Curvature)), Curvature)

xlabel('x[m] (s[m])')
ylabel('y[m]')
axis equal
title(['X_G =[' num2str(goal) ']']);

[xm, ym, headingm, curvaturem] = sample(alpha(1), L(1),0);
[xmm, ymm, headingmm, curvaturemm] = sample(alpha(2), L(2),curvaturem);
mid_pose = compose([xm, ym, headingm], [xmm, ymm, headingmm]);
plot([xm, mid_pose(1),xf], [ym,mid_pose(2),yf], 'ob');
%
 plot([L(1), sum(L(1:2)), sum(L)], [headingm, mid_pose(3), headingf], 'or');
 plot([L(1), sum(L(1:2)), sum(L)], [curvaturem, curvaturemm, curvaturef], 'og');

legend('x-y [m]', 's-\psi [rad]', 's-\kappa [rad.m^{-1}]','x-y [m]', 's-\psi [rad]', 's-\kappa [rad.m^{-1}]')

function [c, ceq] = constraints(p, goal)
c = [];
alpha = [p(1:2),-p(1:2)];
L = [p(3:4), p(3:4)] ;
s0 = p(5);
sF = p(6);
[xf, yf, headingf, curvaturef] = sample_n(alpha, L);
pre_end_pose = compose([s0,0,0],[xf, yf, headingf, curvaturef]);
end_pose = compose(pre_end_pose, [sF,0,0]);
ceq = [end_pose', curvaturef] - goal;
end

function [xf, yf, headingf, curvaturef] = sample(alpha, L, k_m)
funx = @(u)cos(k_m.*u + alpha*u.*u/2);
xf = integral(funx, 0, L); 
funy = @(u)sin(k_m.*u + alpha*u.*u/2);
yf = integral(funy, 0, L);
deflectionf = k_m*L + alpha*L*L/2;
headingf = wrapToPi(deflectionf); % heading between +pi and -pi - should make no difference
curvaturef = k_m + alpha*L;
end

function [xf, yf, headingf, curvaturef, sf] = sample_n(alpha, L)
n = length(alpha);
assert(n==length(L));
k = 0;
mid_pose = [0,0,0];
for i = 1:n
    [xf, yf, headingf, curvaturef] = sample(alpha(i), L(i), k);
    mid_pose = compose(mid_pose, [xf(length(xf)), yf(length(yf)), headingf(length(headingf))]);
    k = curvaturef(length(curvaturef));
end

xf = mid_pose(1);
yf = mid_pose(2);
headingf = mid_pose(3);
curvaturef = k;
end