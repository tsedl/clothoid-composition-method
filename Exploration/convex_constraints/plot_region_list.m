function corners = plot_region_list(region_list)
if nargin <1
     %regions are axis aligned: [xmin, xmax, ymin, ymax]
    region1 = [0.0, 10.0, -5.0, 5.0];
    region2 = [0.0, 25.0, 2.5, 5.0];
    region3 = [15.0, 25.0, -5.0, 5.0];
    region_list = [region1; region2; region3];
end
    corners = [];
    for i = 1:length(region_list(:,1))
        region = region_list(i,:);
        cornersx = [region(1), region(2), region(2), region(1), region(1)]; 
        cornersy = [region(3), region(3), region(4), region(4), region(3)];
        plot(cornersx, cornersy);
        hold on
        corners = [corners, [cornersx; cornersy]];
    end
end
