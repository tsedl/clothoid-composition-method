function [opt_output, total_length] = f_ptp_multiple_shooting(region_list, start , goal, H, w ,plot_all, init)
    if nargin <1
        %regions are axis aligned: [xmin, xmax, ymin, ymax]
        region1 = [0.0, 5.0, -5.0, 5.0];
        region2 = [0.0, 11.0, 2.5, 5.0];
        region3 = [6.0, 11.0, -5.0, 5.0];
        region_list = [region1; region2; region3];
    end
    nr = size(region_list, 1);
    if nargin <2
        start = [0,0,0];
    end
    if nargin <3
        goal = [11,0,0*pi/180];
    end
    if nargin < 4
        n_pieces = nr;
        H = eye(n_pieces, nr);
    end
    if nargin < 5
        w = 1;
    end
    if nargin < 6
        plot_all = false;
    end
    if nargin < 7
        % changed it so there are 9 parameters per region - the last three are x0, y0, psi0 
        %init = [1, -1,1, -1, 1, 1,1, 1, 0, 0, 0, 0, region1(1), region2(1),region1(3),region2(3), 0, 0];
        np = n_pieces;
        init_x0 = zeros(1,np);
        init_y0 = zeros(1,np);
        init_psi0 = zeros(1,np);
        alpha_plus = ones(1,np);
        alpha_minus = -ones(1, np);
        init_alpha = ones(1,2*np);
        init_alpha(1:2:end-1) = alpha_plus;
        init_alpha(2:2:end) = alpha_minus;
        init_L = ones(1,2*np);
        for i =1:np
            for j =1:nr
                if H(i, j)==1
                    %init_x0(i) = 0.5*(region_list(i, 1) + region_list(i, 2));
                    %init_y0(i) = 0.5*(region_list(i, 3) + region_list(i, 4));
                     init_x0(i) = region_list(j, 1);
                     init_y0(i) = region_list(j, 3);
                    if(i>1)
                        % if the psi values don't match the sharpness they throw off
                        % convergence! Better to leave them at zero in that case
                        %init_psi0(i) = init_psi0(i-1) + 0.5*init_alpha(2*i-3)*init_L(2*i-3)^2 + init_alpha(2*i-3)*init_L(2*i-3)*init_L(2*i-2) + 0.5*init_alpha(2*i-2)*init_L(2*i-2)^2;
                    end
                end
            end
        end
        init = [init_alpha, init_L, ones(1,np),ones(1,np), init_x0, init_y0, init_psi0];
        % try the same init vector as used for single shooting
 %       init = [1, 1, -1, -1, 1, 1, ones(1,nr), ones(1,nr), ones(1,nr), ones(1,nr), init_x0, init_y0, init_psi0];
    end
    np = round(length(init)/9);


    options = optimoptions(@fmincon,'Algorithm','interior-point', 'FiniteDifferenceType','forward', 'CheckGradients', true, 'SpecifyConstraintGradient', false, 'SpecifyObjectiveGradient', true, 'Display','iter','PlotFcns',@optimplotfval);

%    corners = plot_region_list(region_list);

    %Plotting for sanity check
    alpha = init(1:2*np);
    L = init(2*np+1:4*np);
    s0 = init(4*np+1:5*np);
    sF = init(5*np+1:6*np);
    x0 = init(6*np+1:7*np);
    y0 = init(7*np+1:8*np);
    psi0 = init(8*np+1:9*np);
    figure
    subplot(2,1,1)
    hold on
    plot_region_list(region_list);
    plot_sequence(alpha, L, s0, sF, x0, y0, psi0, goal);
    hold off

    A = [];
    b = [];
    Aeq = [];
    beq=[];
    ub =[];
    lb = [-inf*ones(1,2*np), zeros(1,2*np), zeros(1,np), zeros(1,np), -inf*ones(1,3*np)];


    constraintsBothNoParam = @(p)constraintsBoth(p, start, goal, region_list, H);
    objectiveBothNoParam = @(p)objectiveBoth(p, w);
%With derivatives
%     tic
%     %[p_opt, fval, exitflag, output] = fmincon(@rosenbrockBoth, init, A, b, Aeq, beq, lb, ub, @unitdisk2, options);
%     [p_opt, fval, exitflag, output] = fmincon(objectiveBothNoParam, init, A, b, Aeq, beq, lb, ub, constraintsBothNoParam, options)
%     toc

    %Without derivatives
     options = optimoptions(@fmincon,'Algorithm','interior-point', 'FiniteDifferenceType','forward', 'CheckGradients', false, 'SpecifyConstraintGradient', false, 'SpecifyObjectiveGradient', false, 'Display','iter','PlotFcns',@optimplotfval, 'MaxIterations', 10000, 'MaxFunctionEvaluations', 200000);
    % 
     tic
     [p_opt, fval, exitflag, output] = fmincon(objectiveBothNoParam, init, A, b, Aeq, beq, lb, ub, constraintsBothNoParam, options)
     toc
     
    %Plotting for sanity check
    alpha = p_opt(1:2*np);
    L = p_opt(2*np+1:4*np);
    s0 = p_opt(4*np+1:5*np);
    sF = p_opt(5*np+1:6*np);
    x0 = p_opt(6*np+1:7*np);
    y0 = p_opt(7*np+1:8*np);
    psi0 = p_opt(8*np+1:9*np);
    opt_output = output;
    total_length = sum(p_opt(2*np+1:6*np))
    figure
    subplot(2,1,1)
    hold on
    plot_region_list(region_list);
    plot_sequence(alpha, L, s0, sF, x0, y0, psi0, goal);
    hold off
    %check constraints are met
    [c_opt, c_eq_opt] = constraintsBoth(p_opt, start, goal, region_list, H)
end

function [f,g] = objectiveBoth(p, w)
weight_vector = ones(size(p));
np = round(length(p)/9);
weight_vector(1:2*np) = w;
W = diag(weight_vector);
    f = p*W*p';
    if nargout > 1
        g = 2*W*p';
    end
end

function [c,ceq,gc,gceq] = constraintsBoth(p, start, goal, regionList, H)
    c = [];
    ceq = [];

    [c, ceq] = constraints(p, start, goal, regionList, H);%[region1;region2]); 
    %c=[];%turn off the inequality constraints until I figure them out
    if nargout > 2
        gc = InequalityGradient(p);% likewise turned off
        gceq = Jacobian(p);
    end
end

    %copypasta from regionConstraints2Segmentsalphadelta.m  -its essential
    %
function [xf, yf, headingf, curvaturef] = sample(alpha, L, k_m)
    funx = @(u)cos(k_m.*u + alpha*u.*u/2);
    xf = integral(funx, 0, L); 
    funy = @(u)sin(k_m.*u + alpha*u.*u/2);
    yf = integral(funy, 0, L);
    deflectionf = k_m*L + alpha*L*L/2;
    headingf = wrapToPi(deflectionf); % heading between +pi and -pi - should make no difference
    curvaturef = k_m + alpha*L;
end

function [xf, yf, headingf, curvaturef] = sample_n(alpha, L)
    n = length(alpha);
    assert(n==length(L));
    k = 0;
    mid_pose = [0,0,0];
    for i = 1:n
        [xf, yf, headingf, curvaturef] = sample(alpha(i), L(i), k);
        mid_pose = compose(mid_pose, [xf, yf, headingf]);
        k = curvaturef;
    end

    xf = mid_pose(1);
    yf = mid_pose(2);
    headingf = mid_pose(3);
    curvaturef = k;
end
function [c, ceq] = constraints(p, start, goal, region_list, H)
    np = round(length(p)/9);
    nr = size(region_list, 1);
    
    alpha = p(1:2*np);
    L = p(2*np+1:4*np);
    s0 = p(4*np+1:5*np);
    sF = p(5*np+1:6*np);
    x0 = p(6*np+1:7*np);
    y0 = p(7*np+1:8*np);
    psi0 = p(8*np+1:9*np);


    k = 0;
    xf_arr = zeros(1,np);
    x0_arr = zeros(1,np);
    yf_arr = zeros(1,np);
    y0_arr = zeros(1,np);
    psif_arr = zeros(1,np);
    psi0_arr = zeros(1,np);
    curvaturem_arr = zeros(1,np);
    curvaturem_ref = zeros(1,np); 
    c_ineq_stack = zeros(1,8*np);

    for i = 1:np
        segment_pose = [x0(i),y0(i),psi0(i)]';
        [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
        first_pose = compose(segment_pose,[s0(i),0,0]);
        mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
        end_pose = compose( mid_pose, [sF(i),0,0]);
        curvaturem_arr(i) = curvaturem;
        xf_arr(i) = end_pose(1);
        yf_arr(i) = end_pose(2);
        psif_arr(i) = end_pose(3);
        x0_arr(i) = segment_pose(1);
        y0_arr(i) = segment_pose(2);
        psi0_arr(i) = segment_pose(3);

        for j = 1:nr
            if H(i, j)==1
                region = region_list(j,:);
                xmin = region(1);
                xmax = region(2);
                ymin = region(3);
                ymax = region(4);
                c_ineq_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-segment_pose(1), segment_pose(1)-xmax, ymin-segment_pose(2), segment_pose(2)-ymax];
            end
        end
        
    end
    c = c_ineq_stack;

    ceq = [[x0(1), y0(1), psi0(1)]-start(1:3), curvaturem_arr - curvaturem_ref, xf_arr - [x0_arr(2:length(x0_arr)), goal(1)], yf_arr - [ y0_arr(2:length(y0_arr)),goal(2)], psif_arr - [ psi0_arr(2:length(psi0_arr)),goal(3)] ];


end
function A = Jacobian(p)
    nr = 1;

    A = zeros(9*nr, 4);
    A(:,1) = dxbydp(p, nr);
    A(:,2) = dybydp(p, nr); %start with this one because (3,2) is what fmin is complaining about
    A(:,3) = dpsibydp(p, nr);
    A(:,4) = dkappabydp(p, nr);
end

function gc = InequalityGradient(p, region_list)
    nr = 1;
    gc = zeros(9*nr, 8*nr);
    % eight constriants per region, need to differentiate with respect to each
    % parameter. Four constraints are based on y and four on x
     for i = 1:nr
         gc(:, i) = -dxbydp(p, nr);
         gc(:, i+1) = dxbydp(p, nr);
         gc(:, i+2) = -dybydp(p, nr);
         gc(:, i+3) = dybydp(p, nr);
         % These ones are all currently zero as the first segment starts at the
         % origin. Fo mlti regions the parameters of the previous segment will
         % be needed somehow
         if i>1
          gc(:, i+4) = -dxbydp(p, nr);
          gc(:, i+5) = dxbydp(p, nr);
          gc(:, i+6) = -dybydp(p, nr);
          gc(:, i+7) = dybydp(p, nr);
         end
     end
    %constraint_stack = zeros(1,8*nr);
    % for i = 1:nr
    %     [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    %     first_pose = compose(start_pose,[s0(i),0,0]);
    %     mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    %     end_pose = compose( mid_pose, [sF(i),0,0]);
    %     
    %     region = region_list(i,:);
    %     xmin = region(1);
    %     xmax = region(2);
    %     ymin = region(3);
    %     ymax = region(4);
    %     constraint_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-start_pose(1), start_pose(1)-xmax, ymin-start_pose(2), start_pose(2)-ymax];
    %     
    %     start_pose = end_pose;
    % end
    % c = constraint_stack;
end

function grad_arr = dxbydp(p, nr)
    % dxbydp = [dxbyda1, dxbyda2, dxbydL1, dxbydL2, dxbyds0, dxbydsF]'
    alpha = p(1:2*nr);
    L = p(2*nr+1:4*nr);
    s0 = p(4*nr+1:5*nr);
    sF = p(5*nr+1:6*nr);
    x0 = p(6*nr+1:7*nr);
    y0 = p(7*nr+1:8*nr);
    psi0 = p(8*nr+1:9*nr);
    psi_f = 0.5*alpha(1)*L(1)^2 + alpha(1)*L(1)*L(2) + 0.5*alpha(2)*L(2)^2;

    grad_arr = zeros(9*nr,1);
    for i = 1:nr
        grad_arr(i) = dxbyda1(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbyda1;
        grad_arr(i+nr) = dxbyda2(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbyda2
        grad_arr(i+2*nr) = dxbydL1(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbydL1
        grad_arr(i+3*nr) = dxbydL2(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbydL2
        grad_arr(i+4*nr) = 1; %dxbyds0
        grad_arr(i+5*nr) = cos(psi_f); %dxbydsF
    end

end

function grad_arr = dybydp(p, nr)
    alpha = p(1:2*nr);
    L = p(2*nr+1:4*nr);
    s0 = p(5*nr);
    sF = p(6*nr);
    x0 = p(7*nr);
    y0 = p(8*nr);
    psi0 = p(9*nr);

    grad_arr = zeros(9,1);
    grad_arr(1) = dybyda1(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybyda1;
    grad_arr(2) = dybyda2(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybyda2
    grad_arr(3) = dybydL1(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybydL1
    grad_arr(4) = dybydL2(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybydL2
    grad_arr(5) = 0; %dybyds0 % Equation(50)
    grad_arr(6) = dybydsF(alpha(1), alpha(2), L(1), L(2)); %dybydsF

end
function grad_arr = dpsibydp(p, nr)
    alpha = p(1:2*nr);
    L = p(2*nr+1:4*nr);
    s0 = p(5*nr);
    sF = p(6*nr);
    x0 = p(7*nr);
    y0 = p(8*nr);
    psi0 = p(9*nr);

    grad_arr = zeros(9,1);
    grad_arr(1) = dpsibyda1(alpha(1), alpha(2), L(1), L(2)); %dpsibyda1;
    grad_arr(2) = dpsibyda2(alpha(1), alpha(2), L(1), L(2)); %dpsibyda2
    grad_arr(3) = dpsibydL1(alpha(1), alpha(2), L(1), L(2)); %dpsibydL1
    grad_arr(4) = dpsibydL2(alpha(1), alpha(2), L(1), L(2)); %dpsibydL2
    grad_arr(5) = 0; %dpsibyds0 % Equation (56)
    grad_arr(6) = 0; %dpsibydsF % Equation (56) also

end
function grad_arr = dkappabydp(p, nr)
    alpha = p(1:2*nr);
    L = p(2*nr+1:4*nr);
    s0 = p(5*nr);
    sF = p(6*nr);
    x0 = p(7*nr);
    y0 = p(8*nr);
    psi0 = p(9*nr);
    grad_arr = zeros(9,1);
    grad_arr(1) = L(1); %dkbyda1;
    grad_arr(2) = L(2); %dkbyda2
    grad_arr(3) = alpha(1); %dkbydL1
    grad_arr(4) = alpha(2); %dkbydL2
    grad_arr(5) = 0; %dkbyds0 % Equation (57)
    grad_arr(6) = 0; %dkbydsF % Equation (57) also
end

function grad = dxbyda1(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;

    fun1 = @(u)-0.5*u.^2.*sin(0.5*alpha_1*u.^2);
    fun2 = @(u)-L_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
    fun3 = @(u)L_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);
    %Equation - now (28)
    grad = integral(fun1, 0, L_1) + cos(delta_1)*integral(fun2,0,L_2) ...
    + (-sin(delta_1)*0.5*L_1^2)*C(alpha_2, L_2, k_m) - sin(delta_1)*integral(fun3,0,L_2) ...
    - (cos(delta_1)*0.5*L_1^2)*S(alpha_2, L_2, k_m) + sF*(L_1*L_2 +0.5*L_1^2)*(-sin(psi_f));
end
function grad = dybyda1(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;

    fun1 = @(u)0.5*u.^2.*cos(alpha_1*u.^2/2);
    fun2 = @(u)-L_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
    fun3 = @(u)L_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);
    %Equation (45)
    grad =  integral(fun1, 0, L_1) + sin(delta_1)*integral(fun2, 0, L_2) ...
         + cos(delta_1)*(0.5*L_1^2)*C(alpha_2, L_2, k_m) + cos(delta_1)*integral(fun3, 0, L_2)...
         + (-sin(delta_1))*(0.5*L_1^2)*S(alpha_2, L_2, k_m) + sF*(L_1*L_2 + 0.5*L_1^2)*cos(psi_f);

end
function grad = dxbyda2(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;
    fun1 = @(u)-0.5*u.^2.*sin(k_m*u + 0.5*alpha_2*u.^2);
    fun2 = @(u)0.5*u.^2.*cos(k_m*u + 0.5*alpha_2*u.^2);
    % Equation (39)
    grad = cos(delta_1)*integral(fun1, 0, L_2) - sin(delta_1)*integral(fun2, 0, L_2) + sF*(-sin(psi_f))*(0.5*L_2^2); 
end

function grad = dybyda2(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;
    fun1 = @(u)-0.5*u.^2.*sin(k_m*u + 0.5*alpha_2*u.^2);
    fun2 = @(u)0.5*u.^2.*cos(k_m*u + 0.5*alpha_2*u.^2);
    % Equation (47)
    grad = sin(delta_1)*integral(fun1, 0, L_2) + cos(delta_1)*integral(fun2, 0, L_2) + sF*cos(psi_f)*0.5*L_2^2;
end

function grad = dxbydL1(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;

    fun2 = @(u)-alpha_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
    fun3 = @(u)alpha_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);

    % Equation-(37)
    grad = cos(0.5*alpha_1*L_1^2) + cos(delta_1)*integral(fun2, 0, L_2) ...
        + (-sin(delta_1)*alpha_1*L_1)*C(alpha_2, L_2, k_m) - sin(delta_1)*integral(fun3, 0, L_2) ...
    - (cos(delta_1)*alpha_1*L_1)*S(alpha_2, L_2, k_m) + sF*(-sin(psi_f))*(alpha_1*L_1 + alpha_1*L_2);
end
function grad = dybydL1(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;

    fun2 = @(u)-alpha_1*u.*sin(alpha_1*L_1*u + 0.5*alpha_2*u.^2);
    fun3 = @(u)alpha_1*u.*cos(alpha_1*L_1*u + 0.5*alpha_2*u.^2);

    % Equation(38)
    grad = sin(0.5*alpha_1*L_1^2) + sin(delta_1)*integral(fun2, 0, L_2) ...
        + cos(delta_1)*alpha_1*L_1*C(alpha_2, L_2, k_m) + cos(delta_1)*integral(fun3, 0, L_2) ...
        - sin(delta_1)*alpha_1*L_1*S(alpha_2, L_2, k_m) + sF*cos(psi_f)*(alpha_1*L_1 + alpha_1*L_2);
end
function grad = dxbydL2(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
    %Equation (36)
    grad = cos(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*cos(delta_1) - sin(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*sin(delta_1)+ sF*(-sin(psi_f))*(alpha_1*L_1 + alpha_2*L_2);
end
function grad = dybydL2(alpha_1, alpha_2, L_1, L_2, sF)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
    %Equation (49)
    grad = cos(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*sin(delta_1) + sin(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*cos(delta_1) + sF*(cos(psi_f))*(alpha_1*L_1 + alpha_2*L_2);
end
function xf = C(alpha, L, k_m)
    funx = @(u)cos(k_m.*u + alpha*u.*u/2);
    xf = integral(funx, 0, L); 
end
function yf = S(alpha, L, k_m)
    funy = @(u)sin(k_m.*u + alpha*u.*u/2);
    yf = integral(funy, 0, L);
end



function grad = dybydsF(alpha_1, alpha_2, L_1, L_2)
    k_m = alpha_1*L_1;
    delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
    psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
    % Equation(51)
    grad = sin(psi_f);
end

function grad = dpsibyda1(alpha_1, alpha_2, L_1, L_2)
    grad = 0.5*L_1^2 + L_1*L_2;
end
function grad = dpsibyda2(alpha_1, alpha_2, L_1, L_2)
    grad = 0.5*L_2^2;
end
function grad = dpsibydL1(alpha_1, alpha_2, L_1, L_2)
    grad = alpha_1*L_1 + alpha_1*L_2; % equation(55)
end
function grad = dpsibydL2(alpha_1, alpha_2, L_1, L_2)
    grad = alpha_1*L_1 + alpha_2*L_2; % equation(56)
end

% Adapted from regionConstraints2Segmentsalphadelta
function plot_sequence(alpha, L, s0, sF, x0, y0, psi0, goal)


    nr = length(alpha)/2;
    subplot(2,1,1);
    hold on
    %just to setup the colours for the legend
    subplot(2,1,2);
    plot([0,0], [1,0], '-r'); %first curvature
    hold on
    plot([0,0], [1,0], '-g'); %then sharpness
    hold on
    for i = 1:nr
        %%from constraints
        [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
        first_pose = compose([x0(i), y0(i), psi0(i)], [s0(i), 0, 0]);
        mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
        end_pose_alt = compose( mid_pose, [sF(i),0,0]);

        %%
        X = x0(i);
        Y = y0(i);
        Heading = psi0(i);
        Curvature = 0;
        Ss = 0;
        [Xi, Yi, Headingi, Curvaturei, Si] = PlotClothoidSequence(alpha(2*i-1:2*i), L(2*i-1:2*i));
        [X, Y, Heading, Curvature, Ss] = join( [X, Y, Heading, Curvature, Ss], [s0(i),0,0,0,s0(i)]);
        [X, Y, Heading, Curvature, Ss] = join([X, Y, Heading, Curvature, Ss], [Xi, Yi, Headingi, Curvaturei, Si]);
        [X, Y, Heading, Curvature, Ss] = join( [X, Y, Heading, Curvature, Ss], [0,sF(i),0,0,0,0,0,0,0,sF(i)]);
        subplot(2,1,1);
        plot(X, Y)
        hold on
        %%
        [xm, ym, headingm, curvaturem] = sample(alpha(2*i-1), L(2*i-1),0);
        [xmm, ymm, headingmm, curvaturemm] = sample(alpha(2*i), L(2*i), curvaturem);
        first_pose = compose( [x0(i), y0(i), psi0(i)], [s0(i),0,0]);
        mid1_pose = compose( first_pose, [xm, ym, headingm]);
        mid2_pose = compose( mid1_pose, [xmm, ymm, headingmm]);
        end_pose = compose( mid2_pose, [sF(i),0,0]);
        subplot(2,1,1);
        plot([first_pose(1), mid1_pose(1), mid2_pose(1),end_pose_alt(1)], [first_pose(2), mid1_pose(2),mid2_pose(2), end_pose_alt(2)], 'ob');
        subplot(2,1,2);
        if(i==1)
            cumS = 0;
        else
            cumS = sum(L(1:2*i-2))+sum(s0(1:i-1))+sum(sF(1:i-1));
        end
        %plot([cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [first_pose(3), mid1_pose(3), mid2_pose(3), end_pose(3)], 'or');
        plot([cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0, curvaturem, curvaturemm, 0], 'og');
        subplot(2,1,2);
        plot([cumS, cumS + s0(i),cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i),cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0,0, alpha(2*i-1), alpha(2*i-1), alpha(2*i), alpha(2*i), 0 ,0], 'or');
        subplot(2,1,2);
        plot([cumS, cumS + s0(i),cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i),cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0,0, alpha(2*i-1), alpha(2*i-1), alpha(2*i), alpha(2*i), 0 ,0], '-r');

        hold on
        plot(cumS + Ss, Curvature, '-g')
        xlabel('s[m]')
    end


    subplot(2,1,1);
    hold on
    plot(goal(1), goal(2), 'cx');
    arrow_length = 0.1;
    plot([goal(1), goal(1) + arrow_length *cos(goal(3))], [goal(2), goal(2) + arrow_length *sin(goal(3))], 'c-');
    xlabel('x[m]');
    ylabel('y[m]')
    title(['X_G =[' num2str(goal) ']']);
    axis equal
    subplot(2,1,2);
    legend('Sharpness [rad.m^{-2}]', 'Curvature [rad.m^{-1}]')

end
