% Run the path finding function with varying w parameter to check
% convergence time
%regions are axis aligned: [xmin, xmax, ymin, ymax]
region1 = [0.0, 5.0, -5.0, 5.0];
region2 = [0.0, 11.0, 2.5, 5.0];
region3 = [6.0, 11.0, -5.0, 5.0];
region_list = [region1; region2; region3];

xhat = 11;
yhat = 0;
psihat = 0*pi/180;
khat = 0;
goal = [xhat, yhat, psihat, khat];
    
power =  -3:3;
w_arr = 10.^power;
t_arr = zeros(size(w_arr));
total_length_arr = zeros(size(w_arr)); 
%each field in the ouput structure as an array
iterations = zeros(size(w_arr));
funcCount  = zeros(size(w_arr));
constrviolation = zeros(size(w_arr));
stepsize = zeros(size(w_arr));
algorithm  = string(zeros(size(w_arr)));
firstorderopt = zeros(size(w_arr));
cgiterations = zeros(size(w_arr));
message = string(zeros(size(w_arr)));
i=1;
for w = w_arr
plot_all = false;
tic
[opt_output, total_length] = regionConstraints2Segments(w, goal, region_list, plot_all);
t_arr(i) = toc;
total_length_arr(i) = total_length;
iterations(i) = opt_output.iterations;
funcCount(i)  = opt_output.funcCount;
constrviolation(i) = opt_output.constrviolation;
stepsize(i) = opt_output.stepsize;
algorithm(i)  = opt_output.algorithm;
firstorderopt(i) = opt_output.firstorderopt;
if strcmp(opt_output.algorithm, 'interior-point')
    cgiterations(i) = opt_output.cgiterations;
end
message(i) = opt_output.message;
i=i+1;
end
 
save(string(['results-', opt_output.algorithm, '.mat']),'constrviolation', 'firstorderopt', 'funcCount', 'iterations', 'message', 'power', 'stepsize', 't_arr', 'total_length_arr', 'w_arr');



