
%regions are axis aligned: [xmin, xmax, ymin, ymax]
region1 = [0.0, 12.0, 0, 12.0];
region2 = [0.0, 12.0, 0, 12.0];



region_list = [region1; region2];
figure
subplot(2,1,1)
hold on
corners = plot_region_list(region_list);

nr = size(region_list, 1)

%origin = [0,0]';
%goal = [11, 0, 0, 0]'; % [x, y, heading, curvature]

xhat = 12;
yhat = 10;
for deg = -20:10:30
    psihat = deg*pi/180;
    khat = 0;
    goal = [xhat, yhat, psihat, khat]

    start = [0,0,0,0];
    goal_rel = decompose(goal, start);
    
    %% Solve for the parameters to reach a point with any heading
    % I wrote a paper about how I could do this with a straight line at either
    % end. In this simplified script 
    % I do not handle theta < phi with a four clothoid special case
    % how is deflection a sensible parameter for optimization?

    % 6 parameters per region, 2xalpha, 2xdelta, 2xS for every region 
    init = [1, -1, -1, 1, ones(1,nr), ones(1,nr)];%, ones(1,nr), ones(1,nr)];

    %check init
    alpha = init(1:2*nr);
    %delta = init(2*nr+1:4*nr);
    L = init(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
    s0 = zeros(1, nr);%init(4*nr+1:5*nr);
    sF = zeros(1, nr);%init(5*nr+1:6*nr);
    %plot_sequence(alpha, L, s0, sF, start, goal);

    A = [];
    b = [];
    Aeq = [];
    beq=[];
    ub =[];
    lb = [-inf*ones(1,nr), -inf*ones(1,nr), 0*ones(1,nr), 0*ones(1,nr)];% 0*ones(1,nr), 0*ones(1,nr)];
    %ub = [inf, 0, pi, 0, inf, inf];
    objective = @(p)p(1:2*nr)*p(1:2*nr)';% + p(2*nr+1:4*nr)*p(2*nr+1:4*nr)';%+ p(4*nr+1:6*nr)*p(4*nr+1:6*nr)';
    constraints_np = @(p)constraints(p, goal, region_list);
    options = optimoptions('fmincon','Display','iter','Algorithm','interior-point', 'MaxIterations', 1000, 'MaxFunctionEvaluations', 10000);
    [p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);

    %check constraints are satified
    [c_opt, ceq_opt] = constraints(p_opt, goal, region_list)
    alpha = p_opt(1:2*nr);
    %delta = p_opt(2*nr+1:4*nr);
    L = p_opt(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
    s0 = zeros(1, nr);%p_opt(4*nr+1:5*nr);
    sF = zeros(1, nr);%p_opt(5*nr+1:6*nr);
    opt_output = output
    %figure
    subplot(2,1,1)
    hold on
    plot_region_list(region_list);
    plot_sequence(alpha, L, s0, sF, goal, false);
    total_length  = sum(L) + sum(s0) + sum(sF);
    save(['X_G =[' num2str(goal(1:2)) ' ' num2str(deg) '].mat'], 'alpha', 'L', 's0', 'sF', 'start', 'goal', 'fval', 'exitflag', 'output', 'p_opt')
    %hold off
end
hold off

function [c, ceq] = constraints(p, goal, region_list)
nr = size(region_list, 1);

alpha = p(1:2*nr);
%delta = p(2*nr+1:4*nr);
L = p(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
    s0 = zeros(1, nr);%p_opt(4*nr+1:5*nr);
    sF = zeros(1, nr);%p_opt(5*nr+1:6*nr);

start_pose = [0,0,0]';
k = 0;
curvaturem_arr = zeros(1,nr);
curvaturem_ref = zeros(1,nr); 
constraint_stack = zeros(1,8*nr);
for i = 1:nr
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose(start_pose,[s0(i),0,0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF(i),0,0]);
    curvaturem_arr(i) = curvaturem;
    
    region = region_list(i,:);
    xmin = region(1);
    xmax = region(2);
    ymin = region(3);
    ymax = region(4);
    constraint_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-start_pose(1), start_pose(1)-xmax, ymin-start_pose(2), start_pose(2)-ymax];
    
    start_pose = end_pose;
end
c = constraint_stack;

ceq = [end_pose'-goal(1:3), curvaturem_arr - curvaturem_ref];



end

% function [xf, yf, headingf, curvaturef] = sample(alpha, L, k_m)
% funx = @(u)cos(k_m.*u + alpha*u.*u/2);
% xf = integral(funx, 0, L); 
% funy = @(u)sin(k_m.*u + alpha*u.*u/2);
% yf = integral(funy, 0, L);
% deflectionf = k_m*L + alpha*L*L/2;
% headingf = wrapToPi(deflectionf); % heading between +pi and -pi - should make no difference
% curvaturef = k_m + alpha*L;
% end
% 
% function [xf, yf, headingf, curvaturef] = sample_n(alpha, L)
% n = length(alpha);
% assert(n==length(L));
% k = 0;
% mid_pose = [0,0,0];
% for i = 1:n
%     [xf, yf, headingf, curvaturef] = sample(alpha(i), L(i), k);
%     mid_pose = compose(mid_pose, [xf, yf, headingf]);
%     k = curvaturef;
% end
% 
% xf = mid_pose(1);
% yf = mid_pose(2);
% headingf = mid_pose(3);
% curvaturef = k;
% end
% 
% function plot_sequence(alpha, L, s0, sF, init_pose, goal)
% if nargin==4
%     init_pose = [0,0,0];
% end
% X = init_pose(1);
% Y = init_pose(2);
% Heading = init_pose(3);
% Curvature = 0;
% S = 0;
% end_pose = init_pose';
% end_pose_alt = init_pose';
% nr = length(alpha)/2;
% subplot(2,1,1);
% hold on
% subplot(2,1,2);
% hold on
% for i = 1:nr
%     %%from constraints
%     [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
%     first_pose = compose(end_pose_alt,[s0(i),0,0]);
%     mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
%     end_pose_alt = compose( mid_pose, [sF(i),0,0]);
%     
%     %%
%     [Xi, Yi, Headingi, Curvaturei, Si] = PlotClothoidSequence(alpha(2*i-1:2*i), L(2*i-1:2*i));
%     [X, Y, Heading, Curvature, S] = join( [X, Y, Heading, Curvature, S], [s0(i),0,0,0,s0(i)]);
%     [X, Y, Heading, Curvature, S] = join([X, Y, Heading, Curvature, S], [Xi, Yi, Headingi, Curvaturei, Si]);
%     [X, Y, Heading, Curvature, S] = join( [X, Y, Heading, Curvature, S], [0,sF(i),0,0,0,0,0,0,0,sF(i)]);
%     
%     %%
%     [xm, ym, headingm, curvaturem] = sample(alpha(2*i-1), L(2*i-1),0);
%     [xmm, ymm, headingmm, curvaturemm] = sample(alpha(2*i), L(2*i), curvaturem);
%     first_pose = compose( end_pose, [s0(i),0,0]);
%     mid1_pose = compose( first_pose, [xm, ym, headingm]);
%     mid2_pose = compose( mid1_pose, [xmm, ymm, headingmm]);
%     end_pose = compose( mid2_pose, [sF(i),0,0]);
%     subplot(2,1,1);
%     %plot([first_pose(1), mid1_pose(1), mid2_pose(1),end_pose_alt(1)], [first_pose(2), mid1_pose(2),mid2_pose(2), end_pose_alt(2)], 'ob');
%     subplot(2,1,2);
%     if(i==1)
%         cumS = 0;
%     else
%         cumS = sum(L(1:2*i-2))+sum(s0(1:i-1))+sum(sF(1:i-1));
%     end
%     plot([cumS, cumS + s0(i),cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i),cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0,0, alpha(2*i-1), alpha(2*i-1), alpha(2*i), alpha(2*i), 0 ,0], '-r');
% 
%     plot([cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0, curvaturem, curvaturemm, 0], 'og');
% end
% 
% 
% subplot(2,1,1);
% plot(X, Y)
% hold on
% plot(goal(1), goal(2), 'cx');
% arrow_length = 0.6;
% plot([goal(1), goal(1) + arrow_length *cos(goal(3))], [goal(2), goal(2) + arrow_length *sin(goal(3))], 'c-');
% xlabel('x[m]');
% ylabel('y[m]')
% title(['X_G =[' num2str(goal(1:2)) ' \theta ]']);
% axis equal
% 
% subplot(2,1,2);
% plot(S, Curvature, '-g')
% xlabel('s[m]')
% 
% 
% legend('Sharpness [rad.m^{-2}]', 'Curvature [rad.m^{-1}]')
%  
% end