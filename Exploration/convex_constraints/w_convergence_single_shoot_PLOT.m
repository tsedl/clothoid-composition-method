% Load the mat files and produce cool graphs
if ~exist('w_convergence_results_sqp.mat', 'file')
    w_convergence_single_shoot_TEST
end

load('w_convergence_results_sqp.mat')
figure(1)
loglog(w_arr, funcCount, '-x')
hold on
figure(2)
semilogx(w_arr, t_arr, '-x')
hold on
figure(3)
loglog(w_arr, firstorderopt, '-x')
hold on
figure(4)
loglog(w_arr, constrviolation, '-x')
hold on

%loglog(w_arr, t_arr) % very similar to funcCount
%loglog(w_arr, total_length_arr) % increases as reducing shaprness becomes more important (w becomes large) 
%loglog(w_arr, iterations) % mirrors funcCount
%loglog(w_arr, firstorderopt) % related to total length, should be 10^-5
% increases as w increases indicating the solutions are worse, maybe
% interesting

clear 
load('w_convergence_results_interior-point.mat')
figure(1)
loglog(w_arr(1:end-1), funcCount(1:end-1), '-x')

title('Impact of Objective Weighting on funcCount')
xlabel('sharpness weight')
ylabel('funcCount')
legend('sqp', 'interior-point')

figure(2)
semilogx(w_arr(1:end-1), t_arr(1:end-1), '-x')
title('Impact of Objective Weighting on Execution Time')
xlabel('sharpness weight')
ylabel('time [seconds]')
legend('sqp', 'interior-point')

figure(3)
loglog(w_arr(1:end-1), firstorderopt(1:end-1), '-x')
title('Impact of Objective Weighting on Optimality')
xlabel('sharpness weight')
ylabel('firstorderopt')
legend('sqp', 'interior-point')

figure(4)
loglog(w_arr(1:end-1), constrviolation(1:end-1), '-x')
title('Impact of Objective Weighting on Constraint Violation')
xlabel('sharpness weight')
ylabel('constrviolation')
legend('sqp', 'interior-point')



%loglog(w_arr, t_arr)
%loglog(w_arr, total_length_arr)
%loglog(w_arr, iterations)
%loglog(w_arr, firstorderopt)