%regions are axis aligned: [xmin, xmax, ymin, ymax]
region1 = [0.0, 5.0, -5.0, 5.0];
region2 = [0.0, 11.0, 2.5, 5.0];
region3 = [6.0, 11.0, -5.0, 5.0];

origin = [0,0]';
goal = [11, 0, 0, 0]'; % [x, y, heading, curvature]

region_list = [region1; region2; region3];
corners = plot_region_list(region_list);

% use henries construction for two segments in one region
% connecting certain corners
path_corners = [[5, 2.5]', [6,2.5]', [11., 0]']; % add the goal on the end
last_point = origin;
last_theta = 0;
theta = zeros(1, length(path_corners(1,:)));
deflection = zeros(1, length(path_corners(1,:)));
relative_angle = zeros(1, length(path_corners(1,:)));
init_alpha = zeros(1, 2*length(path_corners(1,:)));
init_L = zeros(1, 2*length(path_corners(1,:)));
for i = 1:length(path_corners(1,:))
    point_i = path_corners(:,i);
    relative_pos_i = point_i - last_point;
    theta(i) = atan2(relative_pos_i(2), relative_pos_i(1));% target heading should be -*the same as* the angle of a line segment
    relative_angle(i) = theta(i) - last_theta;
    deflection(i) = theta(i)- last_theta;
    % two segments per path_corner
    init_alpha(2*i - 1) = 8*relative_angle(i)/(relative_pos_i(2)^2 + relative_pos_i(1)^2);
    init_alpha(2*i ) = -8*relative_angle(i)/(relative_pos_i(2)^2 + relative_pos_i(1)^2);
    init_L(2*i - 1) = sqrt(relative_pos_i(2)^2 + relative_pos_i(1)^2)/2;
    init_L(2*i) = sqrt(relative_pos_i(2)^2 + relative_pos_i(1)^2)/2;
    last_point = point_i;
    last_theta = deflection(i) + theta(i);
end
init = [init_alpha, init_L];

amax = 8;
kmax = 8;
Lmax = 10;
Lmin = 0.01;

%zero out the sharpness to get a straight lineignoring the obstacles
% init(1:6) = init(1:6)*0;
% init(7:12) = [2.5, 2.5, 0.5, 0.5, 2.5, 2.5];

[x, y, heading, curvature, s] = IntegrateClothoidSequence_klimit(init(1:6), init(7:12), kmax);
plot(x, y, 'xb');

A=[]; B=[];Aeq=[]; Beq=[];
lb=[-amax, -amax, -amax, -amax, -amax, -amax, Lmin, Lmin, Lmin, Lmin,Lmin, Lmin]; 
ub=[amax, amax, amax, amax, amax, amax, Lmax, Lmax, Lmax, Lmax, Lmax, Lmax];
c = [];
constraints_np = @(p)constraints(p, goal, region_list, kmax);%ineq_constraints(p, regions, kmax)];
objective_np = @(p)objective(p, kmax);
options = optimoptions('fmincon','Display','iter','Algorithm','interior-point');%, 'StepTolerance', 1e-10);
[p_opt,fval,exitflag,output]  = fmincon( objective_np, init, A, B, Aeq, Beq, lb, ub, constraints_np, options)

alpha = p_opt(1:6);
L = p_opt(7:12);
[x, y, heading, curvature, s] = IntegrateClothoidSequence_klimit(alpha, L, kmax);

startL=0;
for i = 1:length(L)/2
    pairL = L(2*i-1) + L(2*i);
    srange = s>startL & s<= startL + pairL;
    xi = x(srange);
    yi = y(srange);
    headingi = heading(srange);
    curvaturei = curvature(srange);
    startL = startL + pairL; 
    plot(xi, yi, 'x');
end


path_end = [x(length(x)), y(length(y)), heading(length(heading)), curvature(length(curvature))]'
%ceq = goal - path_end
p_opt
lb
ub
f = objective(p_opt, kmax)
[c, ceq] = constraints(p_opt, goal, region_list, kmax)

function f = objective(p, kmax)
alpha = p(1:6);
L = p(7:12);
[x, y, heading, curvature, s] = IntegrateClothoidSequence_klimit(alpha, L, kmax);
n_samples = 10;
[x, y, heading, curvature, s] = DownsampleClothoid(x, y, heading, curvature, s, n_samples);
f = sum(y); % proxy for the path distance integral, correct while the reference path is along the x axis
end

function [c, ceq] = constraints(p, goal, region_list, kmax) % regions
    alpha = p(1:6);
    L = p(7:12);
    [x, y, heading, curvature, s] = IntegrateClothoidSequence_klimit(alpha, L, kmax);
    
    path_end = [x(length(x)), y(length(y)), heading(length(heading)), curvature(length(curvature))]';    
    ceq = goal - path_end;
    
    c = zeros(1,8*length(region_list(:,1)));
    startL = 0;
    for i = 1:length(L)/2
        % the curves are arranged so that the first curve must lie inside
        % the first region and so on
        region = region_list(i, :);
        xmin = region(1);
        xmax = region(2);
        ymin = region(3);
        ymax = region(4);
        

        pairL = L(2*i-1) + L(2*i);
        srange = s>startL & s<= startL + pairL;
        
        xi = x(srange);
        yi = y(srange);
        headingi = heading(srange);
        curvaturei = curvature(srange);
        
        start_pose_i= [xi(1), yi(1), headingi(1), curvaturei(1)]';
        end_pose_i = [xi(length(xi)), yi(length(yi)), headingi(length(headingi)), curvaturei(length(curvaturei))]';
        c(8*i-7 : 8*i-4) = -[start_pose_i(1) - xmin, xmax - start_pose_i(1), start_pose_i(2) - ymin, ymax - start_pose_i(2)];
        c(8*i-3 : 8*i) = -[end_pose_i(1) - xmin, xmax - end_pose_i(1), end_pose_i(2) - ymin, ymax - end_pose_i(2)];
        
        startL = startL + pairL; 
    end
    
    
    % just need to know if inequality constraints are less than or greater
    % than zero, then check path_end(1)<xmax and path_end(1)>xmin etc
%     for i = 1:length(regions(:,1))
%         region = regions(i,:);
%         c(i:i+4) = [path_end(1)-region(1), path_end(1)-region(1)]
%     end
end

function corners = plot_region_list(region_list)
    corners = [];
    for i = 1:length(region_list(:,1))
        region = region_list(i,:);
        cornersx = [region(1), region(2), region(2), region(1), region(1)]; 
        cornersy = [region(3), region(3), region(4), region(4), region(3)];
        plot(cornersx, cornersy);
        hold on
        corners = [corners, [cornersx; cornersy]];
    end
end