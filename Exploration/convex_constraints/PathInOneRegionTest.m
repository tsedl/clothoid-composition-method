%regions are axis aligned: [xmin, xmax, ymin, ymax]
region1 = [0.0, 5.0, -5.0, 5.0];
region2 = [0.0, 11.0, 2.5, 5.0];
region3 = [6.0, 11.0, -5.0, 5.0];

goal = [11, 0, 0, 0]; % [x, y, heading, curvature]

plot_region_list([region1; region2; region3]);

% use cubic approx, meeting corner points to initialize
%x1 = 5, y1 = 2.5, alpha1 = sqrt(100/125), theta1 = 12.5*0.8944
init = [0.08944, -2, 0.08944, 5, 1, 5];
[x, y, heading, curvature, s] = PlotClothoidSequence(init(1:3), init(4:6));
    plot(x, y, 'xb');


function f = objective(x)
end
function ceq = constraints(x, goal)
[x, y, heading, curvature, s] = PlotClothoidSequence(init(1:3), init(4:6))
end
function c = ineq_constraints(x, regions)
end

function corners = plot_region_list(region_list)
    corners = [];
    for i = 1:length(region_list(:,1))
        region = region_list(i,:);
        cornersx = [region(1), region(2), region(2), region(1), region(1)]; 
        cornersy = [region(3), region(3), region(4), region(4), region(3)];
        plot(cornersx, cornersy);
        hold on
        corners = [corners, [cornersx, cornersy]];
    end
end