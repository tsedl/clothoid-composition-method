function [opt_output, total_length] = f_regionConstraints2Segments(region_list, start , goal, w ,plot_all, init, H)
%% Solve for the parameters to reach a point with any heading
% I wrote a paper about how I could do this with a straight line at either
% end. In this simplified script 
% I do not handle theta < phi with a four clothoid special case
% how is deflection a sensible parameter for optimization?

if nargin <1
    %regions are axis aligned: [xmin, xmax, ymin, ymax]
    region1 = [0.0, 5.0, -5.0, 5.0];
    region2 = [0.0, 11.0, 2.5, 5.0];
    region3 = [6.0, 11.0, -5.0, 5.0];
    region_list = [region1; region2; region3];
end
if nargin <2
    start= [0,0,0,0];
end
if nargin <3
    xhat = 11;
    yhat = 0;
    psihat = 0*pi/180;
    khat = 0;
    goal = [xhat, yhat, psihat, khat];
end
if nargin <4
   w = 10^1;
end
if nargin <5
   plot_all = true;%false;
end
if nargin <6
    nr = size(region_list, 1);
    np = nr;
    alpha_plus = ones(1,np);
    alpha_minus = -ones(1, np);
    init_alpha = ones(1,2*np);
    init_alpha(1:2:end-1) = alpha_plus;
    init_alpha(2:2:end) = alpha_minus;
    init_L = ones(1,2*np);
    init =[init_alpha, init_L,  ones(1,np), ones(1,np)]
    % 6 parameters per region, 2xalpha, 2xL, 2xS for every region 
    %init = [1, 1, -1, -1, 1, 1, ones(1,nr), ones(1,nr), ones(1,nr), ones(1,nr)];
end
%overwrite the total number with the number on the path (if init given)
np = length(init)/6;
if nargin <7
    H = eye(np, nr);
end


if plot_all
    figure
    subplot(2,1,1)
    hold on
    corners = plot_region_list(region_list);
end

%check init
alpha = init(1:2*np);
L = init(2*np+1:4*np);
s0 = init(4*np+1:5*np);
sF = init(5*np+1:6*np);
if plot_all
    plot_sequence(alpha, L, s0, sF, start, goal, true);
end
A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf*ones(1,np), -inf*ones(1,np), 0*ones(1,np), 0*ones(1,np), 0*ones(1,np), 0*ones(1,np)];
%ub = [inf, 0, pi, 0, inf, inf];
w = w % print w, might come in handy
objective = @(p)w*p(1:2*np)*p(1:2*np)' + p(2*np+1:6*np)*p(2*np+1:6*np)' ;
constraints_np = @(p)constraints(p, start, goal, region_list, H);
options = optimoptions('fmincon','Display','iter','Algorithm','sqp', 'MaxIterations', 10000, 'MaxFunctionEvaluations', 200000);
tic
[p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);
toc
%check constraints are satified
[c_opt, ceq_opt] = constraints(p_opt, start, goal, region_list, H)
alpha = p_opt(1:2*np);
L = p_opt(2*np+1:4*np);
s0 = p_opt(4*np+1:5*np);
sF = p_opt(5*np+1:6*np);
opt_output = output
total_length = sum(p_opt(2*np+1:6*np))

if(plot_all)
    figure
    subplot(2,1,1)
    hold on
    plot_region_list(region_list);
    plot_sequence(alpha, L, s0, sF, start, goal, true);
    hold off
end

end

function [c, ceq] = constraints(p, start, goal, region_list, H)
% np is the number of regions traversed
% nr is the total number in the map
% H is a binary matrix with H(i,j)=1 if the ith path couplet is partially   
% within region j  
nr = size(H, 2);%size(region_list, 1);
np = size(H, 1);

alpha = p(1:2*np);
L = p(2*np+1:4*np);
s0 = p(4*np+1:5*np);
sF = p(5*np+1:6*np);

start_pose = start';
k = 0;
curvaturem_arr = zeros(1,np);
curvaturem_ref = zeros(1,np); 
constraint_stack = zeros(1,8*np);
for i = 1:np
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose(start_pose,[s0(i),0,0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF(i),0,0]);
    curvaturem_arr(i) = curvaturem;
    for j = 1:nr
        if H(i, j)==true

            region = region_list(i,:);
            xmin = region(1);
            xmax = region(2);
            ymin = region(3);
            ymax = region(4);
            constraint_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-start_pose(1), start_pose(1)-xmax, ymin-start_pose(2), start_pose(2)-ymax];

            
        end
    end
    start_pose = end_pose;
end
c = constraint_stack;

ceq = [end_pose'-goal(1:3), curvaturem_arr - curvaturem_ref];


end

