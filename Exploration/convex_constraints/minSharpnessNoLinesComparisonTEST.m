
%regions are axis aligned: [xmin, xmax, ymin, ymax]
region1 = [0.0, 12.0, 0, 12.0];
region2 = [0.0, 12.0, 0, 12.0];



region_list = [region1; region2];
figure
subplot(2,1,1)
hold on
corners = plot_region_list(region_list);

nr = size(region_list, 1)

%origin = [0,0]';
%goal = [11, 0, 0, 0]'; % [x, y, heading, curvature]

xhat = 12;
yhat = 10;
for deg = -20:10:30
    psihat = deg*pi/180;
    khat = 0;
    goal = [xhat, yhat, psihat, khat]

    start = [0,0,0,0];
    goal_rel = decompose(goal, start);
    
    %% Solve for the parameters to reach a point with any heading
    % I wrote a paper about how I could do this with a straight line at either
    % end. In this simplified script 
    % I do not handle theta < phi with a four clothoid special case
    % how is deflection a sensible parameter for optimization?

    % 4 parameters per region, 2xalpha, 2xL for every region (two regions overlap fully to allow S shape) 
    init = [1, -1, -1, 1, ones(1,nr)];

    %check init
    alpha = init(1:2*nr);
    %delta = init(2*nr+1:4*nr);
    L = init(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
    s0 = zeros(1, nr);%init(4*nr+1:5*nr);
    sF = zeros(1, nr);%init(5*nr+1:6*nr);
    %plot_sequence(alpha, L, s0, sF, start, goal);

    A = [];
    b = [];
    Aeq = [];
    beq=[];
    ub =[];
    lb = [-inf*ones(1,nr), -inf*ones(1,nr), 0*ones(1,nr), 0*ones(1,nr)];% 0*ones(1,nr), 0*ones(1,nr)];
    %ub = [inf, 0, pi, 0, inf, inf];
    objective = @(p)p(1:2*nr)*p(1:2*nr)';% + p(2*nr+1:4*nr)*p(2*nr+1:4*nr)';%+ p(4*nr+1:6*nr)*p(4*nr+1:6*nr)';
    constraints_np = @(p)constraints(p, goal, region_list);
    options = optimoptions('fmincon','Display','iter','Algorithm','interior-point', 'MaxIterations', 1000, 'MaxFunctionEvaluations', 10000);
    [p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);

    %check constraints are satified
    [c_opt, ceq_opt] = constraints(p_opt, goal, region_list)
    alpha = p_opt(1:2*nr);
    %delta = p_opt(2*nr+1:4*nr);
    L = p_opt(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
    s0 = zeros(1, nr);%p_opt(4*nr+1:5*nr);
    sF = zeros(1, nr);%p_opt(5*nr+1:6*nr);
    opt_output = output
    %figure
    subplot(2,1,1)
    hold on
    plot_region_list(region_list);
    plot_sequence(alpha, L, s0, sF, goal, false);
    total_length  = sum(L) + sum(s0) + sum(sF);
    save(['X_G =[' num2str(goal(1:2)) ' ' num2str(deg) '].mat'], 'alpha', 'L', 's0', 'sF', 'start', 'goal', 'fval', 'exitflag', 'output', 'p_opt')
    %hold off
end
hold off

function [c, ceq] = constraints(p, goal, region_list)
nr = size(region_list, 1);

alpha = p(1:2*nr);
%delta = p(2*nr+1:4*nr);
L = p(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
    s0 = zeros(1, nr);%p_opt(4*nr+1:5*nr);
    sF = zeros(1, nr);%p_opt(5*nr+1:6*nr);

start_pose = [0,0,0]';
k = 0;
curvaturem_arr = zeros(1,nr);
curvaturem_ref = zeros(1,nr); 
constraint_stack = zeros(1,8*nr);
for i = 1:nr
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose(start_pose,[s0(i),0,0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF(i),0,0]);
    curvaturem_arr(i) = curvaturem;
    
    region = region_list(i,:);
    xmin = region(1);
    xmax = region(2);
    ymin = region(3);
    ymax = region(4);
    constraint_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-start_pose(1), start_pose(1)-xmax, ymin-start_pose(2), start_pose(2)-ymax];
    
    start_pose = end_pose;
end
c = constraint_stack;

ceq = [end_pose'-goal(1:3), curvaturem_arr - curvaturem_ref];



end

