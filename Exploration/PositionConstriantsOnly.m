xhat = 1;
yhat = 1;
goal = [xhat, yhat]

%% Solve for the parameters to reach a point with any heading
init = [1, 1];
A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf, 0];
objective = @(p)p*p';
constraints_np = @(p)constraints(p, goal);
[p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np);
[c_opt, ceq_opt] = constraints(p_opt, goal)
alpha = p_opt(1)
L = p_opt(2)
k=0;
[x, y] = sample(alpha, L, k)
heading = alpha*L*L/2
curvature = alpha*L
opt_message = output.message

function [c, ceq] = constraints(p, goal)
c = [];
alpha = p(1);
L = p(2);
k = 0;
[xf, yf] = sample(alpha, L, k);
ceq = [xf, yf] - goal;
end

function [xf, yf] = sample(alpha_2, L_2, k_m)

%% copied from CheckDerivativesSecondCurve.m 155-158
funx = @(u)cos(k_m.*u + alpha_2*u.*u/2);
xf = integral(funx, 0, L_2); 
funy = @(u)sin(k_m.*u + alpha_2*u.*u/2);
yf = integral(funy, 0, L_2);

end