use_derivs = true
plot_path = true
algorithm ='sqp';%'interior-point'

% fmincon plotfns
'optimplotx' %plots the current point
'optimplotfunccount' %plots the function count
'optimplotfval' %plots the function value
'optimplotconstrviolation' % plots the maximum constraint violation
'optimplotresnorm' %plots the norm of the residuals
'optimplotfirstorderopt' %plots the first-order of optimality
'optimplotstepsize' %plots the step size
'optimplotmilp' % plots the gap for mixed-integer linear programs
%[x fval exitflag output] = fmincon(@rosenboth,...
 %  [-1;2],[],[],[],[],[],[],@unitdiskb,options);

goal = [8,6,40*pi/180];
%regions are axis aligned: [xmin, xmax, ymin, ymax]
region1 = [0.0, 10.0, -10.0, 10.0];
region2 = [0.0, 11.0, 2.5, 5.0];
region3 = [6.0, 11.0, -5.0, 5.0];
region_list = [region1];
 
 %init = [1,1];
init = [0.1, -0.1, 1, 1, 0, 0];

A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf, -inf, 0, 0, 0, 0];

constraintsBothNoParam = @(p)constraintsBoth(p, goal , region_list);

%Without derivatives
 options = optimoptions(@fmincon,'Algorithm',algorithm, 'FiniteDifferenceType','forward', 'CheckGradients', false, 'SpecifyConstraintGradient', false, 'SpecifyObjectiveGradient', false, 'Display','iter','PlotFcns',@optimplotfunccount);
% 
 tic
 [p_opt, fval, exitflag, output] = fmincon(@objectiveBoth, init, A, b, Aeq, beq, lb, ub, constraintsBothNoParam, options);
 tn = toc
 % hold on % this doesnt work for @optimplot, workaround follows
h1 = gcf;
h2 = figure(2);
axObjs = get(h1, 'children');
dataObjs = findobj(h1,'-property','YData');
xdata = dataObjs(1).XData;
ydata = dataObjs(1).YData;

 %with deriatives
options = optimoptions(@fmincon,'Algorithm',algorithm, 'FiniteDifferenceType','forward', 'CheckGradients', false, 'SpecifyConstraintGradient', use_derivs, 'SpecifyObjectiveGradient', use_derivs, 'Display','iter','PlotFcns',@optimplotfunccount, 'OutputFcn', @optimplotfunccount);

tic
[p_opt, fval, exitflag, output] = fmincon(@objectiveBoth, init, A, b, Aeq, beq, lb, ub, constraintsBothNoParam, options);
td = toc

 
h3 = gcf;
h4 = figure(4);
dataObjs1 = findobj(h3,'-property','YData');
xdata1 = dataObjs1(1).XData;
ydata1 = dataObjs1(1).YData;

clf
plot(xdata, ydata, '^');
hold on
plot(xdata1, ydata1, 'o');
axis([0 28 0 14]);
xlabel('Iteration')
ylabel('Function Evaluations')
legend(['Finite Differencing', num2str(tn)], ['Analytical Gradients', num2str(td)])
title('Function Evaluations Per Iteration')
 
if plot_path
    figure
    nr = size(region_list, 1);
    alpha = p_opt(1:2*nr);
    L = p_opt(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
    s0 = p_opt(4*nr+1:5*nr);
    sF = p_opt(5*nr+1:6*nr);
    plot_sequence(alpha, L, s0, sF, goal)
end

function A = Jacobian(p)
nr = 1;

A = zeros(6*nr, 4);
A(:,1) = dxbydp(p, nr);
A(:,2) = dybydp(p, nr); %start with this one because (3,2) is what fmin is complaining about
A(:,3) = dpsibydp(p, nr);
A(:,4) = dkappabydp(p, nr);
end

function gc = InequalityGradient(p, region_list)
nr = 1;
gc = zeros(6*nr, 8*nr);
% eight constriants per region, need to differentiate with respect to each
% parameter. Four constraints are based on y and four on x
 for i = 1:nr
     gc(:, i) = -dxbydp(p, nr);
     gc(:, i+1) = dxbydp(p, nr);
     gc(:, i+2) = -dybydp(p, nr);
     gc(:, i+3) = dybydp(p, nr);
     % These ones are all currently zero as the first segment starts at the
     % origin. Fo mlti regions the parameters of the previous segment will
     % be needed somehow
%      gc(:, i+4) = -dxbydp(p, nr);
%      gc(:, i+5) = dxbydp(p, nr);
%      gc(:, i+6) = -dybydp(p, nr);
%      gc(:, i+7) = dybydp(p, nr);
 end
%constraint_stack = zeros(1,8*nr);
% for i = 1:nr
%     [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
%     first_pose = compose(start_pose,[s0(i),0,0]);
%     mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
%     end_pose = compose( mid_pose, [sF(i),0,0]);
%     
%     region = region_list(i,:);
%     xmin = region(1);
%     xmax = region(2);
%     ymin = region(3);
%     ymax = region(4);
%     constraint_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-start_pose(1), start_pose(1)-xmax, ymin-start_pose(2), start_pose(2)-ymax];
%     
%     start_pose = end_pose;
% end
% c = constraint_stack;
end

function grad_arr = dxbydp(p, nr)
% dxbydp = [dxbyda1, dxbyda2, dxbydL1, dxbydL2, dxbyds0, dxbydsF]'
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(5*nr);
sF = p(6*nr);
psi_f = 0.5*alpha(1)*L(1)^2 + alpha(1)*L(1)*L(2) + 0.5*alpha(2)*L(2)^2;

grad_arr = zeros(6*nr,1);
for i = 1:nr
    grad_arr(i) = dxbyda1(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbyda1;
    grad_arr(i+nr) = dxbyda2(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbyda2
    grad_arr(i+2*nr) = dxbydL1(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbydL1
    grad_arr(i+3*nr) = dxbydL2(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dxbydL2
    grad_arr(i+4*nr) = 1; %dxbyds0
    grad_arr(i+5*nr) = cos(psi_f); %dxbydsF
end

end
function grad_arr = dybydp(p, nr)
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(5*nr);
sF = p(6*nr);

grad_arr = zeros(6,1);
grad_arr(1) = dybyda1(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybyda1;
grad_arr(2) = dybyda2(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybyda2
grad_arr(3) = dybydL1(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybydL1
grad_arr(4) = dybydL2(alpha(1), alpha(2), L(1), L(2), sF(1)); %dybydL2
grad_arr(5) = 0; %dybyds0 % Equation(50)
grad_arr(6) = dybydsF(alpha(1), alpha(2), L(1), L(2)); %dybydsF

end
function grad_arr = dpsibydp(p, nr)
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(5*nr);
sF = p(6*nr);

grad_arr = zeros(6,1);
grad_arr(1) = dpsibyda1(alpha(1), alpha(2), L(1), L(2)); %dpsibyda1;
grad_arr(2) = dpsibyda2(alpha(1), alpha(2), L(1), L(2)); %dpsibyda2
grad_arr(3) = dpsibydL1(alpha(1), alpha(2), L(1), L(2)); %dpsibydL1
grad_arr(4) = dpsibydL2(alpha(1), alpha(2), L(1), L(2)); %dpsibydL2
grad_arr(5) = 0; %dpsibyds0 % Equation (56)
grad_arr(6) = 0; %dpsibydsF % Equation (56) also

end
function grad_arr = dkappabydp(p, nr)
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(5*nr);
sF = p(6*nr);
grad_arr = zeros(6,1);
grad_arr(1) = L(1); %dkbyda1;
grad_arr(2) = L(2); %dkbyda2
grad_arr(3) = alpha(1); %dkbydL1
grad_arr(4) = alpha(2); %dkbydL2
grad_arr(5) = 0; %dkbyds0 % Equation (57)
grad_arr(6) = 0; %dkbydsF % Equation (57) also
end

function grad = dxbyda1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;

fun1 = @(u)-0.5*u.^2.*sin(0.5*alpha_1*u.^2);
fun2 = @(u)-L_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun3 = @(u)L_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);
%Equation - now (28)
grad = integral(fun1, 0, L_1) + cos(delta_1)*integral(fun2,0,L_2) ...
+ (-sin(delta_1)*0.5*L_1^2)*C(alpha_2, L_2, k_m) - sin(delta_1)*integral(fun3,0,L_2) ...
- (cos(delta_1)*0.5*L_1^2)*S(alpha_2, L_2, k_m) + sF*(L_1*L_2 +0.5*L_1^2)*(-sin(psi_f));
end
function grad = dybyda1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;

fun1 = @(u)0.5*u.^2.*cos(alpha_1*u.^2/2);
fun2 = @(u)-L_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun3 = @(u)L_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);
%Equation (45)
grad =  integral(fun1, 0, L_1) + sin(delta_1)*integral(fun2, 0, L_2) ...
     + cos(delta_1)*(0.5*L_1^2)*C(alpha_2, L_2, k_m) + cos(delta_1)*integral(fun3, 0, L_2)...
     + (-sin(delta_1))*(0.5*L_1^2)*S(alpha_2, L_2, k_m) + sF*(L_1*L_2 + 0.5*L_1^2)*cos(psi_f);
 
end
function grad = dxbyda2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;
fun1 = @(u)-0.5*u.^2.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun2 = @(u)0.5*u.^2.*cos(k_m*u + 0.5*alpha_2*u.^2);
% Equation (39)
grad = cos(delta_1)*integral(fun1, 0, L_2) - sin(delta_1)*integral(fun2, 0, L_2) + sF*(-sin(psi_f))*(0.5*L_2^2); 
end

function grad = dybyda2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;
fun1 = @(u)-0.5*u.^2.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun2 = @(u)0.5*u.^2.*cos(k_m*u + 0.5*alpha_2*u.^2);
% Equation (47)
grad = sin(delta_1)*integral(fun1, 0, L_2) + cos(delta_1)*integral(fun2, 0, L_2) + sF*cos(psi_f)*0.5*L_2^2;
end

function grad = dxbydL1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;

fun2 = @(u)-alpha_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun3 = @(u)alpha_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);

% Equation-(37)
grad = cos(0.5*alpha_1*L_1^2) + cos(delta_1)*integral(fun2, 0, L_2) ...
    + (-sin(delta_1)*alpha_1*L_1)*C(alpha_2, L_2, k_m) - sin(delta_1)*integral(fun3, 0, L_2) ...
- (cos(delta_1)*alpha_1*L_1)*S(alpha_2, L_2, k_m) + sF*(-sin(psi_f))*(alpha_1*L_1 + alpha_1*L_2);
end
function grad = dybydL1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;

fun2 = @(u)-alpha_1*u.*sin(alpha_1*L_1*u + 0.5*alpha_2*u.^2);
fun3 = @(u)alpha_1*u.*cos(alpha_1*L_1*u + 0.5*alpha_2*u.^2);

% Equation(38)
grad = sin(0.5*alpha_1*L_1^2) + sin(delta_1)*integral(fun2, 0, L_2) ...
    + cos(delta_1)*alpha_1*L_1*C(alpha_2, L_2, k_m) + cos(delta_1)*integral(fun3, 0, L_2) ...
    - sin(delta_1)*alpha_1*L_1*S(alpha_2, L_2, k_m) + sF*cos(psi_f)*(alpha_1*L_1 + alpha_1*L_2);
end
function grad = dxbydL2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
%Equation (36)
grad = cos(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*cos(delta_1) - sin(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*sin(delta_1)+ sF*(-sin(psi_f))*(alpha_1*L_1 + alpha_2*L_2);
end
function grad = dybydL2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
%Equation (49)
grad = cos(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*sin(delta_1) + sin(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*cos(delta_1) + sF*(cos(psi_f))*(alpha_1*L_1 + alpha_2*L_2);
end
function xf = C(alpha, L, k_m)
funx = @(u)cos(k_m.*u + alpha*u.*u/2);
xf = integral(funx, 0, L); 
end
function yf = S(alpha, L, k_m)
funy = @(u)sin(k_m.*u + alpha*u.*u/2);
yf = integral(funy, 0, L);
end



function grad = dybydsF(alpha_1, alpha_2, L_1, L_2)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
% Equation(51)
grad = sin(psi_f);
end

function grad = dpsibyda1(alpha_1, alpha_2, L_1, L_2)
grad = 0.5*L_1^2 + L_1*L_2;
end
function grad = dpsibyda2(alpha_1, alpha_2, L_1, L_2)
grad = 0.5*L_2^2;
end
function grad = dpsibydL1(alpha_1, alpha_2, L_1, L_2)
grad = alpha_1*L_1 + alpha_1*L_2; % equation(55)
end
function grad = dpsibydL2(alpha_1, alpha_2, L_1, L_2)
grad = alpha_1*L_1 + alpha_2*L_2; % equation(56)
end

function [f,g] = objectiveBoth(p)
    f = p*p';
    if nargout > 1
        g = 2*p;
    end
end

function [c,ceq,gc,gceq] = constraintsBoth(p, goal, region_list)
    c = [];
    ceq = [];
    [c, ceq] = constraints(p, goal, region_list); 
    %c=[];%turn off the inequality constraints until I figure them out
    if nargout > 2
        gc = InequalityGradient(p);% likewise turned off
        gceq = Jacobian(p);
    end
end


function [c, ceq] = constraints(p, goal, region_list)
nr = size(region_list, 1);

alpha = p(1:2*nr);
%delta = p(2*nr+1:4*nr);
L = p(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
s0 = p(4*nr+1:5*nr);
sF = p(5*nr+1:6*nr);

start_pose = [0,0,0]';
k = 0;
curvaturem_arr = zeros(1,nr);
curvaturem_ref = zeros(1,nr); 
constraint_stack = zeros(1,8*nr);
for i = 1:nr
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose(start_pose,[s0(i),0,0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF(i),0,0]);
    curvaturem_arr(i) = curvaturem;
    
    region = region_list(i,:);
    xmin = region(1);
    xmax = region(2);
    ymin = region(3);
    ymax = region(4);
    constraint_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-start_pose(1), start_pose(1)-xmax, ymin-start_pose(2), start_pose(2)-ymax];
    
    start_pose = end_pose;
end
c = constraint_stack;

ceq = [end_pose'-goal(1:3), curvaturem_arr - curvaturem_ref];


end
%