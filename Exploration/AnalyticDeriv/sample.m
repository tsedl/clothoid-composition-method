%copypasta from regionConstraints2Segmentsalphadelta.m  -its essential
%
function [xf, yf, headingf, curvaturef] = sample(alpha, L, k_m)
funx = @(u)cos(k_m.*u + alpha*u.*u/2);
xf = integral(funx, 0, L); 
funy = @(u)sin(k_m.*u + alpha*u.*u/2);
yf = integral(funy, 0, L);
deflectionf = k_m*L + alpha*L*L/2;
headingf = wrapToPi(deflectionf); % heading between +pi and -pi - should make no difference
curvaturef = k_m + alpha*L;
end


