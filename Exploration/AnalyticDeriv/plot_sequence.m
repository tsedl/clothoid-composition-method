%
function plot_sequence(alpha, L, s0, sF, start, goal, circles)
if nargin<7
    circles = false;
end
init_pose = start;

X = init_pose(1);
Y = init_pose(2);
Heading = init_pose(3);
Curvature = 0;
S = 0;
end_pose = init_pose';
end_pose_alt = init_pose';
nr = length(alpha)/2;
subplot(2,1,1);
hold on
%just to setup the colours for the legend
subplot(2,1,2);
plot([0,0], [0,0], '-r'); %first curvature
hold on
plot([0,0], [0,0], '-g'); %then sharpness
hold on
for i = 1:nr
    %%from constraints
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose(end_pose_alt,[s0(i),0,0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose_alt = compose( mid_pose, [sF(i),0,0]);
    
    %%
    [Xi, Yi, Headingi, Curvaturei, Si] = PlotClothoidSequence(alpha(2*i-1:2*i), L(2*i-1:2*i));
    [X, Y, Heading, Curvature, S] = join( [X, Y, Heading, Curvature, S], [s0(i),0,0,0,s0(i)]);
    [X, Y, Heading, Curvature, S] = join([X, Y, Heading, Curvature, S], [Xi, Yi, Headingi, Curvaturei, Si]);
    [X, Y, Heading, Curvature, S] = join( [X, Y, Heading, Curvature, S], [0,sF(i),0,0,0,0,0,0,0,sF(i)]);
    
    %%
    [xm, ym, headingm, curvaturem] = sample(alpha(2*i-1), L(2*i-1),0);
    [xmm, ymm, headingmm, curvaturemm] = sample(alpha(2*i), L(2*i), curvaturem);
    first_pose = compose( end_pose, [s0(i),0,0]);
    mid1_pose = compose( first_pose, [xm, ym, headingm]);
    mid2_pose = compose( mid1_pose, [xmm, ymm, headingmm]);
    end_pose = compose( mid2_pose, [sF(i),0,0]);
    
    if(i==1)
        cumS = 0;
    else
        cumS = sum(L(1:2*i-2))+sum(s0(1:i-1))+sum(sF(1:i-1));
    end

    if circles
        subplot(2,1,1);
        plot([first_pose(1), mid1_pose(1), mid2_pose(1),end_pose_alt(1)], [first_pose(2), mid1_pose(2),mid2_pose(2), end_pose_alt(2)], 'ob');
        subplot(2,1,2);
        plot([cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0, curvaturem, curvaturemm, 0], 'og');
        plot([cumS, cumS + s0(i),cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i),cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0,0, alpha(2*i-1), alpha(2*i-1), alpha(2*i), alpha(2*i), 0 ,0], 'or');
    end
    subplot(2,1,2);
    plot([cumS, cumS + s0(i),cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i),cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0,0, alpha(2*i-1), alpha(2*i-1), alpha(2*i), alpha(2*i), 0 ,0], '-r');

end


subplot(2,1,1);
plot(X, Y)
hold on
plot(goal(1), goal(2), 'cx');
arrow_length = 0.4;
plot([goal(1), goal(1) + arrow_length *cos(goal(3))], [goal(2), goal(2) + arrow_length *sin(goal(3))], 'c-');
xlabel('x[m]');
ylabel('y[m]')
title(['X_G =[' num2str(goal) ']']);
axis equal

subplot(2,1,2);
plot(S, Curvature, '-g')
xlabel('s[m]')
legend('Sharpness [rad.m^{-2}]', 'Curvature [rad.m^{-1}]')
 
end