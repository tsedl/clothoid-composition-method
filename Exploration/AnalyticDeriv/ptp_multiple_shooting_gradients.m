options = optimoptions(@fmincon,'Algorithm','interior-point', 'FiniteDifferenceType','forward', 'CheckGradients', true, 'SpecifyConstraintGradient', true, 'SpecifyObjectiveGradient', true, 'Display','iter','PlotFcns',@optimplotfval);
%[x fval exitflag output] = fmincon(@rosenboth,...
 %  [-1;2],[],[],[],[],[],[],@unitdiskb,options);

 %init = [1,1];
 %regions are axis aligned: [xmin, xmax, ymin, ymax]
region1 = [0.0, 5.0, -5.0, 5.0];
region2 = [0.0, 11.0, 2.5, 5.0];
region3 = [6.0, 11.0, -5.0, 5.0];
region_list = [region1; region2];%; region3];
nr = size(region_list, 1);

start = [0,0,0];
% 3 region goal
%goal = [11,0,0*pi/180];
% 2 region goal
 goal = [11,3,0*pi/180];
% 1 region goal
%goal = [4,3,60*pi/180];
corners = plot_region_list(region_list);
 % changed it so there are 9 parameters per region - the last three are x0, y0, psi0 
%init = [1, -1,1, -1, 1, 1,1, 1, 0, 0, 0, 0, region1(1), region2(1),region1(3),region2(3), 0, 0];
init_x0 = zeros(1,nr);
init_y0 = zeros(1,nr);
init_psi0 = zeros(1,nr);
alpha_plus = ones(1,nr);
alpha_minus = -ones(1, nr);
init_alpha = ones(1,2*nr);
% init_alpha(1:2:end-1) = alpha_plus;
% init_alpha(2:2:end) = alpha_minus;
init_L = ones(1,2*nr);
for i =1:nr
    %init_x0(i) = 0.5*(region_list(i, 1) + region_list(i, 2));
    %init_y0(i) = 0.5*(region_list(i, 3) + region_list(i, 4));
     init_x0(i) = region_list(i, 1);
     init_y0(i) = region_list(i, 3);
    if(i>1)
        % if the psi values don't match the sharpness they throw off
        % convergence! Better to leave them at zero in that case
        %init_psi0(i) = init_psi0(i-1) + 0.5*init_alpha(2*i-3)*init_L(2*i-3)^2 + init_alpha(2*i-3)*init_L(2*i-3)*init_L(2*i-2) + 0.5*init_alpha(2*i-2)*init_L(2*i-2)^2;
    end
end
init = [init_alpha, init_L, ones(1,nr),ones(1,nr), init_x0, init_y0, init_psi0];
% try the same init vector as used for single shooting
%init = [1, 1, -1, -1, 1, 1, ones(1,nr), ones(1,nr), ones(1,nr), ones(1,nr), init_x0, init_y0, init_psi0];


%Plotting for sanity check
alpha = init(1:2*nr);
%delta = p_opt(2*nr+1:4*nr);
L = init(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
s0 = init(4*nr+1:5*nr);
sF = init(5*nr+1:6*nr);
x0 = init(6*nr+1:7*nr);
y0 = init(7*nr+1:8*nr);
psi0 = init(8*nr+1:9*nr);
figure
subplot(2,1,1)
hold on
plot_region_list(region_list);
plot_sequence(alpha, L, s0, sF, x0, y0, psi0, goal);
hold off

A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf*ones(1,2*nr), zeros(1,2*nr), zeros(1,nr), zeros(1,nr), -inf*ones(1,3*nr)];


constraintsBothNoParam = @(p)constraintsBoth(p, start, goal, region_list);
tic
%[p_opt, fval, exitflag, output] = fmincon(@rosenbrockBoth, init, A, b, Aeq, beq, lb, ub, @unitdisk2, options);
[p_opt, fval, exitflag, output] = fmincon(@objectiveBoth, init, A, b, Aeq, beq, lb, ub, constraintsBothNoParam, options)
toc

%Without derivatives
%  options = optimoptions(@fmincon,'Algorithm','interior-point', 'FiniteDifferenceType','forward', 'CheckGradients', false, 'SpecifyConstraintGradient', false, 'SpecifyObjectiveGradient', false, 'Display','iter','PlotFcns',@optimplotfval);
% % 
%  tic
%  [p_opt, fval, exitflag, output] = fmincon(@objectiveBoth, init, A, b, Aeq, beq, lb, ub, @constraintsBoth, options)
%  toc
%Plotting for sanity check
alpha = p_opt(1:2*nr);
%delta = p_opt(2*nr+1:4*nr);
L = p_opt(2*nr+1:4*nr);%sqrt(abs(2*delta./alpha));
s0 = p_opt(4*nr+1:5*nr);
sF = p_opt(5*nr+1:6*nr);
x0 = p_opt(6*nr+1:7*nr);
y0 = p_opt(7*nr+1:8*nr);
psi0 = p_opt(8*nr+1:9*nr);
opt_output = output;
total_length = sum(p_opt(2*nr+1:6*nr))
figure
subplot(2,1,1)
hold on
plot_region_list(region_list);
plot_sequence(alpha, L, s0, sF, x0, y0, psi0, goal);
hold off
%check constraints are met
[c_opt, c_eq_opt] = constraintsBoth(p_opt, start, goal, region_list)

function A = Jacobian(p, region_list)
nr = size(region_list, 1);

A = zeros(9*nr, 3 + 4*nr);
A(:,1) = dxbydp(p, nr, 0); % I think this should be a different function, as it concerns the start only
A(:,2) = dybydp(p, nr, 0); 
A(:,3) = dpsibydp(p, nr, 0);
for i = 1:nr-1
    A(:,3+i) = dxbydp(p, nr, i) - dx0bydp(p, nr, i+1);
    A(:,3+nr+i) = dybydp(p, nr, i)- dy0bydp(p, nr, i+1); 
    A(:,3+2*nr+i) = dpsibydp(p, nr, i)- dpsi0bydp(p, nr, i+1);
    A(:,3+3*nr+i) = dkappabydp(p, nr, i);
end
i=nr;
A(:,3+i) = dxbydp(p, nr, i);
A(:,3+nr+i) = dybydp(p, nr, i); 
A(:,3+2*nr+i) = dpsibydp(p, nr, i);
A(:,3+3*nr+i) = dkappabydp(p, nr, i);

end

function gc = InequalityGradient(p, region_list)
nr = size(region_list, 1);
gc = zeros(9*nr, 8*nr);
% eight constriants per region, need to differentiate with respect to each
% parameter. Four constraints are based on y and four on x
 for i = 1:nr
     gc(:, i) = -dxbydp(p, nr, i);
     gc(:, i+1) = dxbydp(p, nr, i);
     gc(:, i+2) = -dybydp(p, nr, i);
     gc(:, i+3) = dybydp(p, nr, i);
     % These ones are all currently zero as the first segment starts at the
     % origin. For multi regions the parameters of the previous segment will
     % be needed somehow
     if i>1
      gc(:, i+4) = -dxbydp(p, nr, i-1);
      gc(:, i+5) = dxbydp(p, nr, i-1);
      gc(:, i+6) = -dybydp(p, nr, i-1);
      gc(:, i+7) = dybydp(p, nr, i-1);
     end
 end
%constraint_stack = zeros(1,8*nr);
% for i = 1:nr
%     [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
%     first_pose = compose(start_pose,[s0(i),0,0]);
%     mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
%     end_pose = compose( mid_pose, [sF(i),0,0]);
%     
%     region = region_list(i,:);
%     xmin = region(1);
%     xmax = region(2);
%     ymin = region(3);
%     ymax = region(4);
%     constraint_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-start_pose(1), start_pose(1)-xmax, ymin-start_pose(2), start_pose(2)-ymax];
%     
%     start_pose = end_pose;
% end
% c = constraint_stack;
end

function grad_arr = dxbydp(p, nr, i)
% dxbydp = [dxbyda1, dxbyda2, dxbydL1, dxbydL2, dxbyds0, dxbydsF, dxbydx0, dxbydy0, dxbydpsi0]'
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(4*nr+1:5*nr);
sF = p(5*nr+1:6*nr);
x0 = p(6*nr+1:7*nr);
y0 = p(7*nr+1:8*nr);
psi0 = p(8*nr+1:9*nr);
psi_f = 0.5*alpha(1)*L(1)^2 + alpha(1)*L(1)*L(2) + 0.5*alpha(2)*L(2)^2;

grad_arr = zeros(9*nr,1);

if i>0
    grad_arr(2*i-1) = dxbyda1(alpha(2*i-1), alpha(2*i), L(2*i-1), L(2*i), sF(i)); %dxbyda1;
    grad_arr(2*i) = dxbyda2(alpha(2*i-1), alpha(2*i), L(2*i-1), L(2*i), sF(i)); %dxbyda2
    grad_arr(2*nr+2*i-1) = dxbydL1(alpha(2*i-1), alpha(2*i), L(2*i-1), L(2*i), sF(i)); %dxbydL1
    grad_arr(2*nr+2*i) = dxbydL2(alpha(2*i-1), alpha(2*i), L(2*i-1), L(2*i), sF(i)); %dxbydL2
    grad_arr(4*nr+i) = 1; %dxbyds0
    grad_arr(5*nr+i) = cos(psi_f); %dxbydsF
    grad_arr(6*nr+i) = 1; %dxbydx0
    grad_arr(7*nr+i) = 0; %dxbydy0
    grad_arr(8*nr+i) = dxbydpsi0(alpha(2*i-1), alpha(2*i), L(2*i-1), L(2*i), s0(i), sF(i), psi0(i)); %dxbydpsi0
else
    grad_arr(6*nr+1) = 1; % dxbydx0 only used for starting position constraint
end

end
function grad_arr = dx0bydp(p, nr, i)
    % dx0bydp = [dxbyda1, dxbyda2, dxbydL1, dxbydL2, dxbyds0, dxbydsF, dxbydx0, dxbydy0, dxbydpsi0]'
grad_arr = zeros(9*nr,1);
grad_arr(6*nr+i) = 1; % dxbydx0 only used for starting position constraint
end

function grad_arr = dybydp(p, nr, i)
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(4*nr+1:5*nr);
sF = p(5*nr+1:6*nr);
x0 = p(6*nr+1:7*nr);
y0 = p(7*nr+1:8*nr);
psi0 = p(8*nr+1:9*nr);

grad_arr = zeros(9*nr,1);

if i > 0
    grad_arr(2*i-1) = dybyda1(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dybyda1;
    grad_arr(2*i) = dybyda2(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dybyda2
    grad_arr(2*nr+2*i-1) = dybydL1(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dybydL1
    grad_arr(2*nr+2*i) = dybydL2(alpha(i), alpha(i+1), L(i), L(i+1), sF(i)); %dybydL2
    grad_arr(i+4*nr) = 0; %dybyds0 % Equation(50)
    grad_arr(i+5*nr) = dybydsF(alpha(i), alpha(i+1), L(i), L(i+1)); %dybydsF
    grad_arr(i+6*nr) = 0; %dybydx0
    grad_arr(i+7*nr) = 1; %dybydy0
    grad_arr(i+8*nr) = dybydpsi0(alpha(2*i-1), alpha(2*i), L(2*i-1), L(2*i), s0(i), sF(i), psi0(i)); %dybydpsi0
else
    grad_arr(1+7*nr) = 1; % dybydy0 only used for starting position constraint
end


end
function grad_arr = dy0bydp(p, nr, i)
    grad_arr = zeros(9*nr,1);
    grad_arr(i+7*nr) = 1; % dybydy0 only used for starting position constraint
end

function grad_arr = dpsibydp(p, nr, i)
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(4*nr+1:5*nr);
sF = p(5*nr+1:6*nr);
x0 = p(6*nr+1:7*nr);
y0 = p(7*nr+1:8*nr);
psi0 = p(8*nr+1:9*nr);

grad_arr = zeros(9*nr,1);
if i >0   
    grad_arr(2*i-1) = dpsibyda1(alpha(i), alpha(i+1), L(i), L(i+1)); %dpsibyda1;
    grad_arr(2*i) = dpsibyda2(alpha(i), alpha(i+1), L(i), L(i+1)); %dpsibyda2
    grad_arr(2*nr+2*i-1) = dpsibydL1(alpha(i), alpha(i+1), L(i), L(i+1)); %dpsibydL1
    grad_arr(2*nr+2*i) = dpsibydL2(alpha(i), alpha(i+1), L(i), L(i+1)); %dpsibydL2
    grad_arr(i+4*nr) = 0; %dpsibyds0 % Equation (56)
    grad_arr(i+5*nr) = 0; %dpsibydsF % Equation (56) also
    grad_arr(i+6*nr) = 0; %dpsibydx0
    grad_arr(i+7*nr) = 0; %dpsibydy0
    grad_arr(i+8*nr) = 1; %dpsibydpsi0
else
    grad_arr(1+8*nr) = 1; %dpsibydpsi0
end

end
function grad_arr = dpsi0bydp(p, nr, i)
    grad_arr = zeros(9*nr,1);
    grad_arr(i+8*nr) = 1; %dpsibydpsi0
end
function grad_arr = dkappabydp(p, nr, i)
alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(4*nr+1:5*nr);
sF = p(5*nr+1:6*nr);
x0 = p(6*nr+1:7*nr);
y0 = p(7*nr+1:8*nr);
psi0 = p(8*nr+1:9*nr);

grad_arr = zeros(9*nr,1);
grad_arr(2*i-1) = L(1); %dkbyda1;
grad_arr(2*i) = L(2); %dkbyda2
grad_arr(2*nr+2*i-1) = alpha(1); %dkbydL1
grad_arr(2*nr+2*i) = alpha(2); %dkbydL2
grad_arr(i+4*nr) = 0; %dkbyds0 % Equation (57)
grad_arr(i+5*nr) = 0; %dkbydsF % Equation (57) also
grad_arr(i+6*nr) = 0; %dkbydx0
grad_arr(i+7*nr) = 0; %dkbydy0 % Equation (57)
grad_arr(i+8*nr) = 0; %dkbydpsi0 % Equation (57) also
end

function grad = dxbyda1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;

fun1 = @(u)-0.5*u.^2.*sin(0.5*alpha_1*u.^2);
fun2 = @(u)-L_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun3 = @(u)L_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);
%Equation - now (28)
grad = integral(fun1, 0, L_1) + cos(delta_1)*integral(fun2,0,L_2) ...
+ (-sin(delta_1)*0.5*L_1^2)*C(alpha_2, L_2, k_m) - sin(delta_1)*integral(fun3,0,L_2) ...
- (cos(delta_1)*0.5*L_1^2)*S(alpha_2, L_2, k_m) + sF*(L_1*L_2 +0.5*L_1^2)*(-sin(psi_f));
end
function grad = dybyda1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;

fun1 = @(u)0.5*u.^2.*cos(alpha_1*u.^2/2);
fun2 = @(u)-L_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun3 = @(u)L_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);
%Equation (45)
grad =  integral(fun1, 0, L_1) + sin(delta_1)*integral(fun2, 0, L_2) ...
     + cos(delta_1)*(0.5*L_1^2)*C(alpha_2, L_2, k_m) + cos(delta_1)*integral(fun3, 0, L_2)...
     + (-sin(delta_1))*(0.5*L_1^2)*S(alpha_2, L_2, k_m) + sF*(L_1*L_2 + 0.5*L_1^2)*cos(psi_f);
 
end
function grad = dxbyda2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;
fun1 = @(u)-0.5*u.^2.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun2 = @(u)0.5*u.^2.*cos(k_m*u + 0.5*alpha_2*u.^2);
% Equation (39)
grad = cos(delta_1)*integral(fun1, 0, L_2) - sin(delta_1)*integral(fun2, 0, L_2) + sF*(-sin(psi_f))*(0.5*L_2^2); 
end

function grad = dybyda2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;
fun1 = @(u)-0.5*u.^2.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun2 = @(u)0.5*u.^2.*cos(k_m*u + 0.5*alpha_2*u.^2);
% Equation (47)
grad = sin(delta_1)*integral(fun1, 0, L_2) + cos(delta_1)*integral(fun2, 0, L_2) + sF*cos(psi_f)*0.5*L_2^2;
end

function grad = dxbydL1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2;

fun2 = @(u)-alpha_1*u.*sin(k_m*u + 0.5*alpha_2*u.^2);
fun3 = @(u)alpha_1*u.*cos(k_m*u + 0.5*alpha_2*u.^2);

% Equation-(37)
grad = cos(0.5*alpha_1*L_1^2) + cos(delta_1)*integral(fun2, 0, L_2) ...
    + (-sin(delta_1)*alpha_1*L_1)*C(alpha_2, L_2, k_m) - sin(delta_1)*integral(fun3, 0, L_2) ...
- (cos(delta_1)*alpha_1*L_1)*S(alpha_2, L_2, k_m) + sF*(-sin(psi_f))*(alpha_1*L_1 + alpha_1*L_2);
end
function grad = dybydL1(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;

fun2 = @(u)-alpha_1*u.*sin(alpha_1*L_1*u + 0.5*alpha_2*u.^2);
fun3 = @(u)alpha_1*u.*cos(alpha_1*L_1*u + 0.5*alpha_2*u.^2);

% Equation(38)
grad = sin(0.5*alpha_1*L_1^2) + sin(delta_1)*integral(fun2, 0, L_2) ...
    + cos(delta_1)*alpha_1*L_1*C(alpha_2, L_2, k_m) + cos(delta_1)*integral(fun3, 0, L_2) ...
    - sin(delta_1)*alpha_1*L_1*S(alpha_2, L_2, k_m) + sF*cos(psi_f)*(alpha_1*L_1 + alpha_1*L_2);
end
function grad = dxbydL2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
%Equation (36)
grad = cos(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*cos(delta_1) - sin(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*sin(delta_1)+ sF*(-sin(psi_f))*(alpha_1*L_1 + alpha_2*L_2);
end
function grad = dybydL2(alpha_1, alpha_2, L_1, L_2, sF)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
%Equation (49)
grad = cos(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*sin(delta_1) + sin(alpha_1*L_1*L_2 + 0.5*alpha_2*L_2^2)*cos(delta_1) + sF*(cos(psi_f))*(alpha_1*L_1 + alpha_2*L_2);
end
function grad = dxbydpsi0(alpha_1, alpha_2, L_1, L_2, s0, sF, psi0)
    [xm, ym, headingm, curvaturem] = sample_n([alpha_1, alpha_2], [L_1, L_2]);
    first_pose = compose([0,0,0], [s0, 0, 0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF,0,0]);
    grad = end_pose(1)*(-sin(psi0)) + end_pose(2)*(-cos(psi0));
end
function grad = dybydpsi0(alpha_1, alpha_2, L_1, L_2, s0, sF, psi0)
    [xm, ym, headingm, curvaturem] = sample_n([alpha_1, alpha_2], [L_1, L_2]);
    first_pose = compose([0,0,0], [s0, 0, 0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF,0,0]);
    grad = end_pose(1)*(cos(psi0)) + end_pose(2)*(-sin(psi0));
end
function xf = C(alpha, L, k_m)
funx = @(u)cos(k_m.*u + alpha*u.*u/2);
xf = integral(funx, 0, L); 
end
function yf = S(alpha, L, k_m)
funy = @(u)sin(k_m.*u + alpha*u.*u/2);
yf = integral(funy, 0, L);
end



function grad = dybydsF(alpha_1, alpha_2, L_1, L_2)
k_m = alpha_1*L_1;
delta_1 = 0.5*alpha_1*L_1^2; % k_0 is always zero
psi_f = delta_1 + k_m*L_2 + 0.5*alpha_2*L_2^2 ;
% Equation(51)
grad = sin(psi_f);
end

function grad = dpsibyda1(alpha_1, alpha_2, L_1, L_2)
grad = 0.5*L_1^2 + L_1*L_2;
end
function grad = dpsibyda2(alpha_1, alpha_2, L_1, L_2)
grad = 0.5*L_2^2;
end
function grad = dpsibydL1(alpha_1, alpha_2, L_1, L_2)
grad = alpha_1*L_1 + alpha_1*L_2; % equation(55)
end
function grad = dpsibydL2(alpha_1, alpha_2, L_1, L_2)
grad = alpha_1*L_1 + alpha_2*L_2; % equation(56)
end

function [f,g] = objectiveBoth(p)
    f = p*p';
    if nargout > 1
        g = 2*p;
    end
end

function [c,ceq,gc,gceq] = constraintsBoth(p, start, goal, region_list)
    c = [];
    ceq = [];
    
    [c, ceq] = constraints(p, start, goal, region_list);%[region1;region2]); 
    c=[];%turn off the inequality constraints until I figure them out
    if nargout > 2
        gc = [];%InequalityGradient(p, region_list);% likewise turned off
        gceq = Jacobian(p, region_list);
    end
end

%copypasta from regionConstraints2Segmentsalphadelta.m  -its essential
%
function [xf, yf, headingf, curvaturef] = sample(alpha, L, k_m)
funx = @(u)cos(k_m.*u + alpha*u.*u/2);
xf = integral(funx, 0, L); 
funy = @(u)sin(k_m.*u + alpha*u.*u/2);
yf = integral(funy, 0, L);
deflectionf = k_m*L + alpha*L*L/2;
headingf = wrapToPi(deflectionf); % heading between +pi and -pi - should make no difference
curvaturef = k_m + alpha*L;
end

function [xf, yf, headingf, curvaturef] = sample_n(alpha, L)
n = length(alpha);
assert(n==length(L));
k = 0;
mid_pose = [0,0,0];
for i = 1:n
    [xf, yf, headingf, curvaturef] = sample(alpha(i), L(i), k);
    mid_pose = compose(mid_pose, [xf, yf, headingf]);
    k = curvaturef;
end

xf = mid_pose(1);
yf = mid_pose(2);
headingf = mid_pose(3);
curvaturef = k;
end
function [c, ceq] = constraints(p, start, goal, region_list)
nr = size(region_list, 1);

alpha = p(1:2*nr);
L = p(2*nr+1:4*nr);
s0 = p(4*nr+1:5*nr);
sF = p(5*nr+1:6*nr);
x0 = p(6*nr+1:7*nr);
y0 = p(7*nr+1:8*nr);
psi0 = p(8*nr+1:9*nr);


k = 0;
xf_arr = zeros(1,nr);
x0_arr = zeros(1,nr);
yf_arr = zeros(1,nr);
y0_arr = zeros(1,nr);
psif_arr = zeros(1,nr);
psi0_arr = zeros(1,nr);
curvaturem_arr = zeros(1,nr);
curvaturem_ref = zeros(1,nr); 
c_ineq_stack = zeros(1,8*nr);

for i = 1:nr
    segment_pose = [x0(i),y0(i),psi0(i)]';
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose(segment_pose,[s0(i),0,0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose = compose( mid_pose, [sF(i),0,0]);
    curvaturem_arr(i) = curvaturem;
    xf_arr(i) = end_pose(1);
    yf_arr(i) = end_pose(2);
    psif_arr(i) = end_pose(3);
    x0_arr(i) = segment_pose(1);
    y0_arr(i) = segment_pose(2);
    psi0_arr(i) = segment_pose(3);
    
    region = region_list(i,:);
    xmin = region(1);
    xmax = region(2);
    ymin = region(3);
    ymax = region(4);
    c_ineq_stack(8*i-7:8*i) = [xmin-end_pose(1), end_pose(1)-xmax, ymin-end_pose(2), end_pose(2)-ymax, xmin-segment_pose(1), segment_pose(1)-xmax, ymin-segment_pose(2), segment_pose(2)-ymax];
    
    
end
c = c_ineq_stack;

ceq = [[x0(1), y0(1), psi0(1)]-start(1:3), xf_arr - [x0_arr(2:length(x0_arr)), goal(1)], yf_arr - [ y0_arr(2:length(y0_arr)),goal(2)], psif_arr - [ psi0_arr(2:length(psi0_arr)),goal(3)], curvaturem_arr - curvaturem_ref,];


end
% Adapted rom regionConstraints2Segmentsalphadelta
function plot_sequence(alpha, L, s0, sF, x0, y0, psi0, goal)


nr = length(alpha)/2;
subplot(2,1,1);
hold on
subplot(2,1,2);
hold on
for i = 1:nr
    %%from constraints
    [xm, ym, headingm, curvaturem] = sample_n(alpha(2*i-1:2*i), L(2*i-1:2*i));
    first_pose = compose([x0(i), y0(i), psi0(i)], [s0(i), 0, 0]);
    mid_pose = compose( first_pose, [xm, ym, headingm, curvaturem]);
    end_pose_alt = compose( mid_pose, [sF(i),0,0]);
    
    %%
    X = x0(i);
    Y = y0(i);
    Heading = psi0(i);
    Curvature = 0;
    Ss = 0;
    [Xi, Yi, Headingi, Curvaturei, Si] = PlotClothoidSequence(alpha(2*i-1:2*i), L(2*i-1:2*i));
    [X, Y, Heading, Curvature, Ss] = join( [X, Y, Heading, Curvature, Ss], [s0(i),0,0,0,s0(i)]);
    [X, Y, Heading, Curvature, Ss] = join([X, Y, Heading, Curvature, Ss], [Xi, Yi, Headingi, Curvaturei, Si]);
    [X, Y, Heading, Curvature, Ss] = join( [X, Y, Heading, Curvature, Ss], [0,sF(i),0,0,0,0,0,0,0,sF(i)]);
    subplot(2,1,1);
    plot(X, Y)
    hold on
    %%
    [xm, ym, headingm, curvaturem] = sample(alpha(2*i-1), L(2*i-1),0);
    [xmm, ymm, headingmm, curvaturemm] = sample(alpha(2*i), L(2*i), curvaturem);
    first_pose = compose( [x0(i), y0(i), psi0(i)], [s0(i),0,0]);
    mid1_pose = compose( first_pose, [xm, ym, headingm]);
    mid2_pose = compose( mid1_pose, [xmm, ymm, headingmm]);
    end_pose = compose( mid2_pose, [sF(i),0,0]);
    subplot(2,1,1);
    plot([first_pose(1), mid1_pose(1), mid2_pose(1),end_pose_alt(1)], [first_pose(2), mid1_pose(2),mid2_pose(2), end_pose_alt(2)], 'ob');
    subplot(2,1,2);
    if(i==1)
        cumS = 0;
    else
        cumS = sum(L(1:2*i-2))+sum(s0(1:i-1))+sum(sF(1:i-1));
    end
    plot([cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [first_pose(3), mid1_pose(3), mid2_pose(3), end_pose(3)], 'or');
    plot([cumS + s0(i), cumS + s0(i) + L(2*i-1), cumS + sum(L(2*i-1:2*i)) + s0(i), cumS + sum(L(2*i-1:2*i)) + s0(i) + sF(i)], [0, curvaturem, curvaturemm, 0], 'og');
    subplot(2,1,2);
    plot(cumS + Ss, Heading, '-r')
    hold on
    plot(cumS + Ss, Curvature, '-g')
    xlabel('s[m]')
end


subplot(2,1,1);
hold on
plot(goal(1), goal(2), 'cx');
arrow_length = 0.1;
plot([goal(1), goal(1) + arrow_length *cos(goal(3))], [goal(2), goal(2) + arrow_length *sin(goal(3))], 'c-');
xlabel('x[m]');
ylabel('y[m]')
title(['X_G =[' num2str(goal) ']']);
axis equal



legend('s-\psi [rad]', 's-\kappa [rad.m^{-1}]', 's-\psi [rad]', 's-\kappa [rad.m^{-1}]')
 
end