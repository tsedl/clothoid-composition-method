xhat = 8%10;
yhat = 6%3.62;
psihat = 60*pi/180;%60*pi/180;%pi/4;
khat = 0;
goal = [xhat, yhat, psihat, khat]

%% Solve for the parameters to reach a point with any heading
% This script does not permit any staright lines - it very rarely converges
% except on special cases like (6,6,60) and even then not unless given the 
% exact parameters as the initial guess - see below
% 
init = [1, -1, 1, 1];%[0.1, -0.01, 1, 9]; %[0.109354, -0.0222, 3.5, 10];%[1, -1, 1, 1]; 
A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf, -inf, 0, 0];

%weighting- radians per metre squared per metre
w = 1;
objective = @(p)p(1:2)*p(1:2)' + w*sum(p(3:4))^2;
constraints_np = @(p)constraints(p, goal);
options = optimoptions('fmincon','Display','iter','Algorithm','interior-point','MaxIterations', 2000, 'MaxFunctionEvaluations', 20000);
[p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);
[c_opt, ceq_opt] = constraints(p_opt, goal)
alpha = p_opt(1:2);
L = p_opt(3:4);
s0 = 0;
sF = 0;
k=0;
[x, y, heading, curvature] = sample_n(alpha, L)
opt_output = output

plot_sequence(alpha, L, s0, sF, goal, true);

function [c, ceq] = constraints(p, goal)
c = [];
alpha = p(1:2);
L = p(3:4) ;
s0 = 0;
sF = 0;
[xf, yf, headingf, curvaturef] = sample_n(alpha, L);
pre_end_pose = compose([s0,0,0],[xf, yf, headingf, curvaturef]);
end_pose = compose(pre_end_pose, [sF,0,0]);
ceq = [end_pose', curvaturef] - goal;
end