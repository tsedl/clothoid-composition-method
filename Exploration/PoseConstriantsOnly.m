xhat = 1;%8;
yhat = -1;%6;
psihat = -pi;%60*pi/180;%pi/4;
khat = 0;
goal = [xhat, yhat, psihat, khat]

%% Solve for the parameters to reach a point with any heading
init = [1, 1, 1, 1];
A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf, -inf, 0, 0];
objective = @(p)p*p';
constraints_np = @(p)constraints(p, goal);
options = optimoptions('fmincon','Display','iter','Algorithm','interior-point');
[p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);
[c_opt, ceq_opt] = constraints(p_opt, goal)
alpha = p_opt(1:2)
L = p_opt(3:4)
k=0;
[x, y, heading, curvature] = sample_pair(alpha, L)
opt_output = output

[X, Y, Heading, Curvature, S] = PlotClothoidSequence(alpha, L);
len = length(X);
final = [X(len), Y(len), Heading(len), Curvature(len)];
[xf, yf, headingf, curvaturef] = sample_pair(alpha, L)
clf
plot(X, Y)
hold on
plot(linspace(0, sum(L),length(Heading)), Heading)
plot(linspace(0, sum(L),length(Curvature)), Curvature)

xlabel('x[m] (s[m])')
ylabel('y[m]')

title(['X_G =[' num2str(goal) ']']);

[xm, ym, headingm, curvaturem] = sample(alpha(1), L(1),0);
plot([xm,xf], [ym,yf], 'ob');
%
 plot([L(1), sum(L)], [headingm, headingf], 'or');
 plot([L(1), sum(L)], [curvaturem, curvaturef], 'og');

legend('x-y [m]', 's-\psi [rad]', 's-\kappa [rad.m^{-1}]','x-y [m]', 's-\psi [rad]', 's-\kappa [rad.m^{-1}]')

function [c, ceq] = constraints(p, goal)
c = [];
alpha_pair = p(1:2);
L_pair = p(3:4);
[xf, yf, headingf, curvaturef] = sample_pair(alpha_pair, L_pair);
ceq = [xf, yf, headingf] - goal(1:3);
end

function [xf, yf, headingf, curvaturef] = sample(alpha, L, k_m)
funx = @(u)cos(k_m.*u + alpha*u.*u/2);
xf = integral(funx, 0, L); 
funy = @(u)sin(k_m.*u + alpha*u.*u/2);
yf = integral(funy, 0, L);
deflectionf = k_m*L + alpha*L*L/2;
headingf = wrapToPi(deflectionf); % heading between +pi and -pi - should make no difference
curvaturef = k_m + alpha*L;
end

function [xf, yf, headingf, curvaturef] = sample_pair(alpha_pair, L_pair)
[x1f, y1f, heading1f, curvature1f] = sample(alpha_pair(1), L_pair(1), 0);
[x2f, y2f, heading2f, curvature2f] = sample(alpha_pair(2), L_pair(2), curvature1f);
final_pose = compose([x1f, y1f, heading1f], [x2f, y2f, heading2f]);
final_posture = [final_pose', curvature2f];
xf = final_posture(1);
yf = final_posture(2);
headingf = final_posture(3);
curvaturef = final_posture(4);
end