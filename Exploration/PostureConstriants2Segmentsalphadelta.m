%% Solve for the parameters to reach a point with any heading
% I wrote a paper about how I could do this with a straight line at either
% end. In this simplified script 
% I do not handle theta < phi with a four clothoid special case
% how is deflection a sensible parameter for optimization? 
init = [1, -1, 1, -1, 1, 1];
A = [];
b = [];
Aeq = [];
beq=[];
ub =[];
lb = [-inf, -inf, -pi, -pi, 0, 0];
%ub = [inf, 0, pi, 0, inf, inf];
objective = @(p)p(1:2)*p(1:2)' + (p(3:4)./p(1:2))*(p(3:4)./p(1:2))' + p(5:6)*p(5:6)';
constraints_np = @(p)constraints(p, goal);
options = optimoptions('fmincon','Display','iter','Algorithm','interior-point');
tic
[p_opt, fval, exitflag, output] = fmincon(objective, init, A, b, Aeq, beq, lb, ub, constraints_np, options);
toc
[c_opt, ceq_opt] = constraints(p_opt, goal)
alpha = p_opt(1:2);
delta = p_opt(3:4);
L = sqrt(abs(2*delta./alpha));
s0 = p_opt(5);
sF = p_opt(6);
k=0;
[x, y, heading, curvature] = sample_n(alpha, L)
opt_output = output

%plot_sequence(alpha, L, s0, sF, goal, true);


function [c, ceq] = constraints(p, goal)
c = [];
alpha = p(1:2);
delta = p(3:4);
L = sqrt(abs(2*delta./alpha));
s0 = p(5);
sF = p(6);
[xf, yf, headingf, curvaturef] = sample_n(alpha, L);
pre_end_pose = compose([s0,0,0],[xf, yf, headingf, curvaturef]);
end_pose = compose(pre_end_pose, [sF,0,0]);
ceq = [end_pose', curvaturef] - goal;
end

function [xf, yf, headingf, curvaturef] = sample(alpha, L, k_m)
funx = @(u)cos(k_m.*u + alpha*u.*u/2);
xf = integral(funx, 0, L); 
funy = @(u)sin(k_m.*u + alpha*u.*u/2);
yf = integral(funy, 0, L);
deflectionf = k_m*L + alpha*L*L/2;
headingf = wrapToPi(deflectionf); % heading between +pi and -pi - should make no difference
curvaturef = k_m + alpha*L;
end

function [xf, yf, headingf, curvaturef] = sample_n(alpha, L)
n = length(alpha);
assert(n==length(L));
k = 0;
mid_pose = [0,0,0];
for i = 1:n
    [xf, yf, headingf, curvaturef] = sample(alpha(i), L(i), k);
    mid_pose = compose(mid_pose, [xf(length(xf)), yf(length(yf)), headingf(length(headingf))]);
    k = curvaturef(length(curvaturef));
end

xf = mid_pose(1);
yf = mid_pose(2);
headingf = mid_pose(3);
curvaturef = k;
end