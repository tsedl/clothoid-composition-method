# Clothoid-composition-method

Continuous curvature path generation
...
## solve_bisection.m
solve_bisection.m is the top level script which should produce a x-y plot 
of a path 

## clothoid_pair.m and plot_clothoid_pair.m
clothoid_pair integrates two clothoids given the lengths S1, S2  
and the sharpness (or rate of change of curvature) kDot1, kDot2
The return structure is a 4xN list of [x, y, heading, curvature]
...
plot_clothoid_pair dioes the same integration but produces some graphs


## clothoid.m
Integrates a single clothoid based on a given length S and sharpness kDot
The return structure is a 4xN list of [x, y, heading, curvature]

## join.m
rotates and translates the second given 4xN list [x, y, heading, curvature]
as a rigid body to match the first 4xM list

## decompose.m
Takes the difference between two poses [x, y, heading] and expresses it in
the frame of reference defined by the first pose 