function [x, y, psi, kappa, t] = cubic_clothoid(KDot, S, kappa_0)
% approximate clothoid with a taylor expansion up to t^5
% 5 polynomial terms 

dt = 0.01;
n_samples = ceil(S/dt);


if(S==0)
    fprintf('travel distance S must be greater than the step size %d m \n',dt);
    t=0;
    flex_dt=0;
else
    flex_dt = S/n_samples;
    t = flex_dt:flex_dt:S;
end

kappa = kappa_0 + KDot*t;
psi = KDot*t.*t*0.5 + kappa_0*t;%cumsum(kappa*dt);
psi = wrapToPi(psi);

A = 1/sqrt(KDot);
x = t - t.*t.*t.*t.*t/(2*5*4*A^4);
y = t.*t.*t/(2*3*A^2);

%check approx is valid
% ' good enough for survey if L/2R<3 '
R = 1/(kappa(length(kappa)));
L = S;
p = L/(2*R);
% "should be less than 3 for accurate results when 10 polynomial terms are used" but if only terms up to t^5 are used, L/2R must be less than 0.55 to keep approximation error below 1.5*10^-3'
fprintf('L/2R = %d, terms up to t^5 are used, L/2R must be less than 0.55 to keep approximation error below 1.5*10^-3', p);

end