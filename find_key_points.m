% to check for collisions efficiently it would be prefereable to have a
% small set of x-y points describing a sample shape bounding the curve.
% find two key points per !elementary! clothoid pair. One at the apex 
% and one at the end. Along with the origin this forms a triangle which 
% contains the entire clothoid (for 


% sharpness = [0.1, -0.05];
% chainage = [1.0, 20.0];
% [xlist, ylist] = find_key_points_(sharpness, chainage)
% PlotClothoidSequence(sharpness, chainage);
% plot(xlist, ylist, 'o')

function [xlist, ylist] = find_key_points(sharpness, chainage)
assert(length(sharpness)==length(chainage))
k_m = 0;
abs_pose_m = [0,0,0];
xlist=zeros(1, length(sharpness));
ylist=zeros(1, length(sharpness));
    for i=1:length(sharpness)
        [x,y,heading, curvature] = clothoid_n(sharpness(i),chainage(i), k_m, chainage(i)/1e-2);
        n = length(curvature);
        rel_pose_m = [x(n), y(n), heading(n)];
        k_m = curvature(n);
        abs_pose_m = compose(abs_pose_m, rel_pose_m);
        xlist(i) = abs_pose_m(1);
        ylist(i) = abs_pose_m(2);
        % somehow find the third point where a line at the start meets a
        % line at the goal
    end
end

