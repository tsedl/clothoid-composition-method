function X = compose(X1, X2)
% expects a three vector x, y, heading for each pose X
% returns a three vector for the relative position o X2 in the coordinate 
% frame defined by X1

homogX1 = [cos(X1(3)), -sin(X1(3)), X1(1);
        sin(X1(3)), cos(X1(3)), X1(2);
        0,     0,                 1];
homogX2 = [cos(X2(3)), -sin(X2(3)), X2(1);
        sin(X2(3)), cos(X2(3)), X2(2);
        0,     0,                 1];
homogX3 = homogX1*homogX2;
X = [homogX3(1:2,3);wrapToPi(X1(3)+X2(3))];
end