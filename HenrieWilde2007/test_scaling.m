
delta = pi/2;
kappa = 1;
[x, y, heading, curvature] = hw_clothoid(delta, kappa);
n = length(heading)
final_heading  = heading(n)
final_k = curvature(n)
figure
hold on 
plot(x, y)
[x, y, heading, curvature] = hw_clothoid(delta, 2*kappa);
plot(x, y)
[x, y, heading, curvature] = hw_clothoid(delta, 3*kappa);
plot(x, y)
title(['\delta = ', num2str(delta),' increasing \kappa affects scale'])
legend(['\kappa = ',num2str(kappa)],['\kappa = ',num2str(2*kappa)],['\kappa = ',num2str(3*kappa)])
[x, y, heading, curvature] = hw_clothoid(delta, kappa);
axis equal
figure
hold on 
plot(x, y)
[x, y, heading, curvature] = hw_clothoid(2*delta, kappa);
plot(x, y)
[x, y, heading, curvature] = hw_clothoid(3*delta, kappa);
plot(x, y)
title(['\kappa = ',num2str(kappa),' increasing delta, affects shape'])
legend(['\delta = ' num2str(delta)],['\delta = ',num2str(2*delta)],['\delta = ', num2str(3*delta)])

function [x, y, heading, curvature] = hw_clothoid(delta, kappa)
% integrate a clothoid from the final heading (delta) and the final
% curvature(kappa)
alpha = (kappa*kappa)/(2*delta);
L = kappa/alpha;
final_angle = wrapToPi(alpha*L*L/2)
same_angle = L*kappa/2
[x, y, heading, curvature] = clothoid(alpha, L, 0);
end
