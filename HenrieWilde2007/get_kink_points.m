% A sparse path respresentation identified by Lima (2016) is a set of kink
% points at which the curvature rate changes
% each point is given by [x, y, psi, kappa]
% the curvature rate and arc length of each segment must be calulcated from the kink points
alpha = [1,0, -1, 0, -2, 2, 0];
chainage = [1,0.5, 1, 0.5, 0.25, 0.25, 1];

% plot(x, y);
% hold on;
% kink_points = get_kink_points(alpha, chainage, step);
% delta=0.1; % this is for plotting only
% plot(x(idx),y(idx),'go')
% plot([x(idx), x(idx)+delta*cos(heading(idx))], [y(idx), y(idx)+delta*sin(heading(idx))],'g')
% [ X, Y, Psi, Kappa, S] = reconstruct(kink_points);
% plot(X,Y)

function [x, y, heading, curvature] = get_kink_points(alpha, chainage, step)

[x, y, heading, curvature, s] = PlotClothoidSequence(alpha, chainage);

% insert the origin
idx = 1;
kink_points=[x(idx), y(idx), heading(idx), curvature(idx)];

for segment_length=chainage
   idx = idx + segment_length/0.01;
   kink_points = [kink_points; [x(idx), y(idx), heading(idx), curvature(idx)]];
   plot(x(idx),y(idx),'go')
   plot([x(idx), x(idx)+delta*cos(heading(idx))], [y(idx), y(idx)+delta*sin(heading(idx))],'g')
end


end