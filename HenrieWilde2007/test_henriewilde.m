Pi = [0, 0, 0, 0];
Pf = [6, 8, 60*pi/180, 0];

delta = (Pf(3) - Pi(3))/2;
d = norm(Pf(1:2)- Pi(1:2))/2;
[k, alpha, L] = henriewilde(delta, d)

[x, y, heading, curvature] = clothoid_n(alpha, L, 0, 1000);
n = length(x);
last = [x(n), y(n), heading(n)];

%d
rel = decompose(last, [0, 0, delta]);
error = rel(1)- d;
if abs(error)>1.5*eps
    % the error increases with increasing final angle - why is that?
    error
end

PlotClothoidSequence([alpha, -alpha], [L, L]);

% there is no point in this fresnel() function - clothoid() is correct
% the x and y are identical but the heading is incorrect
% [x1, y1, heading1, curvature1] = fresnel(alpha, L,0);
% n1 = length(x1);
% last1 = [x(n), y(n), heading(n)];
% h1 = norm(last1(1:2))
% rel1 = decompose(last1, Pf)

