% A sparse path respresentation identified by Lima (2016) is a set of kink
% points at which the curvature rate changes
% each point is given by [x, y, psi, kappa]
% the curvature rate and arc length of each segment must be calulcated from the kink points
alpha = [1,0, -1, 0, -2, 2, 0];
chainage = [1,0.5, 1, 0.5, 0.25, 0.25, 1];
[x, y, heading, curvature, s] = PlotClothoidSequence(alpha, chainage);
plot(x, y);
hold on;

% insert the origin
idx = 1;
kink_points=[x(idx), y(idx), heading(idx), curvature(idx)];

delta=0.1; % this is for plotting only
plot(x(idx),y(idx),'go')
plot([x(idx), x(idx)+delta*cos(heading(idx))], [y(idx), y(idx)+delta*sin(heading(idx))],'g')


for segment_length=chainage
   idx = idx + segment_length/0.01;
   kink_points = [kink_points; [x(idx), y(idx), heading(idx), curvature(idx)]];
   plot(x(idx),y(idx),'go')
   plot([x(idx), x(idx)+delta*cos(heading(idx))], [y(idx), y(idx)+delta*sin(heading(idx))],'g')
end

[ X, Y, Psi, Kappa, S] = reconstruct(kink_points);
plot(X,Y)

% It is interesting to observe how the positions diverge over the length of
% each segment, due to the different integration step size. This is the
% motivation for including the exact positions in the format. Otherwise 
% these small but m,easurable errors will add up over the entire length. 