

function [k, alpha, L] = henriewilde(deflection, scale)
% Clothoid Turn(d1, d) function given by Henrie et al 2007
% Input: deflection/heading change deflection, distance/length of
% 'hypotenuse' scale
% Output: final curvature k, sharpness alpha, arc length/chainage L  

if(deflection==0)
    k=0;
    alpha=0;
    L=scale;
else

    [x,y,heading, curvature]  = hw_clothoid(deflection, 1*sign(deflection));

    n = length(x);
    Pm = [x(n), y(n), heading(n), curvature(n)];
    
    unit_rel = decompose(Pm, [0, 0, deflection])
    assert(abs(unit_rel(3))<1.5*eps)

    d2 = Pm(1)*cos(deflection) + Pm(2)*sin(deflection);
    k = sign(deflection)*d2/scale;
    alpha = k*k/(2*deflection);
    L = k/alpha;
    
    
    % second integration for testing only
    [xt, yt, headingt, curvaturet] = wm_clothoid(deflection, k);%clothoid(alpha, L, 0);
    nt = length(xt);
    Pmt = [xt(nt), yt(nt), headingt(nt), curvaturet(nt)];
    unit_relt = decompose(Pmt, [0, 0, deflection])
    %assert(abs(unit_relt(3))<1.5*eps)
    if(abs(unit_relt(3))>1.5*eps)
        'henriewilde.m line(36) failed assert(abs(unit_relt(3))<1.5*eps)'
    end
    if abs(unit_relt(1) - scale)>1.5*eps
        % the error increases with increasing final angle - why is that?
        d2
        scale_error = unit_relt(1) - scale;
        %the ratio error/d2 remains roughly constant...
        scale_error/d2;
        peak_curvature=Pmt(4); %
        error_over_k = scale_error/(Pmt(4)^2);% curvature has no clear relation to the scaling error
    end

end

end
function [x, y, heading, curvature] = hw_clothoid(delta, kappa)
% integrate a clothoid from the final heading (delta) and the final
% curvature(kappa)
alpha = (kappa*kappa)/(2*delta);
L = kappa/alpha;

x = integral(@(u)cos(0.5*pi*u.*u), 0, L);
y = integral(@(u)sin(0.5*pi*u.*u), 0, L);
heading = delta;
curvature = kappa;

end
function [x, y, heading, curvature] = wm_clothoid(delta, kappa)
% integrate a clothoid from the final heading (delta) and the final
% curvature(kappa) following Walton and Meek 2007
scale = sqrt(2*pi*delta)/kappa;
x = (scale/sqrt(2*pi))*integral(@(u)cos(u)./sqrt(u), 0, delta);
y = (scale/sqrt(2*pi))*integral(@(u)sin(u)./sqrt(u), 0, delta);
heading = delta;
curvature = kappa;


end

