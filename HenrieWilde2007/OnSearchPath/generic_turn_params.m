
% Pi = [0, 0, 0*pi/180, 0];
% Pf = [8, 6, 30*pi/180, 0];
% [x, y, heading, curvature, s] = generic_turn_(Pi, Pf, 0);

function [sharpness, chainage] = generic_turn_params(Pi, Pf)
% the procedure below follows the Algorithm generic_turn in Figure 15 page 25 (Wilde 2007)
Pdiff = Pf - Pi;
phi = atan2(Pdiff(2), Pdiff(1));
d = norm(Pdiff(1:2));
theta_0 = Pi(3);
theta_f = Pf(3);

psi_0 = phi - theta_0;
psi_f = theta_f - phi;
eta = psi_0 + psi_f;

q = (psi_0 - psi_f)/4;

% eta=0 means the entry and exit angle are the same. handle this case later. 
if(abs(eta)<1.5*eps)
    a = d/4;
    b = d/4;
    delta_0 = psi_0 ;
    delta_f = psi_f ;
else
    A = psi_f/2 + q;
    B = psi_0/2 - q;
    delta_0 = psi_0/2 + q;
    delta_f = psi_f/2 - q;
    a = d*sin(abs(A))/sin(abs(eta));
    b = d*sin(abs(B))/sin(abs(eta));
end

[k1, alpha1, L1] = henriewilde(delta_0, a);
[k2, alpha2, L2] = henriewilde(delta_f, b);

sharpness = [alpha1, -alpha1, alpha2, -alpha2];
chainage = [L1, L1, L2, L2];

% subplot(2, 1, 1)
% [x, y, heading, curvature, s] = PlotClothoidSequence(sharpness, chainage, Pi);
% hold on
% plot_len = 1;
% 
% subplot(2,1,1)
% plot(Pi(1), Pi(2), 'rx');
% hold on
% plot(Pf(1), Pf(2), 'gx');
% axis equal
% plot([Pi(1),Pi(1) + plot_len*cos(Pi(3))], [Pi(2), Pi(2)+plot_len*sin(Pi(3))], 'r');
% hold on
% plot([Pf(1), Pf(1) + plot_len*cos(Pf(3))], [Pf(2), Pf(2)+plot_len*sin(Pf(3))], 'g');
% 
% plot(x, y, '.')
% 
% R = 1/max(abs(curvature));
% xlabel('x [m]')
% ylabel('y [m]')
% title(['Minimum R = ', num2str(R), 'm'])
% 
% subplot(2, 1, 2)
% plot(s, curvature, '.')
% xlabel('arc length s [m]')
% ylabel('curvature \kappa [m^{-1}]')


end