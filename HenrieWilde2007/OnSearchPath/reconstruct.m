% A sparse path respresentation identified by Lima (2016) is a set of kink
% points at which the curvature rate changes
% each point is given by [x, y, psi, kappa]
% the curvature rate and arc length of each segment must be calulcated from the kink points

function [ X, Y, Psi, Kappa, S] = reconstruct(kink_points)

    alpha_t = [];
    chainage_t = [];
    for i = 1:length(kink_points(:,1))-1
        P_i = kink_points(i,:);
        P_i1 = kink_points(i+1,:);
        dPsi = P_i1(3)-P_i(3);
        dKappa = P_i1(4)-P_i(4);

        % matching heading and curvature
        if(abs(dKappa)<eps)
            if(abs(dPsi)<eps)
                % Segment is a straight line
                L1 = norm(P_i(1:2)-P_i1(1:2));
                alpha1 = 0;
            else
                % Its an arc
                L1 = dPsi/P_i(4);
                alpha1 = 0;
            end
        else
            % Its a clothoid
            L1 = abs(2*dPsi/dKappa);
            alpha1 = dKappa/L1;
        end
        alpha_t = [alpha_t, alpha1];
        chainage_t = [chainage_t, L1];
    end

    [x_t, y_t, heading_t, curvature_t, s_t] = PlotClothoidSequence(alpha_t, chainage_t);
%    plot(x_t, y_t); % this matches the original curve exactly only because the same integration function is used 


    X =[];
    Y=[];
    Psi=[];
    Kappa=[];
    S=[];
    for i = 1:length(kink_points(:,1))-1
        P_i = kink_points(i,:);
        P_i1 = kink_points(i+1,:);
        [xr,yr, psir, kappar, sr] = clothoid_n(alpha_t(i), chainage_t(i),P_i(4), 1000);
        xfun = @(s)cos(s.*s*alpha_t(i)/2);
    %     xb = integral(xfun, 0, chainage_t(i))
    %     yfun = @(s)sin(s.*s*alpha_t(i)/2);
    %     yb = integral(yfun, 0, chainage_t(i))
    %     n = length(xr)
    %     xrn = xr(n)
    %     yrn = yr(n)
        [x,y, psi, kappa, s] = join([P_i,0],[xr,yr, psir, kappar, sr]);

        X = [X,x];
        Y = [Y,y];
        Psi = [Psi,psi];
        Kappa = [Kappa, kappa];
        S = [S,s];
    end
    
end

% It is interesting to observe how the positions diverge over the length of
% each segment, due to the different integration step size. This is the
% motivation for including the exact positions in the format. Otherwise 
% these small but m,easurable errors will add up over the entire length. 