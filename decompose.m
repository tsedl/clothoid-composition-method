function delta2 = decompose(X2, X1)
% expects a three vector x, y, heading for each pose X
% returns a three vector for the relative position o X2 in the coordinate 
% frame defined by X1

% force them to be column vectors
X1 = X1(:);
X2 = X2(:);

Rot2D = [cos(-X1(3)), -sin(-X1(3));
        sin(-X1(3)), cos(-X1(3))];
delta2_W = X2(1:2) - X1(1:2);
delta2(1:2) = Rot2D*delta2_W;
delta2(3) = wrapToPi(X2(3) - X1(3));
end


