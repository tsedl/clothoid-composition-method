function [x, y, heading, curvature, s] = IntegrateClothoidSequence(sharpness, chainage)
    %INTEGRATECLOTHOIDSEQUENCE
    % return a series of pose samples for a sequence of clothoids defined by
    % the given start pose, each with rate of change of curvature sharpness and
    % length chainage, curvature and heading continuity maintained between each
    % one
    assert(length(sharpness)==length(chainage))
    q_0 = [0, 0, 0, 0];
    x = q_0(1);
    y = q_0(2);
    heading = q_0(3);
    curvature = q_0(4);
    s = 0;

    for i=1:length(sharpness)
        [X,Y,Heading,Curvature, S] = clothoid(sharpness(i), chainage(i), q_0(4));
        n = length(X);
        q_0 = [X(n), Y(n), Heading(n), Curvature(n), S(n)];

        produce_graph = 0;
        [x, y, heading, curvature, s] = join([x, y, heading, curvature, s], [X,Y,Heading,Curvature, S], produce_graph);
    end

end
