%% clothoid investigation with plotting

% aim to write a function which joins two curves to achieve a certain x, y, psi, kappa at the end
% free variables are k1, s1, k2, s2

function [x,y,psi,kappa,t] = clothoid(Kdot, S, kappa_0, dt)

if nargin <4
    dt = 0.01;
end
n_samples = ceil(S/dt);


if(S<eps)
    fprintf('travel distance S must be greater than the step size %d m \n',dt);
    t=0;
    flex_dt=0;
else
    flex_dt = S/n_samples;
    t = flex_dt:flex_dt:S;
end

kappa = Kdot*t + kappa_0;


psi = Kdot*t.*t*0.5 + kappa_0*t;%cumsum(kappa*dt);
psi = wrapToPi(psi); % wrap angle to range (-pi, +pi) to prevent weirdness

   
x = cumsum(flex_dt*cos(psi)) ;
y = cumsum(flex_dt*sin(psi));
if(any(isnan(x)))
    assert(false)
end


end



