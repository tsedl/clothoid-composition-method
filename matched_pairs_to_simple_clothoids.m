function [alpha, chainage] = matched_pairs_to_simple_clothoids(alpha_first, deflection, theta)
    chainage = zeros(1, 2*length(deflection));
    alpha = zeros(1, 2*length(alpha_first));
    
    for i = 1:length(deflection)
        k1 = sqrt(2*deflection(i)*alpha_first(i));
        k2 = k1;
        deflection2 = theta(i) - deflection(i);
        if(abs(deflection2)>eps)
            alpha2 = positive_signum(alpha_first(i))*k2*k2/(2*deflection2);
        else
            % as the deflection2 is zero, the length will be zero and
            % the sharpness magnitude is irrelevant
            % the sign must match the deflection for the square below
            alpha2 = positive_signum(alpha_first(i))*positive_signum(deflection2);
        end
        s1 = sqrt(2*deflection(i)/alpha_first(i));
        s2 = sqrt(sign(alpha_first(i))*2*deflection2/alpha2);
        
        alpha(2*i-1) = alpha_first(i);
        alpha(2*i) = -positive_signum(alpha_first(i))*alpha2;
        chainage(2*i - 1) = s1;
        chainage(2*i) = s2;

    end
end
function y = positive_signum(x)
if any(x>=0)
    y = ones(size(x));
else
    y = -ones(size(x));
end
end