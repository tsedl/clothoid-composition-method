function [xx, yy, psipsi, kappakappa] = plot_clothoid_pair(Kdot1, S1, Kdot2, S2, point)
% Integrate a pair of matched clothoids with the provided rate of change of
% curvature Kdot and length S. Plot the path in x-y space

    
    
[x, y, psi, kappa, s] = clothoid(Kdot1, S1, 0);
n = length(x);
s_m = [x(n);y(n);psi(n);kappa(n)];

[x_, y_, psi_, kappa_, s_] = clothoid(Kdot2, S2, s_m(4));
n_ = length(x_);

figure();
subplot(1,2,1);

plot(x,y)
hold on
if nargin==5
    plot(point(1),point(2), 'x')
    plot([point(1), point(1) + cos(point(3))],[point(2), point(2)+sin(point(3))], '-')
end

hold on
% plot(x_,y_)
% hold on
axis equal


s_2 = [x_(n_);y_(n_);psi_(n_);kappa_(n_)];
psi_m  = s_m(3);
rot  = [cos(psi_m), -sin(psi_m);
    sin(psi_m), cos(psi_m)];
s_f(1:2) = s_m(1:2) + rot*s_2(1:2);
s_f(3) = wrapToPi(s_m(3) + s_2(3));
s_f(4) = s_2(4);

xyshift = rot*[x_;y_];
xx = [x, s_m(1) + xyshift(1,:)];
yy = [y, s_m(2) + xyshift(2,:)];

plot(s_m(1) + xyshift(1,:), s_m(2) + xyshift(2,:))
plot(s_m(1),s_m(2), 'o')
xlabel('x [m]')
ylabel('y [m]')

psipsi = [psi, s_m(3) + psi_];
psipsi = wrapToPi(psipsi);
kappakappa = [kappa, kappa_];
ss = [s, S1 + s_];

n = length(xx);
last = [xx(n), yy(n), psipsi(n), kappakappa(n)]
D = decompose(point, last);
straight_length = D(1)
after_straight = compose(last, [straight_length, 0, 0]);

plot([last(1), after_straight(1)], [last(2), after_straight(2)])
hold off

subplot(1,2,2);
plot(ss, kappakappa, 'color', lines(1))
hold on;
curve_length = ss(n);
plot([curve_length, curve_length+straight_length],[0, 0], 'color', lines(1))
hold off;
xlabel('travel length [m]')
ylabel('curvature [1/m]')


end
