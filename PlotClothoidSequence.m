function [x, y, heading, curvature, s] = PlotClothoidSequence(sharpness, chainage, start_pose, produce_graph)
    %PLOTCLOTHOIDSEQUENCE
    % return a series of pose samples for a sequence of clothoids defined by
    % the given start pose, each with rate of change of curvature sharpness and
    % length chainage, curvature and heading continuity maintained between each
    % one
    if(nargin<4)
        produce_graph = false;
    end
    if(nargin<3)
        start_pose = [0,0,0];
    end
    assert(length(sharpness)==length(chainage))
    q_0 = [start_pose, 0];
    x = q_0(1);
    y = q_0(2);
    heading = q_0(3);
    curvature = q_0(4);
    s = 0;

    for i=1:length(sharpness)
        [X,Y,Heading,Curvature, S] = clothoid(sharpness(i), chainage(i), q_0(4));
        n = length(X);
        if n>0
            q_0(4) = Curvature(n);
            [x, y, heading, curvature, s] = join([x, y, heading, curvature, s], [X,Y,Heading,Curvature, S], produce_graph);
        else
            %q_0(4) = q_0(4);% in rare cases where n is zero the joining curvature is unchanged
            ['cant join clothoid', string(i), 'as zero length']
        end

        
    end

end
