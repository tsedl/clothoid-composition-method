function [xx, yy, psipsi, kappakappa, ss] = clothoid_pair(Kdot1, S1, Kdot2, S2, draw_graph)
% Integrate a pair of matched clothoids with the provided rate of change of
% curvature Kdot and length S. Plot the path in x-y space
if nargin==4
    draw_graph = false;
end
[x, y, psi, kappa, s] = clothoid(Kdot1, S1, 0);
n = length(x);
s_m = [x(n);y(n);psi(n);kappa(n)];
[x_, y_, psi_, kappa_, s_] = clothoid(Kdot2, S2, s_m(4));
[xx, yy, psipsi, kappakappa, ss] = join([x, y, psi, kappa, s],[x_, y_, psi_, kappa_, s_],draw_graph);
end
